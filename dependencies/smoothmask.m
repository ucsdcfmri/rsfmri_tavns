function smask =smoothmask(mask);
  
%$Id$  

 convmap = ones(3,3);
 if length(size(mask)) == 2

   cmask = conv2(double(mask),convmap,'same');
   mask = mask.*(cmask > 2);
   smask = conv2(mask,convmap,'same') > 3;
 else
   smask = zeros(size(mask));
   nslices = size(mask,3);
   for islice = 1:nslices
     cmask = conv2(double(mask(:,:,islice)),convmap,'same');
     tmask = mask(:,:,islice).*(cmask > 2);
     smask(:,:,islice) = conv2(tmask,convmap,'same') > 3;
   end
 end 
 