function clustCell=findClusters(onesMap,cMode)
%usage: clustCell=findClusters(onesMap,cMode)
% finds all continous clusters in onesMap and saves them in seperate cells
%
% Input:
%    onesMap       - input binary matrix
%    cMode        - cluster mode: 1- touching faces, 2-touching faces/edges
%                   3- touching faces/edges/coners
%    
%
% Output:
%    clustCell     - cell array of all clusters (regardless of cluster size)
%
% Copyright (c) 2006 CUPID
% Author: kal
% History:
%  $Id$

% ruler
% --1234567890123456789012345678901234567890123456789012345678901234567890


oneS=onesMap;
cnt=1;
if (sum(oneS(:))>0)==0
    clustCell{1}=zeros(size(onesMap));
else
    while sum(oneS(:))>0
        fk=find(oneS);
        [x y z]=ind2sub(size(oneS),fk(1));
        clustCell{cnt} = grow3d(oneS, [x+1 y+1 z+1],cMode);
        oneS=oneS-clustCell{cnt};
        cnt=cnt+1;
    end
end