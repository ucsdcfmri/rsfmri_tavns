function [avgGM_power_noise_WM, avgGM_power_noise_CSF, avgGM_power_noise_WMCSF,...
    avgGM_power_funcData_noPoly, avgGM_power_funcData_noPoly_noWM, ...
    avgGM_power_funcData_noPoly_noCSF, avgGM_power_funcData_noPoly_noWMCSF, ...
    power_funcData_noPoly_meanGM, power_funcData_noPoly_noWM_meanGM, ...
    power_funcData_noPoly_noCSF_meanGM, power_funcData_noPoly_noWMCSF_meanGM, ...
    freq] = calcNoiseSpect(WM_regs, CSF_regs,...
    funcData_noPoly, brain_mask, GM_mask, nvol, TR)
% reshape the GM mask
sz = size(GM_mask);
GM_mask_rs = reshape(GM_mask, sz(1)*sz(2)*sz(3), 1);

% calculate the mean spectra over the voxels in the GM mask
sz = size(funcData_noPoly);
funcData_noPoly_rs = reshape(funcData_noPoly, sz(1)*sz(2)*sz(3), sz(4));
funcData_noPoly_rs_GM = funcData_noPoly_rs(GM_mask_rs, :);
% calculate the spectra
[dF,freq]   = my_yfft(funcData_noPoly_rs_GM',nvol,1/TR);
avgGM_power_funcData_noPoly   = mean(abs(dF).^2,2)./nvol;

% calculate the spectra of the mean GM signal
funcData_noPoly_meanGM = mean(funcData_noPoly_rs_GM);
% calculate the spectra
[dF,freq]   = my_yfft(funcData_noPoly_meanGM,nvol,1/TR);
power_funcData_noPoly_meanGM   = abs(dF).^2./nvol;

% regress out the WM regressors
funcData_noPoly_noWM = processPreg_RS(funcData_noPoly, ...
    WM_regs,brain_mask, 0);
% the noise components estimated by the WM regressors
noise_WM = funcData_noPoly - funcData_noPoly_noWM;
% calculate the mean spectra of the estimated noise components
% over the voxels in the gray matter mask
sz = size(noise_WM);
noise_WM_rs = reshape(noise_WM, sz(1)*sz(2)*sz(3), sz(4));
noise_WM_rs_GM = noise_WM_rs(GM_mask_rs, :);
% calculate the spectra
[dF,freq]   = my_yfft(noise_WM_rs_GM',nvol,1/TR);
avgGM_power_noise_WM   = mean(abs(dF).^2,2)./nvol;

% calculate the mean spectra over the voxels in the GM mask
% after regressing out the WM regressor
sz = size(funcData_noPoly_noWM);
funcData_noPoly_noWM_rs = reshape(funcData_noPoly_noWM, sz(1)*sz(2)*sz(3), sz(4));
funcData_noPoly_noWM_rs_GM = funcData_noPoly_noWM_rs(GM_mask_rs, :);
% calculate the spectra
[dF,freq]   = my_yfft(funcData_noPoly_noWM_rs_GM',nvol,1/TR);
avgGM_power_funcData_noPoly_noWM   = mean(abs(dF).^2,2)./nvol;

% calculate the spectra of the mean GM signal
% after regressing out the WM regressor
funcData_noPoly_noWM_meanGM = mean(funcData_noPoly_noWM_rs_GM);
% calculate the spectra
[dF,freq]   = my_yfft(funcData_noPoly_noWM_meanGM,nvol,1/TR);
power_funcData_noPoly_noWM_meanGM   = abs(dF).^2./nvol;

% regress out the CSF regressors
funcData_noPoly_noCSF = processPreg_RS(funcData_noPoly, ...
    CSF_regs,brain_mask, 0);
% the noise components estimated by the CSF regressors
noise_CSF = funcData_noPoly - funcData_noPoly_noCSF;
% calculate the mean spectra over the voxels in the gray matter
% mask
sz = size(noise_CSF);
noise_CSF_rs = reshape(noise_CSF, sz(1)*sz(2)*sz(3), sz(4));
noise_CSF_rs_GM = noise_CSF_rs(GM_mask_rs, :);
% calculate the spectra
[dF,freq]   = my_yfft(noise_CSF_rs_GM',nvol,1/TR);
avgGM_power_noise_CSF   = mean(abs(dF).^2,2)./nvol;
% calculate the mean spectra over the voxels in the GM mask
% after regressing out the CSF regressor
sz = size(funcData_noPoly_noCSF);
funcData_noPoly_noCSF_rs = reshape(funcData_noPoly_noCSF, sz(1)*sz(2)*sz(3), sz(4));
funcData_noPoly_noCSF_rs_GM = funcData_noPoly_noCSF_rs(GM_mask_rs, :);
% calculate the spectra
[dF,freq]   = my_yfft(funcData_noPoly_noCSF_rs_GM',nvol,1/TR);
avgGM_power_funcData_noPoly_noCSF   = mean(abs(dF).^2,2)./nvol;
% calculate the spectra of the mean GM signal
% after regressing out the CSF regressor
funcData_noPoly_noCSF_meanGM = mean(funcData_noPoly_noCSF_rs_GM);
% calculate the spectra
[dF,freq]   = my_yfft(funcData_noPoly_noCSF_meanGM,nvol,1/TR);
power_funcData_noPoly_noCSF_meanGM   = abs(dF).^2./nvol;


% regress out the WM and CSF regressors
funcData_noPoly_noWMCSF = processPreg_RS(funcData_noPoly, ...
    [WM_regs, CSF_regs],brain_mask, 0);
% the noise components estimated by the CSF regressors
noise_WMCSF = funcData_noPoly - funcData_noPoly_noWMCSF;
% calculate the mean spectra over the voxels in the gray matter
% mask
sz = size(noise_WMCSF);
noise_WMCSF_rs = reshape(noise_WMCSF, sz(1)*sz(2)*sz(3), sz(4));
noise_WMCSF_rs_GM = noise_WMCSF_rs(GM_mask_rs, :);
% calculate the spectra
[dF,freq]   = my_yfft(noise_WMCSF_rs_GM',nvol,1/TR);
avgGM_power_noise_WMCSF   = mean(abs(dF).^2,2)./nvol;

% calculate the mean spectra over the voxels in the GM mask
% after regressing out the WM and CSF regressors
sz = size(funcData_noPoly_noWMCSF);
funcData_noPoly_noWMCSF_rs = reshape(funcData_noPoly_noWMCSF, sz(1)*sz(2)*sz(3), sz(4));
funcData_noPoly_noWMCSF_rs_GM = funcData_noPoly_noWMCSF_rs(GM_mask_rs, :);
% calculate the spectra
[dF,freq]   = my_yfft(funcData_noPoly_noWMCSF_rs_GM',nvol,1/TR);
avgGM_power_funcData_noPoly_noWMCSF   = mean(abs(dF).^2,2)./nvol;
% calculate the spectra of the mean GM signal
% after regressing out the WM and CSF regressors
funcData_noPoly_noWMCSF_meanGM = mean(funcData_noPoly_noWMCSF_rs_GM);
% calculate the spectra
[dF,freq]   = my_yfft(funcData_noPoly_noWMCSF_meanGM,nvol,1/TR);
power_funcData_noPoly_noWMCSF_meanGM   = abs(dF).^2./nvol;
end

