function [keepV clustCellP]=imClusterCust(onesMap,clustSize,cMode)
%usage: [keepV clustCellP]=imClusterCust(onesMap,clustSize,cMode)
% clusters image
%
% Input:
%    onesMap      - binary 3d map of active voxels
%    clustSize    - cluster size
%    cMode        - cluster mode: 1- touching faces, 2 [default] -touching faces/edges 
%                   3- touching faces/edges/coners
%
%
% Output:
%    keepV        - voxels which pass the cluster criteria
%    clustCellP   - cell array of independent clusters which pass criteria
%
% Notes:
%       -Voxels are assumed to be isotropic (1x1x1)
%       -Any given voxel has 6 face neighbors, 12 edge neighbors, and 8 corner neighbors
%       -the radius for face=1, radius for edge=sqrt(2), radius for corner=sqrt(3)
%       -these radii must be consistent with runAlphaSim, i.e. use same
%       cMode for both programs!
%
% Copyright (c) 2006 CUPID
% Author: kal
% History:
%  $Id$

% ruler
% --1234567890123456789012345678901234567890123456789012345678901234567890

if ~exist('cMode')       | isempty(cMode)         cMode=2;                end

keepV=zeros(size(onesMap));clustCellP=[];
clustCell=findClusters(onesMap,cMode);
pc=1;
for cc=1:length(clustCell)
    if sum(clustCell{cc}(:))>=clustSize
        keepV=keepV+clustCell{cc};
        disp(['Cluster of ' num2str(sum(clustCell{cc}(:))) ' voxels passed'])
        clustCellP{pc}=clustCell{cc};
        pc=pc+1;
    else
        disp(['Cluster of ' num2str(sum(clustCell{cc}(:))) ' voxels rejected'])
    end
end
keepV=keepV>0;
disp(['Total Clustered voxels= ' num2str(sum(keepV(:))) ' voxels'])
