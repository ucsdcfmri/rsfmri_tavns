function [HR, HRCRF, RV, RVRRF, CRF, RRF, c_peak] = calcRVHRCOR(raw_card, card_srate, raw_resp, resp_srate, TR, N, m)
% 
% [GS]=calcGS(funcData, mask, type)
%
% calculate RVHRCOR regressors (see Chang, et al., NIMG, 2009)
% - input
% raw_card      - the raw cardiac signal
% card_srate    - the sampling rate of the raw cardiac signal in Hz.
% raw_resp      - the raw respiration signal
% resp_srate    - the sampling rate of the raw respiration signal in Hz.
% TR            - the repetition time of the corresponding functional data,
%                 or 1 over the sampling rate of the output timeseries
% N             - the total number of volumes in the fMRI acquisition before remove any TRs;
%                 Note, the values of N will not influence the outputs. 

% - output 
% HR        - heart rate (HR, unit: beats-per-minute)
% HRCRF     - HR convolved with the cardiac response function (CRF, derived in Chang, et al., NIMG, 2009)
% RV        - respiration volume (RV, unitless)
% RVRRF     - RV convolved with the respiration response function (RRF, derived in Birn, et al., NIMG 2008)
% CRF       - the cardiac response function
% RVF       - the respiration response function
% c_peak    - the location of the cardiac peaks.
% Author: Yixiang Mao Apr 29 2021
% $Id$

% defualt cardiac sampling rate: 100 Hz
if isempty(card_srate); card_srate = 100; end
% defualt cardiac sampling rate: 25 Hz
if isempty(resp_srate); card_srate = 25; end
% m will be picked to make the sliding window length to approx be 6 sec
% the sliding windown with length (2*m+1)*TR will be used in calculation of
% HR and RV
if isempty(m);
m = round(6/TR/2); % the values of HR and RV at k-th TR will be 
                   % calculated based on (k-m)-th, (k-m+1)-th, ..., k-th,
                   % (k+1)-th, ..., (k+m)-th TRs 
end

card_sample_per_TR = card_srate*TR; % number of cardiac timepoints per TR
resp_sample_per_TR = resp_srate*TR; % number of respiration timepoints per TR

% the maximum number of TRs, it will equal to the length of the output data
num_TRs_in_raw_card = ceil(length(raw_card)/card_sample_per_TR); 
a_raw_card = mod(length(raw_card), card_sample_per_TR); % length(raw_card) = (num_TRs_in_raw_card-1)*card_sample_per_TR+a
T_raw_card = (length(raw_card) - N*TR*card_srate)/card_srate; % the raw cardiac recording started T sec before the scan started.

% detect the cardiac peaks
[c_peak, mean_freq, numpeaks_expected, numpeaks] = findGECardiacPeak(raw_card,0);
% calculate the HR
HR = zeros(num_TRs_in_raw_card, 1);
for idx = 1:num_TRs_in_raw_card
    n = idx - (num_TRs_in_raw_card-N); % we are calculating the HR corresponds to the n-th TR;
                % if n<0, that means we are calculating the HR before the
                % scan started.
    % again, raw cardiac data assigned to the (n-m)-th TR, (n-m+1)-th TR,
    % ..., (n+m)-th TR are used calculate the HR assigned to the n-th TR
    
    % the raw cardiac data assigned to the (n-m)-th TR started at the idx:
    % T*card_srate+(n-m-1)*TR*card_srate+1
    % the raw cardiac data assigned to the (n+m)-th TR ended at the idx:
    % T*card_srate+(n+m)*TR*card_srate
    
    start_idx = round(max(1, T_raw_card*card_srate+(n-m-1)*TR*card_srate+1));
    end_idx = round(min(length(raw_card), T_raw_card*card_srate+(n+m)*TR*card_srate));
    % why we use round here:
    % based on the expression of start_idx and end_idx, they should be
    % integers. However, somehow, when the MATLAB calculates them, the
    % values of of start_idx and end_idx are slightly off from the true
    % integer values. To overcome that, we use the round function.
    
    HR(idx) = 60/mean(diff(find(c_peak(start_idx:end_idx)))./card_srate);
end
% form CRF
t  = 0:TR:30; 
CRF = 0.6*(t.^2.7).*exp(-t/1.6) - (16/sqrt(2*9*pi))*exp(-(t-12).^2 / 18);
CRF = CRF/std(CRF);
% calculate HRCRF
HRCRF = conv(HR, CRF);
HRCRF = HRCRF(1:length(HR));

% the maximum number of TRs, it will equal to the length of the output data
num_TRs_in_raw_resp = ceil(length(raw_resp)/resp_sample_per_TR);
a_raw_resp = mod(length(raw_resp), resp_sample_per_TR); % length(raw_resp) = (num_TRs_in_raw_resp-1)*resp_sample_per_TR+a
T_raw_resp = (length(raw_resp) - N*TR*resp_srate)/resp_srate; % the raw respiration recording started T sec before the scan started.


% calculate the RV
RV = zeros(num_TRs_in_raw_resp, 1);
for idx = 1:num_TRs_in_raw_resp
    n = idx - (num_TRs_in_raw_resp-N); % we are calculating the RV corresponds to the n-th TR;
                % if n<0, that means we are calculating the RV before the
                % scan started.
    % again, raw respiration data assigned to the (n-m)-th TR, (n-m+1)-th TR,
    % ..., (n+m)-th TR are used calculate the RV assigned to the n-th TR
    
    % the raw respiration data assigned to the (n-m)-th TR started at the idx:
    % T*resp_srate+(n-m-1)*TR*resp_srate+1
    % the raw respiration data assigned to the (n+m)-th TR ended at the idx:
    % T*resp_srate+(n+m)*TR*resp_srate
    
    start_idx = round(max(1, T_raw_resp*resp_srate+(n-m-1)*TR*resp_srate+1));
    end_idx = round(min(length(raw_resp), T_raw_resp*resp_srate+(n+m)*TR*resp_srate));
    % why we use round here:
    % based on the expression of start_idx and end_idx, they should be
    % integers. However, somehow, when the MATLAB calculates them, the
    % values of of start_idx and end_idx are slightly off from the true
    % integer values. To overcome that, we use the round function.
    
    RV(idx) = std(raw_resp(start_idx:end_idx));

end
% form RRF
t=0:TR:40;
RRF =0.6*(t.^2.1).*exp(-t/1.6) - 0.0023*(t.^3.54).*exp(-t/4.25);
RRF= RRF/std(RRF);
% calculate RVRRF
RVRRF = conv(RRF, RV);
RVRRF = RVRRF(1:length(RV));
end

