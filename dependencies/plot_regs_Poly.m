function plot_regs_Poly(reg_polynomials,subj_info_str, line_width, label_size)
% plot the legendre polynomial regressors
figure2('Renderer', 'painters', 'Position', [10, 10, 800, 450])
hold on
legends = {};
for i = 1:size(reg_polynomials, 2)
    plot(reg_polynomials(:, i), 'LineWidth', line_width)
    legends = {legends{:}, [num2str(i-1), '-order legendre polynomial']};
end
hold off
grid on
xlabel('volume', 'FontWeight', 'bold', 'FontSize', label_size)
ylabel('legendre polynomial', 'FontWeight', 'bold', 'FontSize', label_size)
legend(legends, 'FontWeight', 'bold', 'FontSize', label_size, 'Location', 'best')
title({['The legendre polynomial regressors'];
    subj_info_str}, 'FontWeight', 'bold', 'FontSize', label_size)
end

