function numdecimalplaces = pvaldecformat(p,numsigfigs)
% Purpose, intake a p-value and a specified number of significant figures,
% and figure out how many decimal places you need in order to show that
% number of significant figures as a string.

% The reason I need this function is that MATLAB forces small numbers to be
% shown in scientific notation. I don't always want this though. 
% https://www.mathworks.com/matlabcentral/answers/23281-how-to-stop-exponential-notation-both-in-output-and-in-variable-editor

% Usage: sprintf(['%.',pvaldecformat(p,3),'f],p);
% This essentially becomes something like sprintf('%.5f',p);

firstsigdecplace = abs(floor(log10(p)));
numdecimalplaces = firstsigdecplace + numsigfigs - 1;
numdecimalplaces = num2str(numdecimalplaces);

end
