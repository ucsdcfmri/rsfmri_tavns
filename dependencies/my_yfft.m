function [sigFFT,freq,w] = my_yfft(signal,nfft,fs)

%my fft function which mean subtracts and window data prior to fft


%defaults
if ~exist('fs')     | isempty(fs)       fs=1; end

%get size
sZ          = size(signal);

%reshape if a single vector into column vector
if min(sZ)==1
    signal  = reshape(signal,max(sZ),1);
    sZ      = size(signal);
end

% %subtract the mean
% mSig        = signal-repMetric(signal,'mean',1);

%form window 
w           = blackmanharris(sZ(1));
w           = repmat(w,1,sZ(2));

% subtract the mean and apply the window
wSig        = (signal - repMetric(w.*signal, 'mean', 1)./...
    repMetric(w, 'mean', 1)).*w;

if ~exist('nfft')    | isempty(nfft)     
    nfft    =  2^floor(log2(sZ(1)));
end

%take fft
rawFFT      = fft(wSig,nfft);
sigFFT      = rawFFT(1:(nfft./2)+1,:);


%determine freq
freq        = linspace(0,fs./2,(nfft/2+1));


