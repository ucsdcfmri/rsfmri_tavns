function plot_regs_TissueSeg(underlay_anat_path, slices_plot, nrow, ncol,...
    brain_mask_orig, pv_mask, gray_pv_thres, GLM, subj_info_str,...
    line_width, label_size)
% plot results of brain tissue segmentation
% overlay is the partial volume maps for gray matter, white matter
% and CSF, i.e. the result of brain tissue segmentation

% load the anatomical image that was aligned to the functional data
[~, anat_aligned2func, ~, ~] = BrikLoad (underlay_anat_path);
% plot settings
tthresh = 1e-6;
cthresh = 0;
mask = logical(brain_mask_orig(:, :, slices_plot));
underlay = anat_aligned2func(:, :, slices_plot);
underlayrange = [min(min(min(anat_aligned2func))), max(max(max(anat_aligned2func)))];
figure_position = [10 10 800 600];

overlayrange = [0, 3];
tmp = zeros(size(brain_mask_orig));
tmp(logical(pv_mask.gray>gray_pv_thres)) = 1; % assign 1 to gray matter
tmp(logical(pv_mask.white>GLM.WMCSF_pv_thres(1))) = 2; % assign 2 to white matter
tmp(logical(pv_mask.csf>GLM.WMCSF_pv_thres(2))) = 3; % assign 3 to CSF
overlay = tmp(:, :, slices_plot);
figure('Renderer', 'painters', 'Position', figure_position)
imageoverlay(underlay,overlay,underlayrange,overlayrange,nrow,ncol,tthresh,cthresh,mask);
title({['The brain tissue segmentation results'];
    ['1: gray matter (pv map>', num2str(gray_pv_thres), ', no erosion);'];
    ['2: white matter (pv map>', num2str(GLM.WMCSF_pv_thres(1)), ', no erosion);'];
    ['3: CSF (pv map>', num2str(GLM.WMCSF_pv_thres(2)), ', no erosion)'];
    subj_info_str},...
    'fontweight','bold','fontsize',10);
end

