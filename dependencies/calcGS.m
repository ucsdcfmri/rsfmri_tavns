function [GS, num_vox]=calcGS(funcData, brainMask, type)

% 
% [GS]=calcGS(funcData, mask, type)
%
% create global signal (GS)
% - input
% funcData   - the functional data.
%              1. it can be a 2D matrix (num. voxels times num. volumes).
%              2. it can be a 4D martix 
% brainMask  - the binary mask that includes the whole brain
%              1. it can be a vector
%              2. it can be a 3D matrix
% type       - how to normalize the functional data
%              0: don't normalize (use this in case the functional data have been normalized)
%              1: convert to percent change (default)
%              2: z-normalization (zero mean and unit variance)
% - output
% GS         - global signal, a column vector (num. volumes times 1)
% num_vox    - number of voxels used in the calculation of GS (more of a sanity check)
% Author: Yixiang Mao Apr 29 2021
% $Id$

% defualt type = 1
if ~exist('type','var'); type = 1; end
if isempty(type); type = 1; end

sz = size(funcData);
if length(sz) ~= 2
    funcData = reshape(funcData, sz(1)*sz(2)*sz(3), sz(4));
end

sz = size(brainMask);
if length(sz) ~= 2
    brainMask = reshape(brainMask, sz(1)*sz(2)*sz(3), 1);
end
brainMask = logical(brainMask); % make sure it is a binary mask

% check if the mask and the functional data have the same number of voxels
if size(brainMask, 1) == size(funcData, 1)
    % mask the functional data
    funcData_masked = funcData(brainMask, :);
else 
    error('The mask and the functional data don''t have the same number of voxels!')
    
end

switch type
    case 0 % should be used in case the functional data have been normalized
        GS = mean(funcData_masked)';
        num_vox = size(funcData_masked, 1);
    case 1 % convert to percent change before compute GS (defualt)
        funcData_perc = ...
            (funcData_masked - repmat( mean(funcData_masked,2) ,1, size(funcData_masked,2) ) ) ...
            ./ repmat( mean(funcData_masked,2) ,1, size(funcData_masked,2) )*100;
        % nan would occur if the mean of a voxel is zero
        % get rid of such voxels 
        nanVoxel = isnan(funcData_perc(:, 1));
        funcData_perc(nanVoxel, :) = [];
        % compute GS
        GS = mean(funcData_perc)';
        num_vox = size(funcData_perc, 1);
    case 2 % z-normalization (zero mean and unit variance)
        funcData_norm = ...
            (funcData_masked - repmat( mean(funcData_masked,2) ,1, size(funcData_masked,2) ) ) ...
            ./ repmat( std(funcData_masked, 0, 2) ,1, size(funcData_masked,2) );
        % nan would occur if the std of a voxel is zero
        % get rid of such voxels 
        nanVoxel = isnan(funcData_norm(:, 1));
        funcData_norm(nanVoxel, :) = [];
        % compute GS
        GS = mean(funcData_norm)';
        num_vox = size(funcData_norm, 1);
end

end






