function [pv_mask]=fsl_loadpv(fullPath_anat,fullPath_func,fslVersion)

%
% [pv_mask]=fsl_loadpv4(fullPath_anat,fullPath_func);
% load gray/white/csf partial volume maps
% if not exist, will run fast segmentation on the anatomical data
% for fsl 4.X versions
%
% Author: Chi Wah (Alec) Wong May 1 2013
% /$Id$/

if ~exist('fslVersion','var'); fslVersion = 5; end;
if isempty(fslVersion); fslVersion = 5; end;

% locate the last '+' location
origInd=strfind(fullPath_anat,'+orig');
tlrcInd=strfind(fullPath_anat,'+tlrc');

% default, when no + tag is identified
anatTag=fullPath_anat;
anatEnd='';
% remove the +orig or +tlrc tag and call it funcName2
if isempty(tlrcInd)
    anatTag=fullPath_anat(1:(origInd(end)-1));
    anatEnd=fullPath_anat(origInd(end):end);
elseif isempty(origInd)
    anatTag=fullPath_anat(1:(tlrcInd(end)-1));
    anatEnd=fullPath_anat(tlrcInd(end):end);
end;

% % load the anatomical file header and extract the orientation info
% headinfo =  ReadAFNIHead([fullPath_anat,'.HEAD']);
%
% % create a AFNI convention of defining orientations
% orient_base={'R','L','P','A','I','S'};

% do FAST segmentation and create partial volume maps
% must copy the anat header into the partial volume maps
if ~exist([anatTag,'_pve_csf',anatEnd,'.HEAD'],'file')
    delete('__temp_anat_rai*');
    eval(['!3dresample -orient rai -prefix ./__temp_anat_rai -inset ',fullPath_anat]);
    eval(['!3dAFNItoNIFTI -float -prefix ./__temp_anat_rai __temp_anat_rai',anatEnd]);
    if fslVersion==4
        eval(['!fast -v -ov -od ',anatTag,' __temp_anat_rai ']);
        eval(['!3drefit -orient lai ',anatTag,'_pve_0.nii.gz ',anatTag,'_pve_1.nii.gz ',anatTag,'_pve_2.nii.gz ']);
     
    elseif fslVersion==5 || fslVersion==6
        eval(['!fast -v -o ',anatTag,' __temp_anat_rai ']);
    else
        error('Wrong fsl version (must be either 4 o5 5). \n');
    end;
    
    eval(['!3dresample -master ',fullPath_anat,' -prefix ',anatTag,'_pve_csf',' -inset ',anatTag,'_pve_0.nii.gz ']);
    eval(['!3dresample -master ',fullPath_anat,' -prefix ',anatTag,'_pve_gray',' -inset ',anatTag,'_pve_1.nii.gz ']);
    eval(['!3dresample -master ',fullPath_anat,' -prefix ',anatTag,'_pve_white',' -inset ',anatTag,'_pve_2.nii.gz ']);
    
    delete([anatTag,'_pve*.nii.gz']);
    delete([anatTag,'_seg.nii.gz']);
    delete([anatTag,'.nii']);
    delete('__temp_anat_rai*');
        
    %eval(['!3drefit -orient ',orient_base{headinfo.orient(1)+1},orient_base{headinfo.orient(2)+1},orient_base{headinfo.orient(3)+1},' -duporigin ',anatTag,'+orig ',anatTag,'_pve_csf+orig ',anatTag,'_pve_white+orig ',anatTag,'_pve_gray+orig ']);
end;

% read partial volume information for anatomical
[pv_mask.white]= loadAnat([anatTag, '_pve_white',anatEnd],fullPath_func);
[pv_mask.csf]  = loadAnat([anatTag, '_pve_csf',anatEnd],fullPath_func);
[pv_mask.gray] = loadAnat([anatTag, '_pve_gray',anatEnd],fullPath_func);


