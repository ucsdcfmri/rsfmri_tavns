function [MPA,RPA] = my_avgPA(cardiacData,cardiacPeaks,cycles,TR, srate)
% See paper: (Chang et al. 2008)
%            Influence of heart rate on the BOLD signal:
%            The cardiac response function
% Modified from the avgPA.m in the repo: fmriqa 
% Yixiang Mao, 06/03/2021
% Extension to peak amplitude.
peak_amp   = 0*cardiacPeaks;
peak_amp2  = nan*cardiacPeaks;
peakPoint = find(cardiacPeaks);%store the index in cardiac
for num=2:length(peakPoint)
    if(peakPoint(num-1)>6)
        temp=cardiacData(peakPoint(num-1))-cardiacData(peakPoint(num-1)-6);
        peak_amp(peakPoint(num-1):peakPoint(num)) = temp;
%         peak_amp2(peakPoint(num-1)) = temp;
    end
end
RPA =  downsample(peak_amp,round(srate/(1/TR)))';

mean_window=round(cycles*TR*srate);
Cuts = [];
for n = 0:length(peak_amp);
    if n<mean_window/2
        Cuts(n+1) = mean(peak_amp(1:n+mean_window/2));
    elseif n>length(cardiacPeaks)-mean_window/2
        Cuts(n+1) = mean(peak_amp(n-mean_window/2:end));
    else      
        Cuts(n+1) = mean(peak_amp(n-mean_window/2+1:n+mean_window/2));
    end
end
MPA = downsample(Cuts,round(srate/(1/TR)))';
end
