function [c_peak, mean_freq, numpeaks_expected, numpeaks] = findGECardiacPeak(cardiac,ifplot)
%usage: [c_peak, mean_freq] = findCardiacPeak(cardiac,ifplot)
% finds cardiac peak in cardiac signal
%
% Input:
%    cardiac - cardiac signal
%    ifplot  - [0], 1 to plot figures
%
% Output:
%    c_peak    - cardiac peaks
%    mean_freq - mean frequency
%
% Copyright (c) 2004 CUPID
% Author: kal
% comments: this is a really old function, needs revision - kal
% History:
%  $Id$

% ruler
% --1234567890123456789012345678901234567890123456789012345678901234567890
if ~exist('ifplot') ifplot=0; end

%this part only works well if there is a lot of data
%detrend and smooth
cc=detrend(cardiac); 
cc=sgolayfilt(cc,4,101);%%order 4 smooth using a window of 1 sec roughly the length of a typical heart beat.
[p,f]=pwelch(cc,3000,[],[],100);  %100Hz sampleing rate, 30sec window

c1=[0.25 0.5 0.25]; %what to convolve with
p2=conv(p,c1);
p2=conv(p2,c1);
p2=p2(3:end);
p2=p2(1:size(p,1));
ind_max=find(p2==max(p2));
mean_freq=f(ind_max);
mean_period=1/mean_freq;
mean_window=round(mean_period/(1/100));

maxpoint=zeros(size(p));
maxpoint(ind_max)=p(ind_max);
if ifplot
    figure;
    plot(f(1:ind_max+ind_max*3),p(1:ind_max+ind_max*3));
    hold on;stem(f(1:ind_max+ind_max*3),maxpoint(1:ind_max+ind_max*3),'r');
end


window=mean_window;
window_plus_40=window+round(0.4*window);
window_40percent = round(0.4*window);

i=1; %travel through all data counter (main counter)
%j is a travel through window counter
cond=1;
c_peak=zeros(size(cardiac));
while(cond)
    window_array=[];
    
    if (i==1)
        j=i:( window_plus_40 + (i-1) ); 
        window_array(j) = cardiac(j);
        ind_max=find(window_array(j)==max(window_array(j)));
        ind_max=ind_max(1);
        c_peak(ind_max) = +1;
        i=ind_max+1;
    end
    
    j=( (i-1) + window_40percent ):( window_plus_40 + (i-1) ); %window focus
    window_array =  cardiac(j);
    ind_max=find(window_array==...
        max(window_array));
    ind_max=ind_max(1);
    ind_max=ind_max+(j(1)-1);
    c_peak(ind_max)=+1;
    i=ind_max+1;
    
    if ( (window_plus_40 + (i-1)) > length(cardiac) )
        cond = 0;
    end
end
numpeaks_expected=size(cardiac,1)*.01*mean_freq;
numpeaks=size(find(c_peak),1);
fprintf('numpeaks expected = %6.1f, numpeaks found = %6.1f\n',numpeaks_expected,numpeaks);



