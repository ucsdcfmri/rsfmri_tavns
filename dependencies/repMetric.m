function mB = repMetric(B,metric,dim)
%usage: mB = repMetric(B,metric,dim)
% gets some metric of an ND matrix and repeats the metric over the last dim
% or the specified dim of 2D matrix
%
% Input:
%    B      - ND data
%    metric - default: <'mean'>, others metrics: <'max'>, <'min'>, <'Vnorm'>
%    dim    - 1 or 2, note: this only will work if B is 2D data
%
% Output:
%    mB    - repeated meteric data
%
% Copyright (c) 2004 CUPID
% Author: kal
% History:
%  $Id$

% ruler
% --1234567890123456789012345678901234567890123456789012345678901234567890

%gets some metric of ND matrix and repeats the metric over the last dim or
%specified dim

if nargin==1 metric='mean';dim=0; end
if nargin==2 dim=0; end %dim=0 mean do repMetric on ND matrix

if ~dim
    if isvector(B) B=B(:)'; end
    sB   = size(B);
    T    = sB(end);
    rB   = reshape(B,prod(sB(1:(end-1))),T)';
    iB = getMetric(rB,metric,1);
    mB   = repmat(iB,T,1);
    mB   = reshape(mB',sB);
    if isvector(B) mB=mB'; end
else
    sB   = size(B);
    rB   = B;
    if dim==1 rmi=[sB(dim),1]; else rmi=[1,sB(dim)]; end
    iB = getMetric(rB,metric,dim);
    mB   = repmat(iB,rmi);
end

function iB = getMetric(rB,metric,dim)

switch metric
    case 'mean'
        iB   = mean(rB,dim);
    case 'max'
        iB   = max(rB,[],dim);
    case 'min'
        iB   = min(rB,[],dim);
    case 'Vnorm'
        iB   = Vnorm(rB,dim);
    case 'std'
        iB   = std(rB,[],dim);
end









