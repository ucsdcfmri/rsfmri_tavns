% LookForBoundary() - look for the beginning and ending index of all continuous
%                    segments from a vector of indexes
% Usage:
%   [start_point, end_point] =  LookForBounday(indexes);
% Required Inputs:
%   indexes_vector           - vector of indexes
% Outputs:
%   start_point              - a vector of the start index number of all
%                              continuous segments
%   end_point                - a vector of the ending index number of all
%                              continuous segments
% The two output vectors should have the same length.

function [start_points,end_points] = LookForBoundary(badzones)
if isempty(badzones)
    start_points = [];
    end_points = [];
else
if size(badzones,1)==1
elseif size(badzones,1)~=1 && size(badzones,2)==1
    badzones = badzones.';
else
    disp('Input should be a vector');
end

    end_index = find(diff(badzones)~=1);
    start_index = end_index+1;
    start_points = badzones(start_index);
    end_points = badzones(end_index);
    start_points = start_points-1;
    start_points = [badzones(1) start_points];
    end_points = [end_points badzones(end)];
end
end
