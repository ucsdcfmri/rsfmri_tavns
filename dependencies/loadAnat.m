function [anatData]=loadAnat(fullPath_anat,fullPath_func)

%
% loadAnat(fullPath_anat,fullPath_func)
% load anatomical data from file fullPath_anat
%
% the anatomical data will be resampled to functional
% space before being loaded
%
% CAUTION!!! NEED TO MAKE SURE the anatomical and functional data are acquired in the same
% scanning session!!!! please use AFNI to check if they align well!!!
%
% if not, please use align_epi_anat.py and create a set of anatomical and
% functional data that are aligned and run the processing again!!!!!
%
% Author: Chi Wah (Alec) Wong May 22 2012
% $Id$

if ~exist([fullPath_anat,'.HEAD'],'file')
    error('Could not find anatomical data!');
end;

fprintf('Always good to make sure the anatomical and functional data are aligned =) \n');

%[err,Info, HEADInfo] = BrikInfo([fullPath_anat,'.HEAD']);

delete('__tempAnat_rs*');
eval(['!3dresample -master ',fullPath_func,...
    ' -prefix ./__tempAnat_rs',...
    ' -inset ', fullPath_anat]);

% extract the prefix of the anatomical file
origInd=strfind(fullPath_anat,'+orig');
tlrcInd=strfind(fullPath_anat,'+tlrc');

% default, when no + tag is identified
anatEnd=fullPath_anat;
% remove the +orig or +tlrc tag and call it funcName2
if isempty(tlrcInd)
    anatEnd=fullPath_anat(origInd(end):end);
elseif isempty(origInd)
    anatEnd=fullPath_anat(tlrcInd(end):end);
end;

[err, anatData, ~, ~]     = BrikLoad(['./__tempAnat_rs',anatEnd]); % no ByteSwap
delete('__tempAnat_rs*');


