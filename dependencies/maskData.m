function [dataMat,dcoord]=maskData(data,mask,sliceFlag);
% maskData  - applies a binary mask and gathers data
% 
% Usage: [dataMat]=maskData(data,mask,sliceFlag);
% 
% Input:
%    data         - ND data array
%    mask         - binary mask (ND-1 array)
%    sliceFlag    - flag to do slice level processing
%                   default = 0: reshape N-Dim matrix, returns
%                                single element cell array
%                     
%                             1: slice by slice processing, returns
%                                cell array with nslice elements
% Output:
%    dataMat      - data in 2d (time as rows) corresponding to binary mask
%                       (cell array)
%    dcoord       - the original position (row,column,slice) for each element
%                  
%  
% Restrictions:
%    1.
%    2.
% 
% Notes:
%    1. 
%    2.
% 
% References:
%    1.
% 
% Examples:
%    1. [out11,out21,...] = test(in11,in21,...)
%           shows how to...
% 
% Copyright (c) 2004 CUPID
% Author: yashar      3/29/05
%$Id$


% ruler --1234567890123456789012345678901234567890123456789012345678901234567890
if ~exist('sliceFlag') | isempty(sliceFlag)    sliceFlag=0;   end

sD      = size(data);

if sliceFlag==0
    dataRS  = reshape(data,prod(sD(1:end-1)),sD(end))';
    maskInd = find(mask(:));
    dataMat{1} = dataRS(:,maskInd); 
    %also return a vector of coordinates; assume a 4D matrix
    if nargout > 1
      if length(sD) == 4
	dcoord =[];
	for k = 1:sD(3);
	  [r,c] = find(mask(:,:,k));
	  dcoord =[dcoord;r c k*ones(length(r),1)];
	end
      end
    end
else
    
    for i=1:sD(3)
        dTemp   = squeeze(data(:,:,i,:));
        mTemp   = squeeze(mask(:,:,i));
        sDt     = size(dTemp);
        dataRS  = reshape(dTemp,prod(sDt(1:end-1)),sDt(end))';
        maskInd = find(mTemp(:));
        dataMat{i} = dataRS(:,maskInd); 
    end
       
end


















