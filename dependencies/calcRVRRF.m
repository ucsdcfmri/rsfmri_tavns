function [RV, RVRRF, RRF, L_resp] = calcRVRRF(raw_resp, resp_srate, TR, seg_len)
%
% calculate the RVRRF regressor (see Chang, et al., NIMG, 2009)
% - input
% raw_resp      - the raw respiration signal
% resp_srate    - the sampling rate of the raw respiration signal in Hz.
% TR            - the repetition time of the corresponding functional data,
%                 or 1 over the sampling rate of the output timeseries
% seg_len       - the length of the segment used to calculate RV and HR (unit: sec)

% - output 
% RV        - respiration volume (RV, unitless)
% RVRRF     - RV convolved with the respiration response function (RRF, derived in Birn, et al., NIMG 2008)
% RVF       - the respiration response function
% Author: Yixiang Mao Apr 29 2021
% $Id$

% defualt respiration sampling rate: 25 Hz
if isempty(resp_srate); resp_srate = 25; end

% defualt segment length: 6 sec
if isempty(seg_len);
seg_len = 6; % use 6-sec long segment to calculate HR]
end

resp_sample_per_TR = resp_srate*TR; % number of respiration timepoints per TR

% the maximum number of TRs, it will equal to the length of the output data
num_TRs_in_raw_resp = floor(length(raw_resp)/resp_sample_per_TR);
a_raw_resp = mod(length(raw_resp), resp_sample_per_TR); % length(raw_resp) = (num_TRs_in_raw_resp-1)*resp_sample_per_TR+a

% Let the respiration segment length be L timepoints
% let L = 2k+1;
% calculate k from the segment length in sec
k_resp = round(seg_len*resp_srate/2);
L_resp = 2*k_resp+1; % not used

% calculate the RV
RV = zeros(num_TRs_in_raw_resp, 1);
for idx = 1:num_TRs_in_raw_resp
    % determine the center index of the segment
    seg_center_idx = a_raw_resp+(idx-1)*resp_sample_per_TR+round(TR*resp_srate/2);
    % determine the start and end indices of the segment 
    seg_start_idx = round(max(1, seg_center_idx-k_resp));
    seg_end_idx = round(min(length(raw_resp), seg_center_idx+k_resp));
    
    RV(idx) = std(raw_resp(seg_start_idx:seg_end_idx));

end
% form RRF
t=0:TR:40;
RRF =0.6*(t.^2.1).*exp(-t/1.6) - 0.0023*(t.^3.54).*exp(-t/4.25);
RRF= RRF/std(RRF);
% calculate RVRRF
RVRRF = conv(RRF, RV);
RVRRF = RVRRF(1:length(RV));
end


