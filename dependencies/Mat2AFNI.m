function [err,info] = Mat2AFNI(data,prefix,refitBrik,dataType,afniView)
% [err,info] = Mat2AFNI(4D-data,'test','refitBrik+orig',dataType,afniView)
% Inputs:
%   data:   4D/3D dataset
%   prefix
%   refitBrik:  'sth+orig/+tlrc'
%   dataType:   0 byte/ 1 short/ 2 int / 3 float, (ref BrikInfo.m); 
%   afniView: '+orig'/'+tlrc'/'+acpd'; you don't need to speficy this only
%       if it is included in refitBrik's name
%  
% This code helps to write mat data into afni brik files.  As an
% alternative of mat2Brik, this routine handles different views and also
% different data types.  However, the code itself won't scale input data.
% Two matlab_afni function will be called: BrikInfo and WriteBrik.
% Author: Hongjian He, 11/02/09.

% store data as: 0 byte/ 1 short/ 2 int / 3 float, (ref BrikInfo.m); 
if ~exist('dataType','var') || isempty(dataType); dataType = 1; end;
% view
if ~exist('afniView','var') || isempty(afniView); afniView = refitBrik(end-4:end); end;
    
[err,info] = BrikInfo(refitBrik);       

info.RootName = ''; %that'll get set by WriteBrik
info.DATASET_RANK(2) = size(data,4); % sub-bricks

if length(dataType)==1; dataType = dataType*ones(size(data,4),1); end;
info.BRICK_TYPES = dataType; 

info.BRICK_STATS = []; % automatically set
info.BRICK_FLOAT_FACS = [];%automatically set
info.BRICK_LABS = '';    % name for each sub-brick
info.IDCODE_STRING = '';%automatically set
info.BRICK_STATAUX = [];
info.TAXIS_NUMS = [];
%% opt
opt.Scale = 0;  % don't scale
opt.Prefix = prefix;   % name of output
opt.verbose = 0;    
opt.View = afniView; % view

[err,errm,info] = WriteBrik(data,info,opt);   % write file