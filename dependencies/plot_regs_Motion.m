function plot_regs_Motion(reg_motion, subj_info_str, line_width, label_size)
% generate the plot showing the motion parameters
% 1- to 6-th columns in motion-parameter matrix correspond to
% z-rotation, x-rotation, y-rotation, z-translation, x-translation and y-translation
figure2('Renderer', 'painters', 'Position', [10, 10, 1600, 900])
subplot(2, 1, 1) % translational motion parameters
plot(reg_motion(:, 5), 'LineWidth', line_width) % x-translation
hold on
plot(reg_motion(:, 6), 'LineWidth', line_width) % y-translation
plot(reg_motion(:, 4), 'LineWidth', line_width) % z-translation
grid on
hold off
xlabel('volume', 'FontWeight', 'bold', 'FontSize', label_size)
ylabel('motion parameters', 'FontWeight', 'bold', 'FontSize', label_size)
legend('x', 'y', 'z', 'FontWeight', 'bold', 'FontSize', label_size, 'Location', 'best')
title('translational motion parameters', 'FontWeight', 'bold', 'FontSize', label_size)

subplot(2, 1, 2) % rotational motion parameters
plot(reg_motion(:, 2), 'LineWidth', line_width) % x-rotation
hold on
plot(reg_motion(:, 3), 'LineWidth', line_width) % y-rotation
plot(reg_motion(:, 1), 'LineWidth', line_width) % z-rotation
grid on
hold off
xlabel('volume', 'FontWeight', 'bold', 'FontSize', label_size)
ylabel('motion parameters', 'FontWeight', 'bold', 'FontSize', label_size)
legend('x', 'y', 'z', 'FontWeight', 'bold', 'FontSize', label_size, 'Location', 'best')
title('rotational motion parameters', 'FontWeight', 'bold', 'FontSize', label_size)

% subplot(2, 2, 3) % the first derivatives of the translational motion parameters
% plot(reg_motion(:, 11), 'LineWidth', line_width) % x-translation
% hold on
% plot(reg_motion(:, 12), 'LineWidth', line_width) % y-translation
% plot(reg_motion(:, 10), 'LineWidth', line_width) % z-translation
% grid on
% hold off
% xlabel('volume', 'FontWeight', 'bold', 'FontSize', label_size)
% ylabel('the first derivatives', 'FontWeight', 'bold', 'FontSize', label_size)
% legend('x', 'y', 'z', 'FontWeight', 'bold', 'FontSize', label_size, 'Location', 'best')
% title('the first derivatives of the translational motion parameters', 'FontWeight', 'bold', 'FontSize', label_size)
% 
% subplot(2, 2, 4) % the first derivatives of therotational motion parameters
% plot(reg_motion(:, 8), 'LineWidth', line_width) % x-rotation
% hold on
% plot(reg_motion(:, 9), 'LineWidth', line_width) % y-rotation
% plot(reg_motion(:, 7), 'LineWidth', line_width) % z-rotation
% grid on
% hold off
% xlabel('volume', 'FontWeight', 'bold', 'FontSize', label_size)
% ylabel('motion parameters', 'FontWeight', 'bold', 'FontSize', label_size)
% legend('x', 'y', 'z', 'FontWeight', 'bold', 'FontSize', label_size, 'Location', 'best')
% title('the first derivatives of the rotational motion parameters', 'FontWeight', 'bold', 'FontSize', label_size)

sgtitle({['The 6 motion parameters'];
    subj_info_str}, 'FontWeight', 'bold', 'FontSize', label_size)

end

