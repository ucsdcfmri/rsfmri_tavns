function B = grow3d(A, seedP,cMode);
%usage: B = grow3d(A, seedP,cMode);
% grows a 3d volume (starting from a seed voxel)
%
% Input:
%    A       - binary input matrix
%    seedP   - seed point
%    cMode   - cluster mode: 1- touching faces, 2-touching faces/edges
%                   3- touching faces/edges/coners
%
% Output:
%    B       - 3d volume of ones connected to seedP
%
% Copyright (c) 2006 CUPID
% Author: kal
% History:
%  $Id$

% ruler
% --1234567890123456789012345678901234567890123456789012345678901234567890

A = single(A);

%pad with NaNs
A=grow(A);
A(1,:,:)=NaN;A(end,:,:)=NaN;A(:,1,:)=NaN;A(:,end,:)=NaN;A(:,:,1)=NaN;A(:,:,end)=NaN;

if A(seedP(1), seedP(2), seedP(3));
    A(seedP(1), seedP(2), seedP(3)) = NaN;
    gC{1} = sub2ind(size(A), seedP(1), seedP(2), seedP(3));

    cc = 1;

    while 1

        cc = cc+1;
        gC{cc} = [];

        [x, y, z] = ind2sub(size(A), gC{cc-1});
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%% touching voxels via face

        ob = nonzeros((A(sub2ind(size(A), x, y, z-1)) == 1).*sub2ind(size(A), x, y, z-1));
        A(ob) = NaN;
        gC{cc} = [gC{cc} ob'];

        ob = nonzeros((A(sub2ind(size(A), x, y, z+1)) == 1).*sub2ind(size(A), x, y, z+1));
        A(ob) = NaN;
        gC{cc} = [gC{cc} ob'];

        ob = nonzeros((A(sub2ind(size(A), x-1, y, z)) == 1).*sub2ind(size(A), x-1, y, z));
        A(ob) = NaN;
        gC{cc} = [gC{cc} ob'];

        ob = nonzeros((A(sub2ind(size(A), x+1, y, z)) == 1).*sub2ind(size(A), x+1, y, z));
        A(ob) = NaN;
        gC{cc} = [gC{cc} ob'];

        ob = nonzeros((A(sub2ind(size(A), x, y-1, z)) == 1).*sub2ind(size(A), x, y-1, z));
        A(ob) = NaN;
        gC{cc} = [gC{cc} ob'];

        ob = nonzeros((A(sub2ind(size(A), x, y+1, z)) == 1).*sub2ind(size(A), x, y+1, z));
        A(ob) = NaN;
        gC{cc} = [gC{cc} ob'];

        if cMode==2 | cMode==3

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%, touching voxels via edges

            %%%%%%%%%%%%%%%%%, in plane voxels

            ob = nonzeros((A(sub2ind(size(A), x+1, y-1, z)) == 1).*sub2ind(size(A), x+1, y-1, z));
            A(ob) = NaN;
            gC{cc} = [gC{cc} ob'];

            ob = nonzeros((A(sub2ind(size(A), x+1, y+1, z)) == 1).*sub2ind(size(A), x+1, y+1, z));
            A(ob) = NaN;
            gC{cc} = [gC{cc} ob'];

            ob = nonzeros((A(sub2ind(size(A), x-1, y+1, z)) == 1).*sub2ind(size(A), x-1, y+1, z));
            A(ob) = NaN;
            gC{cc} = [gC{cc} ob'];

            ob = nonzeros((A(sub2ind(size(A), x-1, y-1, z)) == 1).*sub2ind(size(A), x-1, y-1, z));
            A(ob) = NaN;
            gC{cc} = [gC{cc} ob'];

            %%%%%%%%%%%%%%%%%, lower plane voxels

            ob = nonzeros((A(sub2ind(size(A), x, y-1, z-1)) == 1).*sub2ind(size(A), x, y-1, z-1));
            A(ob) = NaN;
            gC{cc} = [gC{cc} ob'];

            ob = nonzeros((A(sub2ind(size(A), x, y+1, z-1)) == 1).*sub2ind(size(A), x, y+1, z-1));
            A(ob) = NaN;
            gC{cc} = [gC{cc} ob'];

            ob = nonzeros((A(sub2ind(size(A), x-1, y, z-1)) == 1).*sub2ind(size(A), x-1, y, z-1));
            A(ob) = NaN;
            gC{cc} = [gC{cc} ob'];

            ob = nonzeros((A(sub2ind(size(A), x+1, y, z-1)) == 1).*sub2ind(size(A), x+1, y, z-1));
            A(ob) = NaN;
            gC{cc} = [gC{cc} ob'];

            %%%%%%%%%%%%%%%%%, upper plane voxels

            ob = nonzeros((A(sub2ind(size(A), x, y-1, z+1)) == 1).*sub2ind(size(A), x, y-1, z+1));
            A(ob) = NaN;
            gC{cc} = [gC{cc} ob'];

            ob = nonzeros((A(sub2ind(size(A), x, y+1, z+1)) == 1).*sub2ind(size(A), x, y+1, z+1));
            A(ob) = NaN;
            gC{cc} = [gC{cc} ob'];

            ob = nonzeros((A(sub2ind(size(A), x-1, y, z+1)) == 1).*sub2ind(size(A), x-1, y, z+1));
            A(ob) = NaN;
            gC{cc} = [gC{cc} ob'];

            ob = nonzeros((A(sub2ind(size(A), x+1, y, z+1)) == 1).*sub2ind(size(A), x+1, y, z+1));
            A(ob) = NaN;
            gC{cc} = [gC{cc} ob'];

        end
        
        if cMode==3

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%, touching voxels via corners

            %%%%%%%%%%%%%%%%%, lower plane voxels

            ob = nonzeros((A(sub2ind(size(A), x+1, y-1, z-1)) == 1).*sub2ind(size(A), x+1, y-1, z-1));
            A(ob) = NaN;
            gC{cc} = [gC{cc} ob'];

            ob = nonzeros((A(sub2ind(size(A), x+1, y+1, z-1)) == 1).*sub2ind(size(A), x+1, y+1, z-1));
            A(ob) = NaN;
            gC{cc} = [gC{cc} ob'];

            ob = nonzeros((A(sub2ind(size(A), x-1, y+1, z-1)) == 1).*sub2ind(size(A), x-1, y+1, z-1));
            A(ob) = NaN;
            gC{cc} = [gC{cc} ob'];

            ob = nonzeros((A(sub2ind(size(A), x-1, y-1, z-1)) == 1).*sub2ind(size(A), x-1, y-1, z-1));
            A(ob) = NaN;
            gC{cc} = [gC{cc} ob'];

            %%%%%%%%%%%%%%%%%, upper plane voxels

            ob = nonzeros((A(sub2ind(size(A), x+1, y-1, z+1)) == 1).*sub2ind(size(A), x+1, y-1, z+1));
            A(ob) = NaN;
            gC{cc} = [gC{cc} ob'];

            ob = nonzeros((A(sub2ind(size(A), x+1, y+1, z+1)) == 1).*sub2ind(size(A), x+1, y+1, z+1));
            A(ob) = NaN;
            gC{cc} = [gC{cc} ob'];

            ob = nonzeros((A(sub2ind(size(A), x-1, y+1, z+1)) == 1).*sub2ind(size(A), x-1, y+1, z+1));
            A(ob) = NaN;
            gC{cc} = [gC{cc} ob'];

            ob = nonzeros((A(sub2ind(size(A), x-1, y-1, z+1)) == 1).*sub2ind(size(A), x-1, y-1, z+1));
            A(ob) = NaN;
            gC{cc} = [gC{cc} ob'];

        end


        if isempty(gC{cc});
            break;
        end
    end
end

b = cell2mat(gC);
b = sort(b,2);

B = logical(zeros(size(A)));
B(b) = 1;

B=B(2:end-1,2:end-1,2:end-1);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function Y = grow(varargin)
%
% GROW    Expands a matrix.
%
%   Y = GROW(X,[M1 M2 ... Mn]) creates a matrix 'Y' by the expansion
%   of the matrix 'x' of Mi elements on directions i e -i. The size
%   of the new matrix 'Y' will be:
%       size(Y) = size(x) + 2*[M]
%
%   Example: X = [ 1 2 3; 4 5 6; 7 8 9]
%   Y = GROW(X,[1 0])
%   Y = [1 2 3; 1 2 3; 4 5 6; 7 8 9; 7 8 9]
%
%   Y = GROW(X,[1 1])
%   Y = [1 1 2 3 3; 1 1 2 3 3; 4 4 5 6 6; 7 7 8 9 9; 7 7 8 9 9]
%
%   Y = GROW(X) uses as standard Mi = 1.
%
%    If DIM(X)>1, GROW(X,N) = GROW(X,N*ones(DIM(x)))
%
%   Y = GROW(X,[M], 'a') performs assimetric expansion. If Mi is even
%   assimetric expansion is the same as simmetric expansion, execpt that
%   the expansion in the assimetric mode will be half of the one in the
%   simmetric mode. If Mi is odd, the matrix will be expanded ceil(Mi/2)
%   elements in the -i direction and floor(Mi/2) in the i. The size of
%   the expanded matrix will be:  %       size(y) = size(X) + [M]
%
%  [M] can contain negative integers. In this case, the outer elements
%  of X will be discarded.
%
%    See also ROLL, PAD, PADC, SHRINK.
%


[X, M, assimetric] = parse_inputs(varargin{:});

ind = cell(length(M),1);

if assimetric == 0
    for i = 1 : length(M)
        if M(i) >= 0
            ind{i} = [repmat(1,[1 M(i)]) 1:size(X,i) repmat(size(X,i),[1 M(i)]) ];
        else
            ind{i} = 1+(-M(i)) : size(X,i)-(-M(i)); % using -M(i) to change sign
        end
    end
else % if not assimetric
    for i = 1 : length(M)
        if M(i) >= 0
            ind{i} = [repmat(1,[1 ceil(M(i)/2)]) 1:size(X,i) repmat(size(X,i),[1 floor(M(i)/2)]) ];
        else
            ind{i} = 1+ceil(-M(i)/2) : size(X,i) - floor(-M(i)/2);
        end
    end
end
Y = X(ind{:});


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [X,M,assimetric] = parse_inputs(varargin)
assimetric = 0;

switch nargin
    case 0
        error('Too few inputs!')
        return
    case 1
        X = varargin{1};
        M = ones(1,ndims(X));
    case 2
        X = varargin{1};
        M = varargin{2};
        if strcmp(M,'a')
            M = ones(1,ndims(X));
            assimetric = 1;
        end
    case 3
        X = varargin{1};
        M = varargin{2};
        if ~strcmp('a', lower(varargin{3}))
            error('Unknown parameter.');
        end
        assimetric = 1;

    case 4
        error('Too many inputs!')
        return
end

if length(M)==1
    if sum(size(X)>1)>1
        M = M*ones(1,ndims(X));
    elseif ndims(X)==2 & size(X,1)==1 & size(X,2)>1
        M = [0 M];
    elseif ndims(X)==2 & size(X,1)>1 & size(X,2)==1
        M = [M 0];
    end
end

if length(M)~=ndims(X)
    error('Invalid dimensions')
end

