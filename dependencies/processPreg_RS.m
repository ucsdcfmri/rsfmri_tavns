function [funcDataCorr,doff]=processPreg_RS(funcData,regressor,brain_roi,lp,TR)

% 
% reg_WMcsf=processPreg_RS(funcData,regressor,roi_global)
%
% regress out nuisance terms
% input
% regressor - nuisance terms to be projected out using linear regression
% brain_roi - whole brain mask
% 
% 
% Author: Chi Wah (Alec) Wong May 22 2012
% $Id$

funcData2D=reshape(funcData,size(funcData,1)*size(funcData,2)*size(funcData,3),size(funcData,4));
funcData2=transpose(funcData2D(brain_roi>0,:));

% de-mean the data
meanData = repmat(mean(funcData2),size(funcData2,1),1);
funcData2  = funcData2 - meanData;

% peform regression: regress out all nuisance regressors
if ~isempty(regressor)
    beta2_corr=funcData2-regressor*inv(regressor'*regressor)*regressor'*funcData2;
else
    beta2_corr=funcData2;
end;

% low pass filtering
if lp>0;
    b = designfilt('lp',lp,1/TR) ;
    beta2_corr  = applyfilt(beta2_corr,b);
end;

% add the mean back
funcDataCorr=zeros(size(funcData2D));
funcDataCorr(brain_roi>0,:)  = beta2_corr' + meanData';

% reshape it back to 4D
funcDataCorr=reshape(funcDataCorr,size(funcData,1),size(funcData,2),size(funcData,3),size(funcData,4));

% calculate degrees of freedom
if lp>0
    doff=round((size(funcData,4)-size(regressor,2))/(0.5/TR/lp));
else
    doff=size(funcData,4)-size(regressor,2);
end;
