% Plots an image with an underlay and an overlay. This assumes that you
% already have a figure open.
function allclusters_keep = imageoverlay(underlay,overlay,underlayrange,overlayrange,nrow,ncol,tthresh,cthresh,mask)
% underlay - the t-statistic map
% trange - the range of t-statistics to plot
% nrow - the number of rows in the montage
% ncol - the number of columns in the montage
% tthresh - the t-statistic threshold
% cthresh - the cluster-size threshold


%numColors = 256; % number of colors in colormap

% ----------------- Generating true-color images to plot ---------------- %
underlaymontages = getcolormontages(underlay,nrow,ncol,underlayrange);
overlaymontages = getcolormontages(overlay,nrow,ncol,overlayrange);

underlaytouse = underlaymontages.gray;
overlaytouse = overlaymontages.jet;
overlaycmap = overlaymontages.jetmap;

% ------ Applying t-statistic (p-value) and cluster-size thresholds ----- %

% Calculating additional maps
if numel(tthresh) == 1 % example, a t-statistic threshold that applies on both positive and negative sides
    overlay_significant_up_mask = (overlay >= tthresh); % mask of significant up voxels
    overlay_significant_down_mask = (overlay <= -tthresh); % mask of significant down voxels
elseif numel(tthresh) == 2
    overlay_significant_up_mask = (overlay >= tthresh(2)); % mask of significant up voxels
    overlay_significant_down_mask = (overlay <= tthresh(1)); % mask of significant down voxels
end

% Setting voxels outside the mask to minimum of range for visualizing
if exist('mask','var') && ~isempty(mask)
    overlay_significant_up_mask(mask==0) = 0; % mask of significant up voxels
    overlay_significant_down_mask(mask==0) = 0; % mask of significant down voxels
end

% This MATLAB function will find connected components in binary image.
% https://www.mathworks.com/help/images/ref/bwconncomp.html
connectivity = 6; % 6=NN1, 18=NN2, 26=NN3
upclusters = bwconncomp(overlay_significant_up_mask,connectivity);
downclusters = bwconncomp(overlay_significant_down_mask,connectivity);

% Applying the threshold
upclusters_keep = thresholdclusters(upclusters.PixelIdxList,cthresh);
downclusters_keep = thresholdclusters(downclusters.PixelIdxList,cthresh);
allclusters_keep = [upclusters_keep, downclusters_keep];



% ------------------------------- Plotting ------------------------------ %

% Plotting the underlay
h1 = image(underlaytouse);

hold on;

% Plotting the overlay
h2 = image(overlaytouse);

% Clusters are based on 3D volume. Identify their pixels in 3D and then
% construct the 2D montage for alphamapping.
alphamapping_vol = zeros(size(overlay));
% Looping through kept clusters and making opaque
for i=1:1:length(allclusters_keep)
    alphamapping_vol(allclusters_keep{i}) = 1; % setting voxel opacities to 1 via the linear indices
end
alphamapping = vol2montage(alphamapping_vol,nrow,ncol);
set(h2, 'AlphaData', alphamapping);

[nx,ny,~] = size(underlay);
xticklocs = (0:nx:ncol*nx) + nx/2;
yticklocs = (0:ny:nrow*ny) + ny/2;
set(gca,'XTick',xticklocs,'XTickLabels',[],...
    'YTick',yticklocs,'YTickLabels',[]);

hold off;

%{
colormap(gray(numColors));
c = colorbar;
trange = underlayrange;
c.TickLabels = trange(1):((trange(2)-trange(1))/10):trange(2); % span the entire trange in ten increments
%}

%colormap('jet');
colormap(overlaycmap);
c = colorbar;
trange = overlayrange;
c.TickLabels = trange(1):((trange(2)-trange(1))/10):trange(2); % span the entire trange in ten increments


axis image;



end



%  get colored montages
function imagemontages = getcolormontages(image,nrow,ncol,range)

numColors = 256; % number of colors in colormap

% Converting image to a image montage
image_montage = vol2montage(image,nrow,ncol); % local function

% Converting matrix to grayscale within the t-stat range of [-3 3]
% https://www.mathworks.com/help/images/ref/mat2gray.html
image_montage_gray = mat2gray(image_montage,range);

% Converts the grayscale image into an indexed image
% https://www.mathworks.com/help/matlab/creating_plots/image-types.html#f2-155
[image_montage_indexed,~] = gray2ind(image_montage_gray,numColors);

% Converting indexed images to true color images
image_montage_truecolor_gray = ind2rgb(image_montage_indexed,gray(numColors));
% https://www.mathworks.com/help/matlab/ref/brighten.html#d120e103490
newjetmap = brighten(jet(numColors),0.1);
image_montage_truecolor_jet = ind2rgb(image_montage_indexed,newjetmap);

% Black-white colormap
bwmap = zeros(size(gray(numColors)));
bwmap(numColors/2+1:end,:) = 1;
image_montage_truecolor_bw = ind2rgb(image_montage_indexed,bwmap);

% Returning the montages in the various colormaps
imagemontages.gray = image_montage_truecolor_gray;
imagemontages.jet = image_montage_truecolor_jet;
imagemontages.bw = image_montage_truecolor_bw;

imagemontages.jetmap = newjetmap;


end


% Code taken from smontage. Takes a 3D matrix and smears its axial slices
% out into a 2D slice montage.
function montage = vol2montage(x,nr,nc)

x = permute(x,[2 1 3]);
[nx,ny,nz] = size(x);
%xmin = min(min(min(x)));
%xmax = max(max(max(x)));
mx = NaN*ones(nr*nx,nc*ny);
rows = 1:nx;cols = 1:ny;
extra = nr*nc-nz;
xc = cat(3,x,NaN*ones(nx,ny,extra));
for ir = 0:(nr-1)
    for ic = 0:(nc-1)
        k= ir*nc + ic + 1;
        mx(rows+ir*nx,cols+ic*ny) = xc(:,:,k);
    end
end

montage = mx;

end


% Intakes a cell, each entry holding the linear indices of a given cluster,
% and a cluster size threshold.
function passed = thresholdclusters(clusters,sizethreshold)

numclusters = length(clusters);

% Initializing variables
passed = {};
count = 0;

% Loop through all clusters, keeping the ones that pass the size threshold
for iClust=1:1:numclusters
    cluster = clusters{iClust};
    clustersize = length(cluster);
    if clustersize >= sizethreshold
        passed{count+1} = cluster;
        count = count + 1;
    else
        % continue to test the next cluster
    end
end

end