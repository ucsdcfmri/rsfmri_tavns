function [HR, HRCRF, CRF, c_peak, L_card] = calcHRCRF(raw_card, card_srate, TR, seg_len)
%
% calculate the HRCRF regressor (see Chang, et al., NIMG, 2009)
% - input
% raw_card      - the raw cardiac signal
% card_srate    - the sampling rate of the raw cardiac signal in Hz.
% TR            - the repetition time of the corresponding functional data,
%                 or 1 over the sampling rate of the output timeseries
% seg_len       - the length of the segment used to calculate RV and HR (unit: sec)

% - output 
% HR        - heart rate (HR, unit: beats-per-minute)
% HRCRF     - HR convolved with the cardiac response function (CRF, derived in Chang, et al., NIMG, 2009)
% CRF       - the cardiac response function
% c_peak    - the location of the cardiac peaks.
% Author: Yixiang Mao Apr 29 2021
% $Id$

% defualt cardiac sampling rate: 100 Hz
if isempty(card_srate); card_srate = 100; end

% defualt segment length: 6 sec
if isempty(seg_len);
seg_len = 6; % use 6-sec long segment to calculate HR]
end

card_sample_per_TR = card_srate*TR; % number of cardiac timepoints per TR

% the maximum number of TRs, it will equal to the length of the output data
num_TRs_in_raw_card = floor(length(raw_card)/card_sample_per_TR); 
a_raw_card = mod(length(raw_card), card_sample_per_TR); % length(raw_card) = (num_TRs_in_raw_card-1)*card_sample_per_TR+a

% Let the cardiac segment length be L timepoints
% let L = 2k+1;
% calculate k from the segment length in sec
k_card = round(seg_len*card_srate/2);
L_card = 2*k_card+1; % not used
% detect the cardiac peaks
[c_peak, mean_freq, numpeaks_expected, numpeaks] = findGECardiacPeak(raw_card,0);
% calculate the HR
HR = zeros(num_TRs_in_raw_card, 1);
for idx = 1:num_TRs_in_raw_card
    % determine the center index of the segment
    seg_center_idx = a_raw_card+(idx-1)*card_sample_per_TR+round(TR*card_srate/2);
    % determine the start and end indices of the segment 
    seg_start_idx = round(max(1, seg_center_idx-k_card));
    seg_end_idx = round(min(length(raw_card), seg_center_idx+k_card));
    % calculate HR
    HR(idx) = 60/mean(diff(find(c_peak(seg_start_idx:seg_end_idx)))./card_srate);
end
% form CRF
t  = 0:TR:30; 
CRF = 0.6*(t.^2.7).*exp(-t/1.6) - (16/sqrt(2*9*pi))*exp(-(t-12).^2 / 18);
CRF = CRF/std(CRF);
% calculate HRCRF
HRCRF = conv(HR, CRF);
HRCRF = HRCRF(1:length(HR));
end


