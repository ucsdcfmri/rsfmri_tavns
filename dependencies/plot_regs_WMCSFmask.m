function plot_regs_WMCSFmask(underlay_anat_path, funcData_firstSlice,...
    AnatMask_white, AnatMask_csf, brain_mask_orig, ...
    slices_plot, nrow, ncol, GLM, subj_info_str, line_width, label_size)
% Plot the WM and CSF masks used to generate the WM and CSF regressors.
% The masks are overlaid on the anatomical images.

% load the anatomical image that was aligned to the functional data
[~, anat_aligned2func, ~, ~] = BrikLoad (underlay_anat_path);
% plot settings
tthresh = 1e-6;
cthresh = 0;
mask = logical(brain_mask_orig(:, :, slices_plot));
figure_position = [10 10 2800 1000];
figure2('Name', subj_info_str, 'Renderer', 'painters', 'Position', figure_position)
ha = tight_subplot(1,3,[.01 .03],[.1 .1],[.01 .01]);

% subplot(1, 3, 1)
axes(ha(1))
% Plot 1: 
% The overlay is the functional data.
% The underlay is the anatomical image aligned to the functional data.

% overlay
overlayrange = [min(min(min(funcData_firstSlice))), max(max(max(funcData_firstSlice)))];
overlay = funcData_firstSlice(:, :, slices_plot);
% underlay
underlay = anat_aligned2func(:, :, slices_plot);
underlayrange = [min(min(min(anat_aligned2func))), max(max(max(anat_aligned2func)))];

imageoverlay(underlay,overlay,underlayrange,overlayrange,nrow,ncol,tthresh,cthresh,mask);
title({['Overlay: the functional data (the first TR)'];
    ['Underlay: the anatomical image that was aligned to the functional data'];
    },...
    'fontweight','bold','fontsize',10);

% subplot(1, 3, 2)
axes(ha(2))
% Plot 2: WM and CSF masks used to generate the WM and CSF regressors.
% The overlay is the WM and CSF maps used to calculate the WM and CSF
% regressors.
% The underlay is the anatomical image aligned to the functional data.

% overlay
overlayrange = [0, 3];
tmp = zeros(size(brain_mask_orig));
tmp(logical(AnatMask_white)) = 2; % assign 2 to white matter
tmp(logical(AnatMask_csf)) = 3; % assign 3 to CSF
overlay = tmp(:, :, slices_plot);
% underlay
underlay = anat_aligned2func(:, :, slices_plot);
underlayrange = [min(min(min(anat_aligned2func))), max(max(max(anat_aligned2func)))];

imageoverlay(underlay,overlay,underlayrange,overlayrange,nrow,ncol,tthresh,cthresh,mask);
title({['Overlay: the WM and CSF masks used to generate the WM and CSF regressors'];
    ['Underlay: the anatomical image that was aligned to the functional data']
    ['2: white matter mask with ', num2str(nnz(AnatMask_white)),...
    ' voxels (pv map>', num2str(GLM.WMCSF_pv_thres(1)),...
    ', erosion: ', num2str(GLM.WMCSF_erosionNum(1)), ' voxel(s))', ...
    ', cluster-size: ', num2str(GLM.WMCSF_ClustSize(1)), ...
    ', cluster-mode: NN', num2str(GLM.WMCSF_ClustMode(1))];
    ['3: CSF mask with ', num2str(nnz(AnatMask_csf)),...
    ' voxels (pv map>', num2str(GLM.WMCSF_pv_thres(2)),...
    ', erosion: ', num2str(GLM.WMCSF_erosionNum(2)), ' voxel(s))', ...
    ', cluster-size: ', num2str(GLM.WMCSF_ClustSize(2)), ...
    ', cluster-mode: NN', num2str(GLM.WMCSF_ClustMode(2))];
    },...
    'fontweight','bold','fontsize',10);

% subplot(1, 3, 3)
axes(ha(3))
% Plot 3: WM and CSF masks used to generate the WM and CSF regressors.
% The overlay is the WM and CSF maps used to calculate the WM and CSF
% regressors.
% The underlay is the functional data.

% overlay
overlayrange = [0, 3];
tmp = zeros(size(brain_mask_orig));
tmp(logical(AnatMask_white)) = 2; % assign 2 to white matter
tmp(logical(AnatMask_csf)) = 3; % assign 3 to CSF
overlay = tmp(:, :, slices_plot);
% underlay
underlay = funcData_firstSlice(:, :, slices_plot);
underlayrange = [min(min(min(funcData_firstSlice))), max(max(max(funcData_firstSlice)))];

imageoverlay(underlay,overlay,underlayrange,overlayrange,nrow,ncol,tthresh,cthresh,mask);
title({['Overlay: the WM and CSF masks used to generate the WM and CSF regressors'];
    ['Underlay: the functional data (the first TR)']
    ['2: white matter mask with ', num2str(nnz(AnatMask_white)),...
    ' voxels (pv map>', num2str(GLM.WMCSF_pv_thres(1)),...
    ', erosion: ', num2str(GLM.WMCSF_erosionNum(1)), ' voxel(s))', ...
    ', cluster-size: ', num2str(GLM.WMCSF_ClustSize(1)), ...
    ', cluster-mode: NN', num2str(GLM.WMCSF_ClustMode(1))];
    ['3: CSF mask with ', num2str(nnz(AnatMask_csf)),...
    ' voxels (pv map>', num2str(GLM.WMCSF_pv_thres(2)),...
    ', erosion: ', num2str(GLM.WMCSF_erosionNum(2)), ' voxel(s))', ...
    ', cluster-size: ', num2str(GLM.WMCSF_ClustSize(2)), ...
    ', cluster-mode: NN', num2str(GLM.WMCSF_ClustMode(2))];
    },...
    'fontweight','bold','fontsize',10);

%sgtitle(subj_info_str, 'fontweight','bold','fontsize',10)
end

