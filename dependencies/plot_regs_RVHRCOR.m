function plot_regs_RVHRCOR(reg_RVHRCOR, raw_card, c_peak, card_srate, HR,...
    raw_resp, resp_srate, RV, nvol, TR, subj_info_str, line_width, label_size)
% plot raw physio signals, HR, RV, HRCRF and RVCRF
figure2('Renderer', 'painters', 'Position', [10, 10, 2500, 900])
subplot(4, 1, 1) % raw cardiac signal
PPG_data_to_plot = raw_card(end-nvol*TR*card_srate+1:end);
c_peak_to_plot = c_peak(end-nvol*TR*card_srate+1:end);
t = (1:length(PPG_data_to_plot))/card_srate;
plot(t, PPG_data_to_plot) % raw signal
hold on
scatter(t(logical(c_peak_to_plot)), PPG_data_to_plot(logical(c_peak_to_plot)), '*') % detected peaks
grid on
hold off
xlabel('time (sec)', 'FontWeight', 'bold', 'FontSize', label_size)
xlim([t(1), t(end)])
ylabel('raw cardiac', 'FontWeight', 'bold', 'FontSize', label_size)
legend('raw cardiac', 'detected peaks', 'FontWeight', 'bold', 'FontSize', label_size, 'Location', 'southeast')
title('the raw cariac signal and the detected peaks', 'FontWeight', 'bold', 'FontSize', label_size)

subplot(4, 1, 2) % HR and HRCRF
t = (1:nvol)*TR;
HR_to_plot = HR(end-nvol+1:end);
yyaxis left
plot(t, HR_to_plot, 'LineWidth', line_width) % HR
ylabel('HR (beat per min)', 'FontWeight', 'bold', 'FontSize', label_size)
yyaxis right
plot(t, reg_RVHRCOR(:, 1), 'LineWidth', line_width) % HRCRF
ylabel('HRCRF regressor', 'FontWeight', 'bold', 'FontSize', label_size)
grid on
hold off
xlabel('time (sec)', 'FontWeight', 'bold', 'FontSize', label_size)
xlim([t(1), t(end)])
title('the HR and the HRCRF', 'FontWeight', 'bold', 'FontSize', label_size)

subplot(4, 1, 3) % raw respiration signal
Resp_data_to_plot = raw_resp(end-nvol*TR*resp_srate+1:end);
t = (1:length(Resp_data_to_plot))/resp_srate;
plot(t, Resp_data_to_plot) % raw signal
grid on
xlabel('time (sec)', 'FontWeight', 'bold', 'FontSize', label_size)
xlim([t(1), t(end)])
ylabel('raw respiration', 'FontWeight', 'bold', 'FontSize', label_size)
title('the raw respiration signal', 'FontWeight', 'bold', 'FontSize', label_size)

subplot(4, 1, 4) % RV and RVRRF
t = (1:nvol)*TR;
RV_to_plot = RV(end-(nvol)+1:end);
yyaxis left
plot(t, RV_to_plot, 'LineWidth', line_width) % HR
ylabel('RV', 'FontWeight', 'bold', 'FontSize', label_size)
yyaxis right
plot(t, reg_RVHRCOR(:, 2), 'LineWidth', line_width) % HRCRF
ylabel('RVRRF regressor', 'FontWeight', 'bold', 'FontSize', label_size)
grid on
hold off
xlabel('time (sec)', 'FontWeight', 'bold', 'FontSize', label_size)
xlim([t(1), t(end)])
title('the RV and the RVRRF', 'FontWeight', 'bold', 'FontSize', label_size)

sgtitle({['The raw cardiac and respiration signals and RVHRCOR regressors'];
    subj_info_str}, 'FontWeight', 'bold', 'FontSize', label_size)

end

