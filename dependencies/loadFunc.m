function [headinfo,funcData,funcROI]=loadFunc(fullPath_func)

% 
% loadFunc(fullPath_func)
% load functional data from file fullPath_func
% 
% Author: Chi Wah (Alec) Wong May 22 2012
% $Id$

if ~exist([fullPath_func,'.HEAD'],'file')
    error('Could not find funcional data!');
end;

[err,Info, ~] = BrikInfo([fullPath_func,'.HEAD']);

headinfo.nvols=Info.DATASET_RANK(2);
headinfo.TR=Info.TAXIS_FLOATS(2);
headinfo.xdim=Info.DATASET_DIMENSIONS(1);
headinfo.ydim=Info.DATASET_DIMENSIONS(2);
headinfo.zdim=Info.DATASET_DIMENSIONS(3);
headinfo.xvox=Info.DELTA(1);
headinfo.yvox=Info.DELTA(2);
headinfo.zvox=Info.DELTA(3);
headinfo.orient=Info.ORIENT_SPECIFIC;
headinfo.origin=Info.ORIGIN';

%headinfo =  ReadAFNIHead([fullPath_func,'.HEAD']);

[err, funcData , ~, ~]    = BrikLoad(fullPath_func); % no ByteSwap

delete('./__tt_eBmask*');
eval(['!3dcalc -expr "a" -a ',fullPath_func,'\[0\] -prefix ./__tt_eBmask.nii.gz']);
eval('!bet ./__tt_eBmask.nii.gz ./__tt_eBmask2');
eval('!3dcopy ./__tt_eBmask2.nii.gz ./__tt_eBmask2');

% extract the prefix of the anatomical file
origInd=strfind(fullPath_func,'+orig');
tlrcInd=strfind(fullPath_func,'+tlrc');

% default is +orig
funcEnd='+orig';
% remove the +orig or +tlrc tag and call it funcName2
if isempty(tlrcInd)
    funcEnd=fullPath_func(origInd(end):end);
elseif isempty(origInd)
    funcEnd=fullPath_func(tlrcInd(end):end);
end;

[err, funcROI , ~, ~]     = BrikLoad(['__tt_eBmask2',funcEnd]); % no ByteSwap
delete('./__tt_eBmask*');


