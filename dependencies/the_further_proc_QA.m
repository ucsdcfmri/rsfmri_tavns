function the_further_proc_QA(funcData_preGLM, funcData_postGLM, brainMask, nvol, TR, ...
    motion_paras, WM_reg, CSF_reg, raw_card, card_srate,...
    c_peak, HR, HRCRF, raw_resp, resp_srate, RV, RVRRF, opts)

%% calculate FD
r=50;
motion_paras = motion_paras(end-nvol+1:end, :);
delta_mo = diff([motion_paras(1,:);motion_paras],1);
FD       = sum(abs([delta_mo(:,4:6) delta_mo(:,1:3)*2*pi*r/360]) ,2);  
bad_vol_FD = FD>opts.FD_thresh;
outputs.ratio_bad_FD = sum(bad_vol_FD)/nvol*100;
%% compute the raw pulse amplitude (RPA) and the mean pulse amplitude (MPA)
cycles = ceil(6/TR); % a window length close to 6 sec
[MPA,RPA] = my_avgPA(raw_card, c_peak, cycles, TR, card_srate);
MPA        = MPA(end-nvol:end);
RPA        = RPA(end-nvol:end);
%% compute global signal
gs_preGLM = calcGS(funcData_preGLM, brainMask, 1); % calculate the GS in percent change
% compute global signal amplitude
outputs.gsAmp_preGLM = std(gs_preGLM(bad_vol_FD == 0));

gs_postGLM = calcGS(funcData_postGLM, brainMask, 1); % calculate the GS in percent change
% compute global signal amplitude
outputs.gsAmp_postGLM = std(gs_postGLM(bad_vol_FD == 0));
%% mask the functional data and convert the functional data to the percent change
% functional data before GLM
sz = size(funcData_preGLM);
if length(sz) ~= 2
    funcData_preGLM = reshape(funcData_preGLM, sz(1)*sz(2)*sz(3), sz(4));
end
sz = size(brainMask);
if length(sz) ~= 2
    brainMask = reshape(brainMask, sz(1)*sz(2)*sz(3), 1);
end
brainMask = logical(brainMask); % make sure it is a binary mask
% check if the mask and the functional data have the same number of voxels
if size(brainMask, 1) == size(funcData_preGLM, 1)
    % mask the functional data
    funcData_preGLM_masked = funcData_preGLM(brainMask, :);
else 
    error('The mask and the functional data don''t have the same number of voxels!')
    
end
funcData_preGLM_masked_perc = ...
    (funcData_preGLM_masked - repmat( mean(funcData_preGLM_masked,2) ,1, size(funcData_preGLM_masked,2) ) ) ...
    ./ repmat( mean(funcData_preGLM_masked,2) ,1, size(funcData_preGLM_masked,2) )*100;
% nan would occur if the mean of a voxel is zero
% get rid of such voxels
nanVoxel = isnan(funcData_preGLM_masked_perc(:, 1));
funcData_preGLM_masked_perc(nanVoxel, :) = [];

% functional data after GLM
sz = size(funcData_postGLM);
if length(sz) ~= 2
    funcData_postGLM = reshape(funcData_postGLM, sz(1)*sz(2)*sz(3), sz(4));
end
sz = size(brainMask);
if length(sz) ~= 2
    brainMask = reshape(brainMask, sz(1)*sz(2)*sz(3), 1);
end
brainMask = logical(brainMask); % make sure it is a binary mask
% check if the mask and the functional data have the same number of voxels
if size(brainMask, 1) == size(funcData_postGLM, 1)
    % mask the functional data
    funcData_postGLM_masked = funcData_postGLM(brainMask, :);
else 
    error('The mask and the functional data don''t have the same number of voxels!')
    
end
funcData_postGLM_masked_perc = ...
    (funcData_postGLM_masked - repmat( mean(funcData_postGLM_masked,2) ,1, size(funcData_postGLM_masked,2) ) ) ...
    ./ repmat( mean(funcData_postGLM_masked,2) ,1, size(funcData_postGLM_masked,2) )*100;
% nan would occur if the mean of a voxel is zero
% get rid of such voxels
nanVoxel = isnan(funcData_postGLM_masked_perc(:, 1));
funcData_postGLM_masked_perc(nanVoxel, :) = [];

%% start plotting
% f = figure2('units','normalized','outerposition',[0 0 1 0.6]);
f = figure2('Renderer', 'painters', 'Position', [10, 10, 1800, 1500]);
set(f, 'NumberTitle', 'off', 'Name', opts.subjInfo);
anum = 1;
nplots = 10;dht = 0.03;top = .99;
ahts = [0.05*ones(1,8) 0.10 0.10];

%-------------------------------------------------------
%MOTION
%subplot
a1=subplot(nplots,1,anum);
abot = top-dht-ahts(anum);
set(a1,'position',[0.1 abot 0.7 ahts(anum)]);
anum = anum+1;top = abot;
originalSize1 = get(gca, 'Position');

%plot FD
pl = plot(FD,'r');
hold on

% find and plot bad intervals based on FD
[start_points,end_points] = LookForBoundary(find(bad_vol_FD==1));
y_range = get(gca,'Ylim');
if ~isempty(start_points)
    for izone = 1:length(start_points)
        width_rect = end_points(izone)-start_points(izone);
        x_range = [start_points(izone) end_points(izone)];
        p1 = patch(x_range([1 1 2 2])+0.5,y_range([1 2 2 1]),...
            'k','facealpha',0.3,'edgecolor','none');
    end
else
    % an empty patch is created when there is no bad segments
    p1 = patch([0 0 0 0],y_range([1 2 2 1]),...
            'k','facealpha',0.3,'edgecolor','none');
end


% plot threshold
try
yl = yline(opts.FD_thresh, '-' , 'Color', 'k');
catch
   yl = plot([0 opts.num_vol-opts.im1], opts.FD_thresh*ones(1, 2), '-' , 'Color', 'k'); 
end
hold off;

% settings of the plot
ylabel('FD(mm)');
set(gca,'xticklabel',{[]})
legend([pl, p1, yl], ...
    {'Head Motion', 'Bad Segments', 'FD Threshold'}, ...
    'Location','bestoutside');
xlim([1 length(FD)]);
set(gca,'xticklabel',{[]});
axis tight;
grid on;

a1_title = ['FD Threshold: ', num2str(opts.FD_thresh), ...
    ' (mm);     % of bad volumes (FD): ', num2str(outputs.ratio_bad_FD, '%.3f')];
title(a1, a1_title)
%a1.TitleFontSizeMultiplier = 0.5;
set(a1, 'Position', originalSize1);
set(gca,'FontSize',10)
%-------------------------------------------------------
% WM and CSF regressors
a2=subplot(nplots,1,anum);
abot = top-dht-ahts(anum);									
set(a2,'position',[0.1 abot 0.7 ahts(anum)]);
anum = anum+1;top = abot;			

originalSize3 = get(gca, 'Position');
yyaxis left
plot(WM_reg(end-nvol+1:end), 'b')
ylabel('WM reg.');
xlim([1 length(WM_reg(end-nvol+1:end))])
%set('Ytick',0:0.1:5)
yyaxis right
plot(CSF_reg(end-nvol+1:end), 'r')
ylabel('CSF reg.');
xlim([1 length(CSF_reg(end-nvol+1:end))])
set(gca,'xticklabel',{[]});
grid on;
set(a2, 'Position', originalSize3);
set(gca,'FontSize',10)
%-------------------------------------------------------
% RAW CARDIAC
a3=subplot(nplots,1,anum);
abot = top-dht-ahts(anum);			
set(a3,'position',[0.1 abot 0.7 ahts(anum)]);
originalSize1_b = get(gca, 'Position');
anum = anum+1;top = abot;
plot(raw_card(end-nvol*TR*card_srate+1:end));
hold on
cardiacPeaks_for_plot = c_peak;
cardiacPeaks_for_plot(c_peak==0) = nan;
plot(cardiacPeaks_for_plot(end-nvol*TR*card_srate+1:end).*...
    raw_card(end-nvol*TR*card_srate+1:end), '.r');
xlim([1 length(raw_card(end-nvol*TR*card_srate+1:end))])
set(gca,'xticklabel',{[]})
ylabel('Raw Cardiac');
legend('Raw cardiac', 'Detected peaks', 'Location','bestoutside')
grid on;
%legend boxoff;
axis tight;
set(a3, 'Position', originalSize1_b);
set(gca,'FontSize',10)
%-------------------------------------------------------
% HR and HRCRF
a4=subplot(nplots,1,anum);
abot = top-dht-ahts(anum);									
set(a4,'position',[0.1 abot 0.7 ahts(anum)]);
anum = anum+1;top = abot;			

originalSize3 = get(gca, 'Position');
yyaxis left
plot(HR(end-nvol+1:end), 'b')
ylabel({['HR '];['(beat-per-min)']});
xlim([1 length(HR(end-nvol+1:end))])
%set('Ytick',0:0.1:5)
yyaxis right
plot(HRCRF(end-nvol+1:end), 'r')
ylabel('HRf');
xlim([1 length(HRCRF(end-nvol+1:end))])
set(gca,'xticklabel',{[]});
grid on;
set(a4, 'Position', originalSize3);
set(gca,'FontSize',10)

%-------------------------------------------------------
% Mean Peak Amplitude
a5=subplot(nplots,1,anum);
abot = top-dht-ahts(anum);			
set(a5,'position',[0.1 abot 0.7 ahts(anum)]);
anum = anum+1;top = abot;

originalSize2 = get(gca, 'Position');
tax = 1:length(MPA);
plot(tax,MPA,'.-r',tax,RPA,'b-x');
legend('Mean Peak Amplitude','Raw Peak','Location','bestoutside');
ylabel('Ampl.')
set(gca,'xticklabel',{[]})
grid on;
axis tight;
xlim([1 length(MPA)]);grid on;
%legend boxoff;
set(a5, 'Position', originalSize2);

%------------------------------------------------------
%Plot Respiratory Belt
a6=subplot(nplots,1,anum);
abot = top-dht-ahts(anum); 			
set(a6,'position',[0.1 abot 0.7 ahts(anum)]);
anum = anum+1;top = abot;									
originalSize5 = get(gca, 'Position');
plot(raw_resp(end-nvol*TR*resp_srate+1:end),'b');
xlim([1 length(raw_resp(end-nvol*TR*resp_srate+1:end))])
set(gca,'xticklabel',{[]})
ylabel('Raw Resp.');
grid on;
%legend boxoff;
axis tight;
set(a6, 'Position', originalSize5);
set(gca,'FontSize',10)
%-------------------------------------------------------                        
%Plot RV and RVRRF
a7=subplot(nplots,1,anum);
abot = top-dht-ahts(anum); 						
set(a7,'position',[0.1 abot 0.7 ahts(anum)]);
anum = anum+1;top = abot;												

originalSize5 = get(gca, 'Position');
yyaxis left
plot(RV(end-nvol+1:end), 'b')
ylabel('RV');
xlim([1 length(RV(end-nvol+1:end))])

yyaxis right
plot(RVRRF(end-nvol+1:end), 'r')
ylabel('RVf');
xlim([1 length(RVRRF(end-nvol+1:end))])
set(gca,'xticklabel',{[]});
grid on;

%legend boxoff;
set(a7, 'Position', originalSize5);
set(gca,'FontSize',10)

%-------------------------------------------------------                   
%Plot global signal
% set up subplot
a8=subplot(nplots,1,anum);
abot = top-dht-ahts(anum); 
set(a8,'position',[0.1 abot 0.7 ahts(anum)]);
anum = anum+1;top = abot;						
originalSize4 = get(gca, 'Position');

% draw global signal
plot(gs_preGLM, 'b')
hold on
plot(gs_postGLM, 'r')
hold off
ylabel('GS % BOLD');
if opts.GLM_Poly_outsideGLM == 1 % polynomial regressors were regressed out outside the GLM.
    str = ['(Polynomial regressors were regressed out from both GS)'];
else % polynomial regressors were regressed out inside the GLM.
    str = ['(Polynomial regressors were regressed out only from the GS after GLM)'];
end
title(a8, ['GS Amp. before GLM: ', num2str(outputs.gsAmp_preGLM, '%.4f'), ...
    '; GS Amp. after GLM: ', num2str(outputs.gsAmp_postGLM, '%.4f'),...
    ';  ', str])
legend({'GS before GLM', 'GS after GLM'}, ...
    'Location','bestoutside');
% settings of plot
% a7.TitleFontSizeMultiplier = 0.8;
xlim([1 length(gs_preGLM)])
set(gca,'xticklabel',{[]});
grid on;
set(a8, 'Position', originalSize4);
set(gca,'FontSize',10)

%-------------------------------------------------------                        
%the signal heatmap: the functional data before GLM
a9=subplot(nplots,1,anum);
abot = top-dht-ahts(anum); 									
set(a9,'position',[0.1 abot 0.7 ahts(anum)]);
anum = anum+1;top = abot;															

imagesc(funcData_preGLM_masked_perc, [-1, 1])
if opts.GLM_Poly_outsideGLM == 1 % polynomial regressors were regressed out outside the GLM.
    str = ['(Polynomial regressors were regressed out)'];
else % polynomial regressors were regressed out inside the GLM.
    str = ['(Polynomial regressors were not regressed out)'];
end
title(a9, ['The voxel-wise BOLD signal before GLM (scaled to [-1, 1]);  ', str])
ylabel('voxel');
% settings of plot
% a8.TitleFontSizeMultiplier = 0.8;
set(gca,'xticklabel',{[]});
originalSize = get(gca, 'Position');
c1 = colorbar;
set(a9, 'Position', originalSize);
colormap gray;
set(gca,'FontSize',10)

%-------------------------------------------------------                        
%the signal heatmap
a10=subplot(nplots,1,anum);
abot = top-dht-ahts(anum); 									
set(a10,'position',[0.1 abot 0.7 ahts(anum)]);
anum = anum+1;top = abot;															

imagesc(funcData_postGLM_masked_perc, [-1, 1])
title(a10, ['The voxel-wise BOLD signal after GLM (scaled to [-1, 1])'])
ylabel('voxel');
xlabel('volume')
% settings of plot
%a9.TitleFontSizeMultiplier = 0.8;
originalSize = get(gca, 'Position');
c2 = colorbar;
set(a10, 'Position', originalSize);
colormap gray;
set(gca,'FontSize',10)


end

