function [r,p,z] = mcorr(x,y,doff)

% [r,p,z] = mcorr(x,y)
% calculate pearson's r correlation between matrix x and y.
% Inputs:
% x: n by v1, n is dt;
% y: n by v2
% Outputs:
% r / p / z: v1 by v2
% r:    c.c.
% p:    p value (degrees of freedom = doff)
% z:    fisher's z, normalized by sqrt(doff-3)
%
% Author: Hongjian He, 06/03/09
%$Id$

n = size(x,1);  % time points
if ~exist('doff','var');  doff=n; end;
if isempty(doff);  doff=n; end;

%% check size
if ndims(x)>2 || ndims(y)>2
    error('This code can only handle 2d or 1d data');
end
if size(y,1)~=n
    if size(y,2)==n
        y = y';
    else
        error('input matrixs dont share any equal dimension');
    end
end

%% zeros-mean
x = bsxfun(@minus,x,mean(x,1));
y = bsxfun(@minus,y,mean(y,1));

%% correlation
warning off MATLAB:divideByZero;
r = x'*y ./ sqrt( (x'.^2 * ones(n,1)) * (ones(1,n) * y.^2) ) ;
%  v1*n*n*v2 ./    v1*n*n*1           *       1*n*n*v2
r(isnan(r)) = 0;

%% the following options are done only when it's necessary
% find p value trougth t transformation
if nargout>1
    % t transformation
    warning off MATLAB:divideByZero;    % in the case of r=+-1
    t = r ./sqrt((1-r.^2)./(doff-2));
    
    % find p value according to tcdf, p = 2 * left area less than -|t|.
    p = 2 * tcdf(-abs(t),doff-2);
end

% find the normalized z value throught fisher's z transform
if nargout>2
    z = sqrt(doff-3) * atanh(r);
end