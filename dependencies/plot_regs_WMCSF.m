function plot_regs_WMCSF(reg_WMCSF, GLM, subj_info_str, line_width, label_size)
% plot WM and CSF regressors
if GLM.WMCSF_type % PCA
    if GLM.WMCSF_combine_WM_CSF % combined WM and CSF masks
        figure2('Renderer', 'painters', 'Position', [10, 10, 1600, 900])
        hold on
        legends = {};
        for i = 1:GLM.WMCSF_numPC
            plot(reg_WMCSF(:, i), 'LineWidth', line_width)
            legends = {legends{:}, [num2str(i), '-th PC']};
        end
        hold off
        grid on
        xlabel('volume', 'FontWeight', 'bold', 'FontSize', label_size)
        ylabel('aCompCor regressors', 'FontWeight', 'bold', 'FontSize', label_size)
        legend(legends, 'FontWeight', 'bold', 'FontSize', label_size, 'Location', 'best')
        title({[num2str(GLM.WMCSF_numPC), ' aCompCor regressors from the combined WM and CSF mask'];
            subj_info_str}, 'FontWeight', 'bold', 'FontSize', label_size)
    else 
        figure2('Renderer', 'painters', 'Position', [10, 10, 1600, 900])
        subplot(2, 1, 1) % WM regressors
        hold on
        legends = {};
        for i = 1:GLM.WMCSF_numPC
            plot(reg_WMCSF(:, i), 'LineWidth', line_width)
            legends = {legends{:}, [num2str(i), '-th PC']};
        end
        hold off
        grid on
        xlabel('volume', 'FontWeight', 'bold', 'FontSize', label_size)
        ylabel('WM regressors', 'FontWeight', 'bold', 'FontSize', label_size)
        legend(legends, 'FontWeight', 'bold', 'FontSize', label_size, 'Location', 'best')
        title([num2str(GLM.WMCSF_numPC), ' aCompCor regressors from the WM mask'], 'FontWeight', 'bold', 'FontSize', label_size)

        subplot(2, 1, 2) % CSF regressors
        hold on
        legends = {};
        for i = 1:GLM.WMCSF_numPC
            plot(reg_WMCSF(:, i+GLM.WMCSF_numPC), 'LineWidth', line_width)
            legends = {legends{:}, [num2str(i), '-th PC']};
        end
        hold off
        grid on
        xlabel('volume', 'FontWeight', 'bold', 'FontSize', label_size)
        ylabel('CSF regressors', 'FontWeight', 'bold', 'FontSize', label_size)
        legend(legends, 'FontWeight', 'bold', 'FontSize', label_size, 'Location', 'best')
        title([num2str(GLM.WMCSF_numPC), ' aCompCor regressors from the CSF mask'], 'FontWeight', 'bold', 'FontSize', label_size)

        sgtitle({['The WM and CSF regressors (aCompCor, # of PCs: ', num2str(GLM.WMCSF_numPC), ')'];
            subj_info_str}, 'FontWeight', 'bold', 'FontSize', label_size)
%         saveas(gcf, [ProcData_save_dir, 'reg_aCompCor', num2str(GLM.WMCSF_numPC), 'PCsWMCSF_'], 'png')
%         close(gcf)
    end
else % mean
    figure2('Renderer', 'painters', 'Position', [10, 10, 1600, 900])
    subplot(2, 1, 1) % WM regressors
    plot(reg_WMCSF(:, 1), 'LineWidth', line_width) % WM
    hold on
    plot(reg_WMCSF(:, 3), 'LineWidth', line_width) % its 1-st derivatives
    grid on
    hold off
    xlabel('volume', 'FontWeight', 'bold', 'FontSize', label_size)
    ylabel('WM regressors', 'FontWeight', 'bold', 'FontSize', label_size)
    legend('the mean WM signal', 'its first derivatives', 'FontWeight', 'bold', 'FontSize', label_size, 'Location', 'best')
    title('the WM regressors', 'FontWeight', 'bold', 'FontSize', label_size)
    
    subplot(2, 1, 2) % CSF regressors
    plot(reg_WMCSF(:, 2), 'LineWidth', line_width) % CSF
    hold on
    plot(reg_WMCSF(:, 4), 'LineWidth', line_width) % its 1-st derivatives
    grid on
    hold off
    xlabel('volume', 'FontWeight', 'bold', 'FontSize', label_size)
    ylabel('CSF regressors', 'FontWeight', 'bold', 'FontSize', label_size)
    legend('the mean CSF signal', 'its first derivatives', 'FontWeight', 'bold', 'FontSize', label_size, 'Location', 'best')
    title('the CSF regressors', 'FontWeight', 'bold', 'FontSize', label_size)
    
    sgtitle({['The WM and CSF regressors (mean signals + their first derivatives)'];
        subj_info_str}, 'FontWeight', 'bold', 'FontSize', label_size)
%     saveas(gcf, [ProcData_save_dir, 'reg_meanWMCSF'], 'png')
%     close(gcf)
end
end

