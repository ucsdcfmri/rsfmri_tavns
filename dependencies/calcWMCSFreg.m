function [WMCSFreg, AnatMask_white, AnatMask_csf] = calcWMCSFreg(funcData,pv_mask,brain_mask,compType,numComp, derivatives, combine_WM_CSF, erosionNum,...
    pv_thres, ClustSize, ClustMode, FuncNorm, FuncDetrend)

%
% create WM and CSF regressors
% Input
% pv_mask       - structure containing partial volume mask for white/gray/csf
% brain_mask     - whole brain mask
% compType      - 0 = mean, 1= PCA
%                 (default = 0)
% numComp       - number of components to return if PCA is used
% derivatives     - 0 = WMcsf only 
%                 - 1 = WMcsf + 1st derivative 
%                 (default=1)
% combine_WM_CSF - Do you want to combine the WM and CSF masks by using the
%                  'and' operation?
%                  1 = do
%                  0 = don't
%                  (default = 0)
% erosionNum    - number of voxels to be eroded for WM and CSF. e.g [2 1] ;
%                   (default [2 2]) The erosion starts with the inserted
%                   value (e.g [2 1]), if less than 10 voxels left in the
%                   mask (e.g CSF mask) with a given erosion, then the
%                   algorithm will reduce the erosion number by one (e.g [2
%                   0]) and generates a new masks. The erosion that is used
%                   to genarted the regressors will be saved in finalErosion.
% 
% pv_thres      - The partial volume map thresholds to determine the WM
%                   and CSF voxels repectively. E.g., if we set
%                   GLM.WMCSF_pv_thres = [0.99, 0.99], then CSF voxels were
%                   determined by first thresholding the CSF partial volume
%                   maps at 0.99. Similarly, the WM voxels were determined
%                   by first thresholding the  WM partial volume maps at
%                   0.99.  
% ClustSize     - The partial volume map after thresholding is then
%                   clusterized to minimize multiple tissue partial
%                   voluming (Behzadi Y, et al., NIMG, 2007). This option
%                   is to control the cluster size when clustering the WM
%                   and CSF masks respectively.
%                   (default = [2, 2])
% ClustMode     - The partial volume map after thresholding is then
%                   clusterized to minimize multiple tissue partial
%                   voluming (Behzadi Y, et al., NIMG, 2007). This option
%                   is to control the cluster mode when clustering the WM
%                   and CSF masks respectively: 
%                   1 = touching faces (NN1)
%                   2 = touching faces/edges (NN2) 
%                   3 = touching faces/edges/corners (NN3)
%                   (default = [2, 2])
% FuncDetrend   - The maximum order of the polynomials (not include DC) you want to
%                   regress out from the functional data before
%                   normalization and calculating the WM and CSF regressors.  
%                 0 - don't detrend
%                 >=1 - detrend. 1 to [FuncDetrend] order of polynomials
%                 will be regressed out from the functional data.

% FuncNorm      - how to normalize the functional data after detrending and
%                 before calculating the WM and CSF regressors  
%                   0 = don't normalize
%                   1 = convert to percent change
%                   2 = z-normalization (zero mean and unit variance).
%                   Note, this option is used in the aCompCor paper
%                   (Behzadi Y, et al., NIMG, 2007).
%                   3 = demean
% Note, the functional data are first detrended then normalized.
% Output
% reg_WMcsf     - the WM and CSF regressors
% pv_mask       - the WM and CSF masks that are used to generate the WM and
%                   CSF regressors.
% finalErosion  - The erosion numbers used for each WM and CSF masks

% Author: Yixiang Mao 05/28/2021
% $Id$

if ~exist('derivatives','var'); derivatives = 1; end;
if isempty(derivatives); derivatives = 1; end;

if ~exist('compType','var'); compType = 0; end;
if isempty(compType); compType = 0; end;

if ~exist('numComp','var'); numComp = 1; end;
if isempty(numComp); numComp = 1; end;

if ~exist('erosionNum','var') || isempty(erosionNum); erosionNum = [2 2]; end;

if ~exist('combine_WM_CSF','var'); combine_WM_CSF = 0; end;
if isempty(combine_WM_CSF); combine_WM_CSF = 0; end;

if ~exist('pv_thres','var'); pv_thres = [0.99, 0.99]; end;
if isempty(pv_thres); pv_thres = [0.99, 0.99]; end;

if ~exist('ClustSize','var'); ClustSize = [2, 2]; end;
if isempty(ClustSize); ClustSize = [2, 2]; end;

if ~exist('ClustMode','var'); ClustMode = [2, 2]; end;
if isempty(ClustMode); ClustMode = [2, 2]; end;

if ~exist('FuncNorm','var'); FuncNorm = 2; end;
if isempty(FuncNorm); FuncNorm = 2; end;

if ~exist('FuncDetrend','var'); FuncDetrend = 2; end;
if isempty(FuncDetrend); FuncDetrend = 2; end;

% re-assign parameters to increse the readability of the code
% pv map thresholds
pv_thres_white = pv_thres(1);
pv_thres_csf = pv_thres(2);
% erosion numbers
erosionNum_white = erosionNum(1);
erosionNum_csf = erosionNum(2);
% clustering criteria: cluster size
ClustSize_white = ClustSize(1);
ClustSize_csf = ClustSize(2);
% clustering criteria: cluster mode
ClustMode_white = ClustMode(1);
ClustMode_csf = ClustMode(2);


% generate the WM and CSF masks
AnatMask_white = makeAnatMask(pv_mask.white, brain_mask, pv_thres_white,...
    erosionNum_white, ClustSize_white, ClustMode_white, 'White Matter');

AnatMask_csf = makeAnatMask(pv_mask.csf, brain_mask, pv_thres_csf,...
    erosionNum_csf, ClustSize_csf, ClustMode_csf, 'CSF');

% generate the WM and CSF regressors
comp = [];
if combine_WM_CSF % you want to combine the WM and CSF masks as one mask
    combined_WM_CSF_mask = AnatMask_white | AnatMask_csf;
    funcData_masked = maskData(funcData, combined_WM_CSF_mask);
    funcData_masked = funcData_masked{1}; % the output of maskData function is a cell array
    [~,~,comp,~,~] = calcComp(funcData_masked,compType,numComp, FuncNorm, FuncDetrend); 

else % you don't want to combine the WM and CSF masks as one mask, 
     % hence the WM and CSF masks will be used separately. 
    
	% regressors calculated by the white matter mask
	funcData_masked_white = maskData(funcData, AnatMask_white);
    funcData_masked_white = funcData_masked_white{1}; % the output of maskData function is a cell array
    [~,~,comp_white,~,~] = calcComp(funcData_masked_white,compType,numComp, FuncNorm, FuncDetrend);
    
    % regressors calculated by the CSF mask
	funcData_masked_csf = maskData(funcData, AnatMask_csf);
    funcData_masked_csf = funcData_masked_csf{1}; % the output of maskData function is a cell array
    [~,~,comp_csf,~,~] = calcComp(funcData_masked_csf,compType,numComp, FuncNorm, FuncDetrend);
    
    % concatenate the WM and CSF regressors 
    comp = [comp_white, comp_csf];
end

% calculate the 1-st derivatives if needed
compDiff=[];
if derivatives==1
    compDiff=diff(comp);
    % the 1st derivatives is one point shorter, hence we pad
    % one point.
    compDiff = [compDiff(1, :); compDiff];
end;

WMCSFreg=[comp, compDiff];


function AnatMask=makeAnatMask(pv_mask, brain_mask, pv_thres, erosionNum, ClustSize, ClustMode, tissue_type)
    
    % first generate the mask by applying the threshold to the pv map
    the_mask = (pv_mask>pv_thres)&(brain_mask>0);
    
    % erode the mask
    sz = size(the_mask);
    for izslice = 1:sz(3)
        the_mask_eroded(:, :, izslice) = bwmorph(the_mask(:,:,izslice), 'erode', erosionNum);  
    end
    
    % clustering the mask
    the_mask_eroded_clustered = imClusterCust(the_mask_eroded>0,ClustSize,ClustMode);
    
    num_vox_in_the_mask = nnz(the_mask_eroded_clustered);
    if num_vox_in_the_mask == 0 % if there is no surviving voxel in the mask
        error(['No ', tissue_type, ' voxels identified!']) 
    else % output the mask if the mask has at least 1 voxel
        AnatMask = the_mask_eroded_clustered;
        display([num2str(num_vox_in_the_mask), ' voxels in the ', tissue_type, ' mask.']);  
    end


function [U,S,comp,data,compcorEig]=calcComp(data,compType,numPC, FuncNorm, FuncDetrend)
% transpose the data matrix, hence the matrix is with time as the last dimension
data=data';
%detrend
if FuncDetrend>0
    sz = size(data);
    S = legendremat(FuncDetrend,sz(2));
    S = S(:, 2:end); % not include DC, hence the mean of the functional
                     % data is not taken out. 
    data = data*(eye(sz(2))-S*inv(S'*S)*S');
end
switch FuncNorm
    case 0 % no normalization
        data=data;
    case 1 % convert to percent change before compute GS
        data = ...
            (data - repmat( mean(data,2) ,1, size(data,2) ) ) ...
            ./ repmat( mean(data,2) ,1, size(data,2) )*100;
        % nan would occur if the mean of a voxel is zero
        % get rid of such voxels 
        nanVoxel = isnan(data(:, 1));
        data(nanVoxel, :) = [];
    case 2 % z-normalization (zero mean and unit variance)
        data = ...
            (data - repmat( mean(data,2) ,1, size(data,2) ) ) ...
            ./ repmat( std(data, 0, 2) ,1, size(data,2) );
        % nan would occur if the std of a voxel is zero
        % get rid of such voxels 
        nanVoxel = isnan(data(:, 1));
        data(nanVoxel, :) = [];
    case 3 % demean
        data = ...
            (data - repmat( mean(data,2) ,1, size(data,2) ) );
        % nan would occur if the std of a voxel is zero
        % get rid of such voxels 
        nanVoxel = isnan(data(:, 1));
        data(nanVoxel, :) = [];
end

dataG   = data';

if compType==1 % do PCA based
   
    %svd of the covariance matrix
    [U,S,~] = svd(dataG*dataG');
    compcorEig=diag(S);
    comp    = U(:,1:numPC);
    
else %calculate the mean
     
    if size(dataG,1)>1 %check if there is only one voxel. If so, no mean is calculated.
        comp=mean(dataG,2);
    else comp=dataG';
    end;
    compcorEig=[];
    % comp=comp-mean(comp);
    U=[];
    S=[];
    
end