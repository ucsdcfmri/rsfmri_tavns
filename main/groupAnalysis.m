%% The script for seed-based full brain connectivity analysis.
% Description
% This script does:
% 1. calculate voxel-wise correlation maps across seeds and runs (optional),
% 2. generate needed BRIK file for AFNI,
% 3. use AFNI to do the statistical analysis (t-tests and FWER correction),
% 4. do post-hoc analysis for each significant clusters.

% Dependencies: 
% 1. AFNI (Version AFNI_21.2.04 'Nerva')

% Authors:  Yixiang Mao (y1mao@ucsd.ued)
close all; clear all; clc;
%% Define paths
% ----------- Please set the following variables properly -----------------
% define the directory where this repo is located
repo_dir = ['/home/ymao/git_repo/rsfmri_tavns/'];
addpath(genpath(repo_dir))

% 
flag_calcFCmaps = 1; % 0: Load the existing functional connectivity maps.
                     % 1: Re-calculate the functional connectivity maps 
                     %      from the BOLD signal.
% -------------------------------------------------------------------------

% ---- If flag_calcFCmaps = 1, please set the following dirs properly------
% processed data save root directory
masterPath = ['/data/students_group/data/vorso_reProc_test/'];

% the name of the folder where the functional data after GLM are saved
dataProc_save_folder = ['_reg_topup_ts_aw_4mm_GLM_Poly2_Mo1_WMCSF5aCompCor_RVHRCOR1_GS0_tlrc_202112281223/', ...
    'FuncFilt_bandpass10to100mHz_Mocen_FD_thres01_FDfilt_low0to100mHz/'];
funcName_suffix = ['_proc_PercChng_FuncFilt_MoCen+tlrc'];
% -------------------------------------------------------------------------

% ------ The following variables properly do NOT need to be changed---------
% the results save dir
if flag_calcFCmaps == 1 % Re-calculate the FC maps
    result_save_root_dir = [masterPath, dataProc_save_folder, 'seeds_based_FC_analysis/'];
    if ~exist(result_save_root_dir, 'dir')
        mkdir(result_save_root_dir)
    end
else % Load the existing functional connectivity maps.
    result_save_root_dir = [repo_dir, 'results/seeds_based_FC_analysis/'];
end
    
if ~exist(result_save_root_dir, 'dir')
        mkdir(result_save_root_dir)
end
    
% included subjects
subj_idx_include = [1:6, 8:16, 20:24, 26]; % the index of subjects that will be included in the analysis

% scan info
nvol = 363;
TR = 0.96;

% directory where we save the brain mask and seed masks
maskpath = [repo_dir, 'masks_and_templates/'];
AnatIm_path = [maskpath ,'mni_icbm152_t1_tal_nlin_asym_09a_brain_rs+tlrc'];
AnatIm = BrikLoad( [maskpath ,'mni_icbm152_t1_tal_nlin_asym_09a_brain_rs+tlrc'] ); % for plotting

% names of the ROI masks
masknames = {
    'CG_7_1_mask_rs+tlrc';
    'CG_7_2_mask_rs+tlrc';
    'CG_7_3_mask_rs+tlrc';
    'CG_7_4_mask_rs+tlrc';
    'CG_7_5_mask_rs+tlrc';
    'CG_7_6_mask_rs+tlrc';
    'CG_7_7_mask_rs+tlrc';
    'INS_6_1_mask_rs+tlrc';
    'INS_6_2_mask_rs+tlrc';
    'INS_6_3_mask_rs+tlrc';
    'INS_6_4_mask_rs+tlrc';
    'INS_6_5_mask_rs+tlrc';
    'INS_6_6_mask_rs+tlrc';
    };

% the name of the ROIs
masknames_print = {
    'dorsal area 23';
    'rostroventral area 24';
    'pregenual area 32';
    'ventral area 23';
    'caudodorsal area 24';
    'caudal area 23';
    'subgenual area 32';
    'hypergranular insula';
    'ventral agranular insula';
    'dorsal agranular insula';
    'ventral granular insula';
    'dorsal granular insula';
    'dorsal dysgranular insula';
    };

%% Read the masks
% read the brain mask
brainMask3d = BrikLoad( [maskpath, ...
    'mni_icbm152_t1_tal_nlin_asym_09a_brain_rs_automask2+tlrc'] );
brainMask3d = logical(brainMask3d);
sz = size(brainMask3d);
brainMask = reshape(brainMask3d, sz(1)*sz(2)*sz(3),1 );
%  read the ROI masks
roiMasks =[];
for imask=1:length(masknames)
    
    roimask =[];
    roimask = BrikLoad( [maskpath ,masknames{imask}] );
    roimask = reshape(roimask, sz(1)*sz(2)*sz(3),1 );
    roiMasks = [roiMasks, roimask]; % I check and all these masks are within the brain automask2
end
roiMasks = logical(roiMasks);
sum(roiMasks)
sum(roiMasks(brainMask, :))
%% calculate voxel-wise correlation maps across seeds and runs
if flag_calcFCmaps == 1 % Re-calculate the FC maps
    % initialization
    FCmaps_r_all = [];
    FCmaps_z_all = [];
    if exist([result_save_root_dir, 'summaryData_FCmaps.mat'], 'file')
        % in case the FC maps have been already re-calculated
        load([result_save_root_dir, 'summaryData_FCmaps.mat'],...
            'FCmaps_r_all', 'FCmaps_z_all')
    else % re-calculate the FC maps
        cd([masterPath, dataProc_save_folder])
        for isub  = subj_idx_include
            for iday = 1:2
                
                if iday == 1
                    treat_or_cont = 'treat';
                else
                    treat_or_cont = 'cont';
                end
                
                subj_str = ['sub-', treat_or_cont, num2str(isub, '%02.f')];
                for iscan = 1:2
                    ses_str = ['ses-', num2str(iscan, '%02.f')];
                    % ses-01: pre session
                    % ses-02: post session
                    
                    subj_info_str_for_save = ['subj', num2str(isub), '_', treat_or_cont, '_', ses_str];
                    
                    ['Processing: ', subj_info_str_for_save]
                    
                    % the input functional data name
                    funcName_input = [subj_str, '_', ses_str, ...
                        funcName_suffix];
                    
                    % load the funcitonal data
                    funcData = BrikLoad(funcName_input);
                    sz = size(funcData);
                    funcData_rs = reshape(funcData, sz(1)*sz(2)*sz(3), sz(4));
                    funcData_rs_masked = funcData_rs(brainMask, :);
                    
                    funcRoi = [];
                    for imask=1:size(roiMasks, 2)
                        if sum(roiMasks(:,imask))==1
                            funcRoi(imask,:) = funcData_rs( roiMasks(:,imask), : );
                        else
                            funcRoi(imask,:) = mean(funcData_rs( roiMasks(:,imask), : ));
                        end
                        [FCmaps_r_all(:,imask,isub,iday,iscan),...
                            p1(:,imask,isub,iday,iscan),...
                            FCmaps_z_all(:,imask,isub,iday,iscan)] = ...
                            mcorr( funcRoi(imask,:)',...
                            funcData_rs_masked');
                    end
                end
                
            end
        end
        save([result_save_root_dir, 'summaryData_FCmaps.mat'],...
            'FCmaps_r_all', 'FCmaps_z_all', ...
            'subj_idx_include', '-v7.3', '-nocompression')
    end
else % Load the existing functional connectivity maps.
    load([repo_dir, '/summaryData/summaryData_FCmaps.mat'],...
        'FCmaps_r_all', 'FCmaps_z_all', 'subj_idx_include')
end

%% Setting for the statistical tests
nSub = length(subj_idx_include);

% assign numbers to different sections
treat_session = 1;
cont_session = 2;
prescan = 1;
postscan = 2;

% 
FCmaps_r_all(FCmaps_r_all==0) = nan;
FCmaps_z_all(FCmaps_z_all==0) = nan;

% select ROIs to be included in the analysis
nROI = length(masknames_print);
ROI_select = [1:nROI];

% settings for the t-tests
whichtest = 'post1-pre_treat-cont'; % the contrast of interest
flags_if_AFNI_perm = 1;

% settings for plot
tstats_clim = [-5, 5];
rvalue_clim = [-0.1, 0.1];
zvalue_clim = [-3, 3];
%% seed-based full brain FC analysis
% using 3dttest++
% generating needed BRIK files

result_save_dir = [result_save_root_dir, 'seeds_based_FC_ttests/'];
if ~exist(result_save_dir, 'dir')
    mkdir(result_save_dir)
end
BRIK_file_save_dir = [result_save_dir, 'BRIK_files/'];
if ~exist(BRIK_file_save_dir, 'dir')
    mkdir(BRIK_file_save_dir)
end

cd(BRIK_file_save_dir);

% z-score
sz = size(brainMask3d);
% treat pre
FCmaps_z_treat_pre = zeros([sz(1)*sz(2)*sz(3), nROI, nSub]);
FCmaps_z_treat_pre(brainMask, :, :)  =  squeeze(FCmaps_z_all(:, :, subj_idx_include, treat_session, prescan));
FCmaps_z_treat_pre = reshape(FCmaps_z_treat_pre, sz(1), sz(2), sz(3), nROI, nSub);
% treat post
FCmaps_z_treat_post = zeros([sz(1)*sz(2)*sz(3), nROI, nSub]);
FCmaps_z_treat_post(brainMask, :, :)  =  squeeze(FCmaps_z_all(:, :, subj_idx_include, treat_session, postscan));
FCmaps_z_treat_post = reshape(FCmaps_z_treat_post, sz(1), sz(2), sz(3), nROI, nSub);
% cont pre
FCmaps_z_cont_pre = zeros([sz(1)*sz(2)*sz(3), nROI, nSub]);
FCmaps_z_cont_pre(brainMask, :, :)  =  squeeze(FCmaps_z_all(:, :, subj_idx_include, cont_session, prescan));
FCmaps_z_cont_pre = reshape(FCmaps_z_cont_pre, sz(1), sz(2), sz(3), nROI, nSub);
% cont post
FCmaps_z_cont_post = zeros([sz(1)*sz(2)*sz(3), nROI, nSub]);
FCmaps_z_cont_post(brainMask, :, :)  =  squeeze(FCmaps_z_all(:, :, subj_idx_include, cont_session, postscan));
FCmaps_z_cont_post = reshape(FCmaps_z_cont_post, sz(1), sz(2), sz(3), nROI, nSub);

clearvars z1
% save BRIK files for post-hoc analysis
for iroi = 1:nROI
    % z-score pre treat
    tmp_name = ['ROI', num2str(iroi), '_', num2str(nSub), 'subj', ...
        '_pre_treat'];
    tmp =  squeeze(FCmaps_z_treat_pre(:, :, :,  iroi, :));
    Mat2AFNI(double(tmp),[BRIK_file_save_dir, tmp_name],  ...
        [masterPath, dataProc_save_folder, 'sub-cont01_ses-01_proc_PercChng_FuncFilt_MoCen+tlrc'] ,3);
    
    
    % z-score post treat
    tmp_name = ['ROI', num2str(iroi), '_', num2str(nSub), 'subj', ...
        '_post1_treat'];
    tmp = squeeze(FCmaps_z_treat_post(:, :, :, iroi, :));
    Mat2AFNI(double(tmp),[BRIK_file_save_dir, tmp_name],  ...
        [masterPath, dataProc_save_folder, 'sub-cont01_ses-01_proc_PercChng_FuncFilt_MoCen+tlrc'] ,3);
    
    % z-score pre cont
    tmp_name = ['ROI', num2str(iroi), '_', num2str(nSub), 'subj', ...
        '_pre_cont'];
    tmp = squeeze(FCmaps_z_cont_pre(:, :, :, iroi, :));
    Mat2AFNI(double(tmp),[BRIK_file_save_dir, tmp_name],  ...
        [masterPath, dataProc_save_folder, 'sub-cont01_ses-01_proc_PercChng_FuncFilt_MoCen+tlrc'] ,3);
    
    % z-score post cont
    tmp_name = ['ROI', num2str(iroi), '_', num2str(nSub), 'subj', ...
        '_post1_cont'];
    tmp = squeeze(FCmaps_z_cont_post(:, :, :, iroi, :));
    Mat2AFNI(double(tmp),[BRIK_file_save_dir, tmp_name],  ...
        [masterPath, dataProc_save_folder, 'sub-cont01_ses-01_proc_PercChng_FuncFilt_MoCen+tlrc'] ,3);
end

% calculate the z-score of the contrasts: 1) (post-pre)_cont, 2)
% (post-pre)_treat
FCmaps_z_treat_post_pre  =  FCmaps_z_treat_post - FCmaps_z_treat_pre;
clearvars z2d_treat_post z2d_treat_pre
FCmaps_z_cont_post_pre   =  FCmaps_z_cont_post  - FCmaps_z_cont_pre;
clearvars z2d_cont_post z2d_cont_pre


% generate BRIK files for t-tests
for isub = 1:nSub
    if ~isfile([BRIK_file_save_dir, 'pilot', num2str(subj_idx_include(isub)), ...
            '_post1-pre_treat-cont+tlrc.HEAD'])
        
        tmp = squeeze(FCmaps_z_treat_post_pre(:, :, :, :, isub)-FCmaps_z_cont_post_pre(:, :, :, :, isub));
        Mat2AFNI(double(tmp),[BRIK_file_save_dir, 'pilot', num2str(subj_idx_include(isub)), ...
            '_post1-pre_treat-cont'],  ...
            [masterPath, dataProc_save_folder, 'sub-cont01_ses-01_proc_PercChng_FuncFilt_MoCen+tlrc'] ,3);
    end
    
    if ~isfile([BRIK_file_save_dir, 'pilot', num2str(subj_idx_include(isub)), ...
            '_post1-pre_treat+tlrc.HEAD'])
        
        tmp = squeeze(FCmaps_z_treat_post_pre(:, :, :, :, isub));
        Mat2AFNI(double(tmp),[BRIK_file_save_dir, 'pilot', num2str(subj_idx_include(isub)), ...
            '_post1-pre_treat'],  ...
            [masterPath, dataProc_save_folder, 'sub-cont01_ses-01_proc_PercChng_FuncFilt_MoCen+tlrc'] ,3);
    end
    
    if ~isfile([BRIK_file_save_dir, 'pilot', num2str(subj_idx_include(isub)), ...
            '_post1-pre_cont+tlrc.HEAD'])
        
        tmp = squeeze(FCmaps_z_cont_post_pre(:, :, :, :, isub));
        Mat2AFNI(double(tmp),[BRIK_file_save_dir, 'pilot', num2str(subj_idx_include(isub)), ...
            '_post1-pre_cont'],  ...
            [masterPath, dataProc_save_folder, 'sub-cont01_ses-01_proc_PercChng_FuncFilt_MoCen+tlrc'] ,3);
    end
end

% 3dttest++ -ClustSim (non-parametric/permutation-based CST)
if flags_if_AFNI_perm
    AFNI_perm_result_save_dir = [result_save_dir, 'AFNI_perm_', whichtest, '/'];
    if ~exist([AFNI_perm_result_save_dir], 'dir')
        mkdir([AFNI_perm_result_save_dir])
    end
    cd(AFNI_perm_result_save_dir);
    for iroi = ROI_select
        cmd = ['3dttest++ -ClustSim -prefix zscore_roi', num2str(iroi), ...
            '_' , whichtest, ' ', ...
            '-mask ', maskpath ,'mni_icbm152_t1_tal_nlin_asym_09a_brain_rs_automask2+tlrc ', ...
            '-prefix_clustsim roi', num2str(iroi), '_', whichtest, ...
            ' -setA '];
        for isub = 1:nSub
            cmd = [cmd, BRIK_file_save_dir, 'pilot', num2str(subj_idx_include(isub)), ...
                '_', whichtest, '+tlrc''[' , num2str(iroi-1), ']'' '];
        end
        
        system(cmd)
        
        % remove tmp files
        cmd = ['rm *minmax.1D'];
        system(cmd)
        
        % calculate the t-stats
        cmd = ['3dttest++ -prefix tstats_roi', num2str(iroi), ...
            '_' , whichtest, ' ', ...
            '-mask ', maskpath ,'mni_icbm152_t1_tal_nlin_asym_09a_brain_rs_automask2+tlrc ', ...
            '-setA '];
        for isub = 1:nSub
            cmd = [cmd, BRIK_file_save_dir, 'pilot', num2str(subj_idx_include(isub)), ...
                '_', whichtest, '+tlrc''[' , num2str(iroi-1), ']'' '];
        end
        
        system(cmd)
        
    end
    
    % start clustering
    NN = '2'; % NN1, NN2 and NN3
    side = 'bisided'; % 1sided, 2sided and bisided
    p_threshold = 0.005;
    alpha_threshold = 0.05;
    
    AFNI_perm_cluster_result_save_dir = [AFNI_perm_result_save_dir, 'cluster_result_p',...
        strrep(num2str(p_threshold), '.', ''), '_a', strrep(num2str(alpha_threshold), '.', ''), ...
        '_NN', NN, '_', side, '/'];
    if ~exist([AFNI_perm_cluster_result_save_dir], 'dir')
        mkdir([AFNI_perm_cluster_result_save_dir])
    end
    cd(AFNI_perm_cluster_result_save_dir)
    for iroi = ROI_select
        M = Read_1D([AFNI_perm_result_save_dir, 'roi', num2str(iroi), '_', whichtest,...
            '.CSimA.NN', NN, '_', side, '.1D']);
        alpha_levels_in_the_1Dfile = [0, .10000, .09000, .08000, .07000, .06000, .05000, .04000, .03000, .02000, .01000];
        % get the cluster size threshold (CST)
        CST = M(M(:, 1)==p_threshold, alpha_levels_in_the_1Dfile==alpha_threshold);
        CST = ceil(CST);
        % start clustering
        input_name = [AFNI_perm_result_save_dir, 'zscore_roi', num2str(iroi),...
            '_', whichtest, '+tlrc'];
        order_mask_name = ['roi', num2str(iroi), '_order_mask+tlrc'];
        cmd = ['3dClusterize -nosum -1Dformat -ithr 1 -NN ', NN, ' -clust_nvox ', num2str(CST), ...
            ' -', side, ' p=', num2str(p_threshold), ' -inset ', input_name, ...
            ' -pref_map ', order_mask_name, ...
            ' > roi', num2str(iroi), 'clust_report.1D'];
        system(cmd)
        % use whereami to look for the location of the clusters
        cmd = ['echo ''\n The location of the clusters: '' >> roi', num2str(iroi), 'clust_report.1D'];
        system(cmd)
        cmd = ['whereami  -omask ', order_mask_name, ' -atlas CA_N27_ML' ...
            ' >> roi', num2str(iroi), 'clust_report.1D'];
        system(cmd)
        % derived the cluster masks based on the order mask
        if isfile([order_mask_name, '.HEAD'])
            tmp = BrikLoad(order_mask_name);
            num_cluster = max(max(max(tmp)));
            
            for iclust = 1:num_cluster
                cluster_mask_name = ['roi', num2str(iroi), '_cluster', num2str(iclust), '_mask+tlrc'];
                
                cmd = ['3dcalc -a ', order_mask_name, ' -expr ''iszero(a-', num2str(iclust), ')'' ', ...
                    '-prefix ', cluster_mask_name];
                system(cmd)
                tmp = BrikLoad(cluster_mask_name);
                cluster_size(iclust) = sum(sum(sum(tmp)));
            end
            
            %  plot z-score and t-stats after clustering
            plot_save_dir = [AFNI_perm_cluster_result_save_dir, '/roi', num2str(iroi),...
                '_clustered_maps/'];
            if ~exist([plot_save_dir], 'dir')
                mkdir([plot_save_dir])
            end
            % plot settings
            slices_plot = [1:80];
            nrow = 8;
            ncol = 10;
            tthresh = 1e-6;
            cthresh = 0;
            mask = fliplr(brainMask3d(:, :, slices_plot));
            underlay = fliplr(AnatIm(:, :, slices_plot));
            underlayrange = [min(min(min(AnatIm))), max(max(max(AnatIm)))];
            figure_position = [10 10 800 600];
            % load t-stats
            tstats = BrikLoad([AFNI_perm_result_save_dir,'tstats_roi',...
                num2str(iroi), '_' , whichtest, '+tlrc']);
            tstats = squeeze(tstats(:, :, :, 2)); % second column is the t-stats
            
            for iclust = 1:num_cluster
                cluster_mask_name = ['roi', num2str(iroi), '_cluster', num2str(iclust), '_mask+tlrc'];
                mask = logical(BrikLoad(cluster_mask_name));
                mask = fliplr(mask(:, :, slices_plot));
                
                % plot t-stats
                overlayrange = tstats_clim;
                overlay = fliplr(tstats(:, :, slices_plot));
                figure('Renderer', 'painters', 'Position', figure_position)
                imageoverlay(underlay,overlay,underlayrange,overlayrange,nrow,ncol,tthresh,cthresh,mask);
                title({['Thresholded t-stats map (p<', num2str(p_threshold), '; \alpha<', num2str(alpha_threshold), ')'];
                    ['seed: ', masknames_print{iroi}, '; cluster #', num2str(iclust), ' (', num2str(cluster_size(iclust)), ' voxels)'];
                    ['Post1-Pre, Treat-Cont']},...
                    'fontweight','bold','fontsize',15);
                %     set(gcf,'PaperPositionMode','auto');
                saveas(gcf, [plot_save_dir, '/tstats_roi', num2str(iroi), '_cluster', num2str(iclust), '_masked'], 'png')
                close(gcf)
                
            end
            % plot all surviving clusters in one plot
            cluster_mask_name = ['roi', num2str(iroi), '_order_mask+tlrc'];
            mask = logical(BrikLoad(cluster_mask_name));
            mask = fliplr(mask(:, :, slices_plot));
            
            % plot t-stats
            overlayrange = tstats_clim;
            overlay = fliplr(tstats(:, :, slices_plot));
            figure('Renderer', 'painters', 'Position', figure_position)
            imageoverlay(underlay,overlay,underlayrange,overlayrange,nrow,ncol,tthresh,cthresh,mask);
            title({['Thresholded t-stats map (p<', num2str(p_threshold), '; \alpha<', num2str(alpha_threshold), ')'];
                ['seed: ', masknames_print{iroi}, '; all clusters'];
                ['Post1-Pre, Treat-Cont']},...
                'fontweight','bold','fontsize',15);
            %     set(gcf,'PaperPositionMode','auto');
            saveas(gcf, [plot_save_dir, '/tstats_roi', num2str(iroi), '_all_clusters_masked'], 'png')
            close(gcf)
            
        end
    end
end

%% post-analysis for seed-based full brain connectivity
% p<0.005, alpha<0.05
% need cluster mask!

% directory where we would like to save the post-hoc results
result_save_dir = [result_save_root_dir, 'seeds_based_FC_ttests',...
    '/post_hoc_ttests/'];
if ~exist(result_save_dir, 'dir')
    mkdir(result_save_dir)
end

% directory where we saved the BRIK files of z-score maps for each  condition
z_score_save_dir = [result_save_root_dir, 'seeds_based_FC_ttests/BRIK_files/'];
% directory where we saved the masks of the significant clusters
clust_mask_save_dir = [result_save_root_dir, 'seeds_based_FC_ttests', ...
    '/AFNI_perm_post1-pre_treat-cont/cluster_result_p0005_a005_NN2_bisided/'];

% surviving clusters
roi_num = [2, 5, 8, 9]; % corresponding ROI indices
clust_num = [1, 1, 1, 1]; % number of significant clusters for each ROI
cluster_names = {'right superior temporal gyrus', 'right inferior parietal lobule', ...
    'right precuneus', 'right cuneus gyrus'}; % names of the clusters


% fontsize for plotting
fontsize = 14;

% start post-hoc anaylysis
cnt = 1;
for i = 1:length(roi_num)
    iroi = roi_num(i);
    % read the mask of the significant cluster
    [~, clust_mask, ~, ~] = BrikLoad([clust_mask_save_dir, 'roi', num2str(iroi), '_order_mask+tlrc']);
    sz = size(clust_mask);
    clust_mask = reshape(clust_mask, sz(1)*sz(2)*sz(3), 1);
    % read the z-score maps for each condition
    % pre treat
    tmp_name = [z_score_save_dir, 'ROI', num2str(iroi), '_', num2str(nSub), 'subj', ...
        '_pre_treat'];
    [~, z_treat_pre_full_brain, ~, ~] = BrikLoad([tmp_name, '+tlrc']);
    z_treat_pre_full_brain = reshape(z_treat_pre_full_brain, sz(1)*sz(2)*sz(3), nSub);
    z_treat_pre_full_brain = z_treat_pre_full_brain(brainMask, :);
    % post treat
    tmp_name = [z_score_save_dir, 'ROI', num2str(iroi), '_', num2str(nSub), 'subj', ...
        '_post1_treat'];
    [~, z_treat_post_full_brain, ~, ~] = BrikLoad([tmp_name, '+tlrc']);
    z_treat_post_full_brain = reshape(z_treat_post_full_brain, sz(1)*sz(2)*sz(3), nSub);
    z_treat_post_full_brain = z_treat_post_full_brain(brainMask, :);
    % pre cont
    tmp_name = [z_score_save_dir, 'ROI', num2str(iroi), '_', num2str(nSub), 'subj', ...
        '_pre_cont'];
    [~, z_cont_pre_full_brain, ~, ~] = BrikLoad([tmp_name, '+tlrc']);
    z_cont_pre_full_brain = reshape(z_cont_pre_full_brain, sz(1)*sz(2)*sz(3), nSub);
    z_cont_pre_full_brain = z_cont_pre_full_brain(brainMask, :);
    % post cont
    tmp_name = [z_score_save_dir, 'ROI', num2str(iroi), '_', num2str(nSub), 'subj', ...
        '_post1_cont'];
    [~, z_cont_post_full_brain, ~, ~] = BrikLoad([tmp_name, '+tlrc']);
    z_cont_post_full_brain = reshape(z_cont_post_full_brain, sz(1)*sz(2)*sz(3), nSub);
    z_cont_post_full_brain = z_cont_post_full_brain(brainMask, :);
   
    for iclust = 1:clust_num(i)
        % average the z-score values within the cluster
        mask = clust_mask(brainMask) == iclust;
        z_treat_pre = mean(z_treat_pre_full_brain(mask, :));
        z_treat_post1 = mean(z_treat_post_full_brain(mask, :));
        z_cont_pre = mean(z_cont_pre_full_brain(mask, :));
        z_cont_post1 = mean(z_cont_post_full_brain(mask, :));
        % settings for plotting
        if iroi == 2
            zlimits = [-20, 20];
            textyaxis = [-12, -12, -15];
        elseif iroi == 5
            zlimits = [-20, 20];
            textyaxis = [12, 12, 13];
        elseif iroi == 8
            zlimits = [-20, 20];
            textyaxis = [-12, -12, -15];
       elseif iroi == 9
            zlimits = [-20, 20];
            textyaxis = [-12, -12, -15];
        end
        % start plotting
        figure('Position',[100 100 1600 800]);
        analyzeclusters_local(z_cont_pre, z_cont_post1, z_treat_pre, z_treat_post1, subj_idx_include,fontsize, zlimits, textyaxis)
        title({['Avg. z-score over voxels in the surviving cluster: ', cluster_names{cnt}];
            ['(Seed: ', masknames_print{iroi}, ')']
            [];
            },'fontweight','bold','fontsize',20)
        % save the plot
        exportgraphics(gcf,[result_save_dir, 'roi', num2str(iroi), '_clust', num2str(iclust), '.pdf'],'ContentType','vector')
        close(gcf)
        cnt = cnt+1;
    end
end

%% post-hoc analysis - Are there differences between men and women?
% p<0.005, alpha<0.05
% need cluster mask!

% directory where we saved the BRIK files of z-score maps for each  condition
z_score_save_dir = [result_save_root_dir, 'seeds_based_FC_ttests/BRIK_files/'];
% directory where we saved the masks of the significant clusters
clust_mask_save_dir = [result_save_root_dir, 'seeds_based_FC_ttests', ...
    '/AFNI_perm_post1-pre_treat-cont/cluster_result_p0005_a005_NN2_bisided/'];

% surviving clusters
roi_num = [2, 5, 8, 9]; % corresponding ROI indices
clust_num = [1, 1, 1, 1]; % number of significant clusters for each ROI
cluster_names = {'right superior temporal gyrus', 'right inferior parietal lobule', ...
    'right precuneus', 'right cuneus gyrus'}; % names of the clusters

% fontsize for plotting
fontsize = 14;

% separating male and female subejcts
male_subj = [2:4, 7, 13, 16:19, 21];
female_subj = [1, 5, 6, 8:12, 14, 15, 20];

% initialization
cnt = 1;
t = [];
p = [];
for i = 1:length(roi_num)
    iroi = roi_num(i);
    [~, clust_mask, ~, ~] = BrikLoad([clust_mask_save_dir, 'roi', num2str(iroi), '_order_mask+tlrc']);
    sz = size(clust_mask);
    clust_mask = reshape(clust_mask, sz(1)*sz(2)*sz(3), 1);
    % pre treat
    tmp_name = [z_score_save_dir, 'ROI', num2str(iroi), '_', num2str(nSub), 'subj', ...
        '_pre_treat'];
    [~, z_treat_pre_full_brain, ~, ~] = BrikLoad([tmp_name, '+tlrc']);
    z_treat_pre_full_brain = reshape(z_treat_pre_full_brain, sz(1)*sz(2)*sz(3), nSub);
    z_treat_pre_full_brain = z_treat_pre_full_brain(brainMask, :);
    % post treat
    tmp_name = [z_score_save_dir, 'ROI', num2str(iroi), '_', num2str(nSub), 'subj', ...
        '_post1_treat'];
    [~, z_treat_post_full_brain, ~, ~] = BrikLoad([tmp_name, '+tlrc']);
    z_treat_post_full_brain = reshape(z_treat_post_full_brain, sz(1)*sz(2)*sz(3), nSub);
    z_treat_post_full_brain = z_treat_post_full_brain(brainMask, :);
    % pre cont
    tmp_name = [z_score_save_dir, 'ROI', num2str(iroi), '_', num2str(nSub), 'subj', ...
        '_pre_cont'];
    [~, z_cont_pre_full_brain, ~, ~] = BrikLoad([tmp_name, '+tlrc']);
    z_cont_pre_full_brain = reshape(z_cont_pre_full_brain, sz(1)*sz(2)*sz(3), nSub);
    z_cont_pre_full_brain = z_cont_pre_full_brain(brainMask, :);
    % post cont
    tmp_name = [z_score_save_dir, 'ROI', num2str(iroi), '_', num2str(nSub), 'subj', ...
        '_post1_cont'];
    [~, z_cont_post_full_brain, ~, ~] = BrikLoad([tmp_name, '+tlrc']);
    z_cont_post_full_brain = reshape(z_cont_post_full_brain, sz(1)*sz(2)*sz(3), nSub);
    z_cont_post_full_brain = z_cont_post_full_brain(brainMask, :);
    
    for iclust = 1:clust_num(i)
        mask = clust_mask(brainMask) == iclust;
        z_treat_pre = mean(z_treat_pre_full_brain(mask, :));
        z_treat_post1 = mean(z_treat_post_full_brain(mask, :));
        z_cont_pre = mean(z_cont_pre_full_brain(mask, :));
        z_cont_post1 = mean(z_cont_post_full_brain(mask, :));
        z_mainContrast_clustAvg = (z_treat_post1-z_treat_pre) - ...
            (z_cont_post1 - z_cont_pre);
        
        [h,p(cnt),ci,stats] = ttest2(z_mainContrast_clustAvg(male_subj), ...
            z_mainContrast_clustAvg(female_subj));
        t(cnt) = stats.tstat;
        cnt = cnt+1;
    end
end
% the t-statistics and p values
t
p
%%
function results = analyzeclusters_local(control_pre, control_post, treatment_pre, treatment_post, include,fontsize, zlimits, textyaxis)
% include - the subjects to include in the plotting

if ~exist('extrainfo','var')
    extrainfo = ' ';
end

% global outpath mask_path mask


% Initialize results cell array. The format is:
% {1} - prefix info
% {2} - num voxels
% {3} - t-stats
%results = cell(1,numclusts);

% Defining legend labels for later
plotnames = cell(1,length(include));
for i=1:1:length(plotnames)
    iSub = include(i);
    plotnames{i} = ['Subj',num2str(iSub)]; % for internal, skip numbers so we know which subject we are talking about
    % plotnames{i} = ['Subj',num2str(i)]; % for poster, no need to skip numbers
end

% Defining scatter plot markers for later
bullets = {'o','+','x','s','d','^','v','>','<','p','o','+','x','s','d','^','v','>','<','p',...
    'o','+','x','s','d','^','v','>','<','p','o','+','x','s','d','^','v','>','<','p',...
    'o','+','x','s','d','^','v','>','<','p','o','+','x','s','d','^','v','>','<','p'};
%colors  = {'b','b','b','b','b','b','b','b','b','b','b','r','r','r','r','r','r','r','r','r'};
%colors  = {'r','r','r','r','r','r','r','r','r','r','r','r','r','r','r','r','r','r','r','r'}; % for poster, don't differentiate between new and old subjects
% colors  = {'m','c','r','g','b','k','m','c','r','g','b','k','m','c','r','g','b','k','m','c'}; % for poster, don't differentiate between new and old subjects
colors  = {'#0072BD','#D95319','#EDB120','#7E2F8E','#77AC30','#4DBEEE','#A2142F',...
    '#0072BD','#D95319','#EDB120','#7E2F8E','#77AC30','#4DBEEE','#A2142F',...
    '#0072BD','#D95319','#EDB120','#7E2F8E','#77AC30','#4DBEEE','#A2142F',...
    '#0072BD','#D95319','#EDB120','#7E2F8E','#77AC30','#4DBEEE','#A2142F',...
    '#0072BD','#D95319','#EDB120','#7E2F8E','#77AC30','#4DBEEE','#A2142F',...
    '#0072BD','#D95319','#EDB120','#7E2F8E','#77AC30','#4DBEEE','#A2142F',...
    '#0072BD','#D95319','#EDB120','#7E2F8E','#77AC30','#4DBEEE','#A2142F'
    };


names = {'Pre_{cont}','Post_{cont}', ...
    'Pre_{treat}','Post_{treat}', ...
    '(Post-Pre)_{cont}','(Post-Pre)_{treat}'};

% Now, given these masks, grab all the CBFs for each subject. Plot the CBF
% values within these masks.

numIncludedSubjects = length(include);

control_postminuspre = control_post - control_pre;
treatment_postminuspre = treatment_post - treatment_pre;

hold on;

% Looping through subjects to get the CBF data
for j=1:1:numIncludedSubjects
    
    % Indexing through subjects via the include vector
    iSub = include(j); % subject index
    
    %marker = [bullets{iSub},colors{iSub}]; % bullet style, color, line style
    marker = bullets{iSub};
    color = colors{iSub};
    % NaNs are to sever certain connections when plotting
    %{
        datatoplot = [treatmentcbfspost(j) treatmentcbfspre(j) NaN ...
            controlcbfspost(j) controlcbfspre(j) NaN ...
            treatmentcbfspostminuspre(j) controlcbfspostminuspre(j)];
    %}
    datatoplot = [control_pre(j) control_post(j) NaN ...
        treatment_pre(j) treatment_post(j) NaN ...
        control_postminuspre(j) treatment_postminuspre(j)];
    
    %scatter([1 2 NaN 3 4 NaN 5 6],datatoplot,marker,'LineWidth',2);
    %scatter([1 2 NaN 3 4 NaN 5 6],datatoplot,marker,'MarkerEdgeColor',color,'LineWidth',2);
    scatter([1 2 NaN 3 4 NaN 5 6],datatoplot,fontsize*5,marker,...
        'MarkerEdgeColor',color, 'LineWidth',2);
end


% A kludge - plotting a second time to add the lines. We could do both
% at once, but then we cannot exclude the line from the legend nor can
% we control the marker vs line widths independently.
for j=1:1:numIncludedSubjects
    
    % Indexing through subjects via the include vector
    iSub = include(j); % subject index
    linestyle = '--';
    color = colors{iSub};
    %{
        datatoplot = [treatmentcbfspost(j) treatmentcbfspre(j) NaN ...
            controlcbfspost(j) controlcbfspre(j) NaN ...
            treatmentcbfspostminuspre(j) controlcbfspostminuspre(j)];
    %}
    datatoplot = [control_pre(j) control_post(j) NaN ...
        treatment_pre(j) treatment_post(j) NaN ...
        control_postminuspre(j) treatment_postminuspre(j)];
    %plot([1 2 NaN 3 4 NaN 5 6],datatoplot,linestyle);
    plot([1 2 NaN 3 4 NaN 5 6],datatoplot,'Color',color,'LineStyle',linestyle);
end

% Creating figure
hold off;
%title(['Average CBF over Voxels in Cluster #',num2str(i),' (Cluster Size: ',num2str(clustersize),' Voxels) ',extrainfo]);
grid;
set(gca,'GridAlpha',0.35);
ylim([zlimits(1) zlimits(2)]); 
yticks(zlimits(1):5:zlimits(2));
ylabel('z score','FontSize',fontsize*1.2);
xlim([0.5 7.5]);
xticks([1 2 3 4 5 6]);
xticklabels(names);
legend(plotnames,'FontSize',round(fontsize*0.9));
ax = gca; ax.FontSize = fontsize*1.2; % Setting font size

%{
    % Adding extrainfo
    if exist('extrainfo','var')
        text(1.5,-20,extrainfo,'BackgroundColor','white');
    end
%}

% Post-hoc t-tests on the control and treatment data
[~,controlp,~,controlstats] = ttest(control_post,control_pre);
control_post_mean = mean(control_post);
control_pre_mean = mean(control_pre);
control_delta = control_post_mean - control_pre_mean;
control_percentdelta = (control_delta)/control_pre_mean*100;

[~,treatmentp,~,treatmentstats] = ttest(treatment_post,treatment_pre);
treatment_post_mean = mean(treatment_post);
treatment_pre_mean = mean(treatment_pre);
treatment_delta = treatment_post_mean - treatment_pre_mean;
treatment_percentdelta = (treatment_delta)/treatment_pre_mean*100;

[~,combinedp,~,combinedstats] = ttest(treatment_post-treatment_pre,control_post-control_pre);
combined_post_mean = mean(treatment_post-treatment_pre);
combined_pre_mean = mean(control_post-control_pre);
combined_delta = combined_post_mean - combined_pre_mean;
% combinedcbfs_percentdelta =
% (combinedcbfs_delta)/combinedcbfspre_mean*100; % percent doesn't make
% sense if there are negative denominators involved

results.controlp = controlp;
results.controlt = controlstats.tstat;
results.controlcbfspost_mean = control_post_mean;
results.controlcbfspre_mean = control_pre_mean;
results.controlcbfs_delta = control_delta;
results.controlcbfs_percentdelta = control_percentdelta;

results.treatmentp = treatmentp;
results.treatmentt = treatmentstats.tstat;
results.treatmentcbfspost_mean = treatment_post_mean;
results.treatmentcbfspre_mean = treatment_pre_mean;
results.treatmentcbfs_delta = treatment_delta;
results.treatmentcbfs_percentdelta = treatment_percentdelta;

results.contrastp = combinedp;
results.contrastt = combinedstats.tstat;

tdecimalplaces = 3;
ddecimalplaces = 3;
psigfigs = 2;

controlcomments = {
    ['Post_{cont} - Pre_{cont}'], ...
    ['t(', num2str(numIncludedSubjects-1), ') = ',sprintf(['%.',num2str(tdecimalplaces),'f'],controlstats.tstat)],...
    ['p = ',sprintf(['%.',pvaldecformat(controlp,psigfigs),'f'],controlp)],...
    ['d = ',sprintf(['%.',num2str(ddecimalplaces),'f'],controlstats.tstat/sqrt(numIncludedSubjects))]
    };

% '$\overline{C}_{Prop} / \overline{C}_{PropMax}$'
% ['\DeltaCBF = ',num2str(controlcbfs_delta,4),' mL/100g/min'],...
% text(1.05,-15,controlcomments,'BackgroundColor','white','Interpreter','latex','FontSize',fontsize);
text(1.05,textyaxis(1),controlcomments,'BackgroundColor','white','FontSize',fontsize);

treatmentcomments = {
    ['Post_{treat} - Pre_{treat}'], ...
    ['t(', num2str(numIncludedSubjects-1), ') = ',sprintf(['%.',num2str(tdecimalplaces),'f'],treatmentstats.tstat)],...
    ['p = ',sprintf(['%.',pvaldecformat(treatmentp,psigfigs),'f'],treatmentp)],...
    ['d = ',sprintf(['%.',num2str(ddecimalplaces),'f'],treatmentstats.tstat/sqrt(numIncludedSubjects))]
    };
% text(3.05,-15,treatmentcomments,'BackgroundColor','white','Interpreter','latex','FontSize',fontsize);
text(3.05,textyaxis(2),treatmentcomments,'BackgroundColor','white','FontSize',fontsize);

combinedcomments = {
    ['(Post-Pre)_{treat} - (Post-Pre)_{cont}'], ...
    ['t(', num2str(numIncludedSubjects-1), ') = ',sprintf(['%.',num2str(tdecimalplaces),'f'],combinedstats.tstat)],...
    ['p = ',sprintf(['%.',pvaldecformat(combinedp,psigfigs),'f'],combinedp)], ...
    ['d = ',sprintf(['%.',num2str(ddecimalplaces),'f'],combinedstats.tstat/sqrt(numIncludedSubjects))]
    };
% text(5.05,-15,combinedcomments,'BackgroundColor','white','Interpreter','latex','FontSize',fontsize);
text(5.05,textyaxis(3),combinedcomments,'BackgroundColor','white','FontSize',fontsize);


%saveas(gcf,[prefix,'clust',num2str(i),'.png']);

% Also saving off the cluster
%clustervol = zeros(size(mask));
%clustervol(cluster) = 1;

%Mat2AFNIv2(LPIandRAI(clustervol),[prefix,'clust',num2str(i)],[outpath,mask_path(1:end-5)]);
%Mat2AFNIv2(LPIandRAI(residual),[procdir,'Resid_Subj',sprintf('%02d',iSub)],[outpath,mask_path(1:end-5)]);
end

