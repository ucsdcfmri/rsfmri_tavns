%% The script for the post-hoc t-tests using the ROIs in the left and right hemisphere .
% Description
% This script does:
% 1. calculate the ROI-to-cluster correlations (optional),
% 2. calculate paired t-tests on the ROI-to-cluster correlations for the
% main contrast.

% Note, please make sure that you have run groupAnalysis.m before running
% this script if you would like to calculate the ROI-to-cluster correlations.

% Dependencies: 
% 1. AFNI (Version AFNI_21.2.04 'Nerva')

% Authors:  Yixiang Mao (y1mao@ucsd.ued)
close all; clear all; clc;
%% Define paths
% ----------- Please set the following variables properly -----------------
% define the directory where this repo is located
repo_dir = ['/home/ymao/git_repo/rsfmri_tavns/'];
addpath(genpath(repo_dir))

% 
flag_calcCorr = 0; % 0: Load the existing ROI-to-cluster correlations.
                     % 1: Re-calculate the ROI-to-cluster correlations
                     %      from the BOLD signal.
% -------------------------------------------------------------------------

% ---- If flag_calcCorr = 1, please set the following dirs properly------
if flag_calcCorr == 1 % Re-calculate the ROI-to-cluster correlations
    % processed data save root directory
    masterPath = ['/data/students_group/data/vorso_reProc_test/'];
    
    % the name of the folder where the functional data after GLM are saved
    dataProc_save_folder = ['_reg_topup_ts_aw_4mm_GLM_Poly2_Mo1_WMCSF5aCompCor_RVHRCOR1_GS0_tlrc_202112281223/', ...
        'FuncFilt_bandpass10to100mHz_Mocen_FD_thres01_FDfilt_low0to100mHz/'];
    funcName_suffix = ['_proc_PercChng_FuncFilt_MoCen+tlrc'];
end
% -------------------------------------------------------------------------

% ------ The following variables properly do NOT need to be changed---------
% the results save dir
if flag_calcCorr == 1 % Re-calculate the ROI-to-cluster correlations
    result_save_root_dir = [masterPath, dataProc_save_folder, 'seeds_based_FC_analysis/'];
end

% included subjects
subj_idx_include = [1:6, 8:16, 20:24, 26]; % the index of subjects that will be included in the analysis

% scan info
nvol = 363;
TR = 0.96;
Fs = 1/TR;

% directory where we save the seed masks and cluster masks
maskpath = [repo_dir, 'masks_and_templates/'];

% names of the ROI masks
masknames = {
    'CG_7_2_mask_rs+tlrc'; 
    'CG_7_5_mask_rs+tlrc'; 
    'INS_6_1_mask_rs+tlrc';
    'INS_6_2_mask_rs+tlrc';
    };
masknames_print = {
    'A24rv'; 
    'A24cd'; 
    'hypergranular insula';
    'ventral agranular insula';
    };
left_masknames = {
    'CG_L_7_2_mask_rs+tlrc'
    'CG_L_7_5_mask_rs+tlrc'; 
    'INS_L_6_1_mask_rs+tlrc';
    'INS_L_6_2_mask_rs+tlrc';
    };
L_masknames_print = {
    'left A24rv'; 
    'left A24cd';
    'left hypergranular insula';
    'left ventral agranular insula';
    };
right_masknames = {
    'CG_R_7_2_mask_rs+tlrc'
    'CG_R_7_5_mask_rs+tlrc'; 
    'INS_R_6_1_mask_rs+tlrc';
    'INS_R_6_2_mask_rs+tlrc';
    };
R_masknames_print = {
    'right A24rv'; 
    'right A24cd';
    'right hypergranular insula';
    'right ventral agranular insula';
    };

% directory where we saved the masks of the significant clusters
clust_mask_save_dir = [result_save_root_dir, 'seeds_based_FC_ttests', ...
    '/AFNI_perm_post1-pre_treat-cont/cluster_result_p0005_a005_NN2_bisided/'];
clust_masknames = {
    'roi2_cluster1_mask+tlrc'; 
    'roi5_cluster1_mask+tlrc';
    'roi8_cluster1_mask+tlrc'; 
    'roi9_cluster1_mask+tlrc';
    };
clust_masknames_print = {
    'right STG'; 
    'right IPL';
    'right precuneus';
    'right rostral cuneus gyrus';
    };
%% Read the masks
if flag_calcCorr == 1
    % read the brain mask
    brainMask3d = BrikLoad( [maskpath, ...
        'mni_icbm152_t1_tal_nlin_asym_09a_brain_rs_automask2+tlrc'] );
    brainMask3d = logical(brainMask3d);
    sz = size(brainMask3d);
    brainMask = reshape(brainMask3d, sz(1)*sz(2)*sz(3),1 );
    brainMask = logical(brainMask);
    % read the ROI masks
    % bilateral
    roiMasks =[];
    for imask=1:length(masknames)

        mask_tmp =[];
        mask_tmp = BrikLoad( [maskpath ,masknames{imask}] );
        mask_tmp = reshape(mask_tmp, sz(1)*sz(2)*sz(3),1 );
        roiMasks = [roiMasks, mask_tmp];
    end
    roiMasks = logical(roiMasks);
    sum(roiMasks)
    sum(roiMasks(brainMask, :))
    % left roi
    L_roiMasks =[];
    for imask=1:length(left_masknames)
        mask_tmp =[];
        mask_tmp = BrikLoad( [maskpath ,left_masknames{imask}] );
        mask_tmp = reshape(mask_tmp, sz(1)*sz(2)*sz(3),1 );
        L_roiMasks = [L_roiMasks, mask_tmp];
    end
    L_roiMasks = logical(L_roiMasks);
    sum(L_roiMasks)
    sum(L_roiMasks(brainMask, :))
    % right roi
    R_roiMasks =[];
    for imask=1:length(right_masknames)
        mask_tmp =[];
        mask_tmp = BrikLoad( [maskpath ,right_masknames{imask}] );
        mask_tmp = reshape(mask_tmp, sz(1)*sz(2)*sz(3),1 );
        R_roiMasks = [R_roiMasks, mask_tmp];
    end
    R_roiMasks = logical(R_roiMasks);
    sum(R_roiMasks)
    sum(R_roiMasks(brainMask, :))
    % read the clust masks
    clustMasks =[];
    for imask=1:length(clust_masknames)

        mask_tmp =[];
        mask_tmp = BrikLoad( [clust_mask_save_dir ,clust_masknames{imask}] );
        mask_tmp = reshape(mask_tmp, sz(1)*sz(2)*sz(3),1 );
        clustMasks = [clustMasks, mask_tmp]; 
    end
    clustMasks = logical(clustMasks);
    sum(clustMasks)
    sum(clustMasks(brainMask, :))
end
%% calculate the ROI-to-cluster corrlations
if flag_calcCorr == 1 % Re-calculate the ROI-to-cluster correlations
    % initialization
    r_clust_roi = [];
    r_clust_L_roi = [];
    r_clust_R_roi = [];
    z_clust_roi = [];
    z_clust_L_roi = [];
    z_clust_R_roi = [];
    if exist([result_save_root_dir, 'post_hoc_left_and_right.mat'], 'file')
        % in case the ROI-to-cluster correlations have been already re-calculated
        load([result_save_root_dir, 'post_hoc_left_and_right.mat'],...
            'r_clust_roi', 'r_clust_L_roi', 'r_clust_R_roi', ...
            'z_clust_roi', 'z_clust_L_roi', 'z_clust_R_roi')
    else % re-calculate the ROI-to-cluster correlations
        cd([masterPath, dataProc_save_folder])
        for isub  = subj_idx_include
            for iday = 1:2
                
                if iday == 1
                    treat_or_cont = 'treat';
                else
                    treat_or_cont = 'cont';
                end
                
                subj_str = ['sub-', treat_or_cont, num2str(isub, '%02.f')];
                for iscan = 1:2
                    ses_str = ['ses-', num2str(iscan, '%02.f')];
                    % ses-01: pre session
                    % ses-02: post session
                    
                    subj_info_str_for_save = ['subj', num2str(isub), '_', treat_or_cont, '_', ses_str];
                    
                    ['Processing: ', subj_info_str_for_save]
                    
                    % the input functional data name
                    funcName_input = [subj_str, '_', ses_str, ...
                        funcName_suffix];
                    
                    % load the funcitonal data
                    funcData = BrikLoad(funcName_input);
                    sz = size(funcData);
                    funcData_rs = reshape(funcData, sz(1)*sz(2)*sz(3), sz(4));
                    funcData_rs_masked = funcData_rs(brainMask, :);
                    
                    funcRoi = [];
                    for imask=1:size(roiMasks, 2)
                        if sum(roiMasks(:,imask))==1
                            funcRoi(imask,:) = funcData_rs( roiMasks(:,imask), : );
                        else
                            funcRoi(imask,:) = mean(funcData_rs( roiMasks(:,imask), : ));
                        end
                    end
                    
                    func_L_Roi = [];
                    for imask=1:size(L_roiMasks, 2)
                        if sum(L_roiMasks(:,imask))==1
                            func_L_Roi(imask,:) = funcData_rs( L_roiMasks(:,imask), : );
                        else
                            func_L_Roi(imask,:) = mean(funcData_rs( L_roiMasks(:,imask), : ));
                        end
                    end
                    func_R_Roi = [];
                    for imask=1:size(R_roiMasks, 2)
                        if sum(R_roiMasks(:,imask))==1
                            func_R_Roi(imask,:) = funcData_rs( R_roiMasks(:,imask), : );
                        else
                            func_R_Roi(imask,:) = mean(funcData_rs( R_roiMasks(:,imask), : ));
                        end
                    end
                    
                    func_clust = [];
                    for imask=1:size(clustMasks, 2)
                        if sum(clustMasks(:,imask))==1
                            func_clust(imask,:) = funcData_rs( clustMasks(:,imask), : );
                        else
                            func_clust(imask,:) = mean(funcData_rs( clustMasks(:,imask), : ));
                        end
                    end
                    
                    for iclust = 1:size(clustMasks, 2)
                        [r_clust_roi(iclust,isub,iday,iscan),...
                            p_clust_roi(iclust,isub,iday,iscan),...
                            z_clust_roi(iclust,isub,iday,iscan)] = ...
                            mcorr( funcRoi(iclust, :)',func_clust(iclust, :)');
                        
                        [r_clust_L_roi(iclust,isub,iday,iscan),...
                            p_clust_L_roi(iclust,isub,iday,iscan),...
                            z_clust_L_roi(iclust,isub,iday,iscan)] = ...
                            mcorr( func_L_Roi(iclust, :)',func_clust(iclust, :)');
                        
                        [r_clust_R_roi(iclust,isub,iday,iscan),...
                            p_clust_R_roi(iclust,isub,iday,iscan),...
                            z_clust_R_roi(iclust,isub,iday,iscan)] = ...
                            mcorr( func_R_Roi(iclust, :)',func_clust(iclust, :)');
                    end
                end
                
            end
        end
        save([result_save_root_dir, 'post_hoc_left_and_right.mat'],...
            'r_clust_roi', 'r_clust_L_roi', 'r_clust_R_roi', ...
            'z_clust_roi', 'z_clust_L_roi', 'z_clust_R_roi', ...
            'subj_idx_include', '-v7.3', '-nocompression')
    end
else % load the existing ROI-to-cluster correlations
    load([repo_dir, '/summaryData/post_hoc_left_and_right.mat'],...
            'r_clust_roi', 'r_clust_L_roi', 'r_clust_R_roi', ...
            'z_clust_roi', 'z_clust_L_roi', 'z_clust_R_roi')
end

%% Setting for the statistical tests
nSub = length(subj_idx_include);

% assign numbers to different sections
treat_session = 1;
cont_session = 2;
prescan = 1;
postscan = 2;

% calculate the z-scores for the main contrast: (post-pre)_treat - (post-pre)_cont
z_clust_L_roi_treat_pre(:, :)  =  squeeze(z_clust_L_roi(:, subj_idx_include, treat_session, prescan));
z_clust_L_roi_treat_post(:, :)  =  squeeze(z_clust_L_roi(:, subj_idx_include, treat_session, postscan));
z_clust_L_roi_cont_pre(:, :)  =  squeeze(z_clust_L_roi(:, subj_idx_include, cont_session, prescan));
z_clust_L_roi_cont_post(:, :)  =  squeeze(z_clust_L_roi(:, subj_idx_include, cont_session, postscan));

z_clust_R_roi_treat_pre(:, :)  =  squeeze(z_clust_R_roi(:, subj_idx_include, treat_session, prescan));
z_clust_R_roi_treat_post(:, :)  =  squeeze(z_clust_R_roi(:, subj_idx_include, treat_session, postscan));
z_clust_R_roi_cont_pre(:, :)  =  squeeze(z_clust_R_roi(:, subj_idx_include, cont_session, prescan));
z_clust_R_roi_cont_post(:, :)  =  squeeze(z_clust_R_roi(:, subj_idx_include, cont_session, postscan));

% post-pre
z_clust_L_roi_treat_post_pre  =  z_clust_L_roi_treat_post - z_clust_L_roi_treat_pre;
z_clust_L_roi_cont_post_pre  =  z_clust_L_roi_cont_post - z_clust_L_roi_cont_pre;

z_clust_R_roi_treat_post_pre  =  z_clust_R_roi_treat_post - z_clust_R_roi_treat_pre;
z_clust_R_roi_cont_post_pre =  z_clust_R_roi_cont_post - z_clust_R_roi_cont_pre;

% (post-pre)treat - (post-pre)cont
z_clust_L_roi_treat_cont_post_pre  = z_clust_L_roi_treat_post_pre - z_clust_L_roi_cont_post_pre;

z_clust_R_roi_treat_cont_post_pre  = z_clust_R_roi_treat_post_pre - z_clust_R_roi_cont_post_pre;


%% post-hoc ttests
% main contrast: (Post-Pre)traetment - (Post-Pre)control
% left ROI to cluster 
p_clust_L_roi_mainContrast = [];
t_clust_L_roi_mainContrast = [];
% right ROI to cluster
p_clust_R_roi_mainContrast = [];
t_clust_R_roi_mainContrast = [];

% start t-tests
for iclust = 1:length(clust_masknames)
    [~,p_clust_L_roi_mainContrast(iclust),~,stats] = ...
        ttest2(z_clust_L_roi_treat_post_pre(iclust, :)', z_clust_L_roi_cont_post_pre(iclust, :)');
    t_clust_L_roi_mainContrast(iclust) = stats.tstat;
    
    [~,p_clust_R_roi_mainContrast(iclust),~,stats] = ...
        ttest2(z_clust_R_roi_treat_post_pre(iclust, :)', z_clust_R_roi_cont_post_pre(iclust, :)');
    t_clust_R_roi_mainContrast(iclust) = stats.tstat;
end
% the t-statistics and p values
% left ROIs to clusters
t_clust_L_roi_mainContrast
p_clust_L_roi_mainContrast
% right ROIs to clusters
t_clust_R_roi_mainContrast
p_clust_R_roi_mainContrast