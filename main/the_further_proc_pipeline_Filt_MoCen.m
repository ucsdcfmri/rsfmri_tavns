%% the further preprocessing pipeline - filtering and motion censoring
% Description
% This pipeline does:
% 1. Filtering.
% 2. Motion censoring.

% Dependencies: 

% Authors:  Yixiang Mao (y1mao@ucsd.ued)

close all; clearvars; clc;
%% Define paths
% add the paths to other functions in the repo
repo_dir = ['/home/ymao/git_repo/rsfmri_tavns/'];
addpath(genpath(repo_dir))

% raw data root directory
raw_data_root_dir = '/data/students_group/data/vorso_BIDS_skull_stripped/';

% processed data save root directory
ProcData_save_root_dir = ['/data/students_group/data/vorso_reProc_test/'];
if ~exist(ProcData_save_root_dir, 'dir')
    mkdir(ProcData_save_root_dir)
end

% the name of the folder where we save the data after GLM
GLM_folder_name = ['_reg_topup_ts_aw_4mm_GLM_Poly2_Mo1_WMCSF5aCompCor_RVHRCOR1_GS0_tlrc_202112281223/'];
% the folder where we save the functional data after GLM
ProcFunc_afterGLM_save_dir = [ProcData_save_root_dir, GLM_folder_name];

%% the indices of subjects to be processed
% note, subject 18 and 19 didn't complete the whole experiment, hence are
% not processed
subj_idx_include = [1:17, 20:27];

%% scan parameters
TR = 0.96; % Repetition time: 0.96 seconds
%% Read the brain masks
brain_mask_dir = [repo_dir, 'masks_and_templates/', ...
    'mni_icbm152_t1_tal_nlin_asym_09a_brain_rs_automask2+tlrc'];
brainMask3d = BrikLoad(brain_mask_dir);
brainMask3d = logical(brainMask3d);
sz = size(brainMask3d);
brainMask = reshape(brainMask3d, sz(1)*sz(2)*sz(3),1 );
brainMask = logical(brainMask);

%% options
% settings about the functional data filtering
FuncFilt_type = 'bandpass'; % 'low': lowpass filerting
                            % 'bandpass': bandpass filtering
                            % []: don't applying filtering
FuncFilt_order = 2; % the order of the Butterworth filter
FuncFilt_stopband = [0.01 0.1]; % Passband edges.
                                % In case of lowpass filtering, i.e.
                                % [FuncFilt_type] = 'low', assign 0 to the
                                % first element of [FuncFilt_stopband] and
                                % assign the lowpass stopband you want to
                                % the second element of
                                % [FuncFilt_stopband].
FuncFilt_sanityPlot = 1; % 1: plot the sanity checks
                         % 0: don't plot the sanity checks                       
FuncFilt_str = ['FuncFilt_', FuncFilt_type, num2str(FuncFilt_stopband(1)*1000),...
        'to', num2str(FuncFilt_stopband(2)*1000), 'mHz']; 
% the str that shows the parameters used in functional filtering, is
% not used when [FuncFilt_type] = [];
    
% settings about the motion censoring
MoCen_criteria = 'FD'; % The criteria we used to determine motion
                       % timepoints to be deleted.
                       % 'FD': framewise displacement (mm).
                       % []: don't do motion censoring
MoCen_FDthres =  0.1; % The FD threshold (mm, is used only when FD is used as
                      % a criterion). Timepoints with FD>[MoCen_FDthres]
                      % will be regarded as motion timepoints hence be
                      % discarded from further analysis.
MoCen_FDFilt_type = 'low'; % 'low': lowpass filerting
                           % 'bandpass': bandpass filtering
                           % []: don't applying filtering
MoCen_FDFilt_order = 4; % the order of the Butterworth filter
MoCen_FDFilt_stopband = [0 0.1]; % Passband edges.
                             % In case of lowpass filtering, i.e.
                             % [FuncFilt_type] = 'low', assign 0 to the
                             % first element of [FuncFilt_stopband] and
                             % assign the lowpass stopband you want to
                             % the second element of
                             % [FuncFilt_stopband].
if isempty(MoCen_FDFilt_type)                              
    MoCen_str = ['Mocen_', MoCen_criteria, '_thres', ...
        num2str(MoCen_FDthres*10, '%02.f')];
else
    MoCen_str = ['Mocen_', MoCen_criteria, '_thres', ...
        num2str(MoCen_FDthres*10, '%02.f'), '_FDfilt_', MoCen_FDFilt_type, ...
        num2str(MoCen_FDFilt_stopband(1)*1000), 'to', ...
        num2str(MoCen_FDFilt_stopband(2)*1000), 'mHz'];
end
% the str that shows the parameters used in motion censoring, is
% not used when [MoCen_criteria] = [];    

% create the folder where we want to save the data after filtering and
% motion censoring
if ~isempty(FuncFilt_type) && ~isempty(MoCen_criteria)
    ProcData_save_dir = [ProcData_save_root_dir, GLM_folder_name, ...
        FuncFilt_str, '_', MoCen_str, '/'];
elseif ~isempty(FuncFilt_type) && isempty(MoCen_criteria)
    ProcData_save_dir = [ProcData_save_root_dir, GLM_folder_name, ...
        FuncFilt_str, '/'];
elseif isempty(FuncFilt_type) && ~isempty(MoCen_criteria)
    ProcData_save_dir = [ProcData_save_root_dir, GLM_folder_name, ...
        MoCen_str, '/'];
elseif isempty(FuncFilt_type) && isempty(MoCen_criteria)
    error('Why you run this script?')
end
if ~exist(ProcData_save_dir, 'dir')
    mkdir(ProcData_save_dir)
end

% create the folder where we want to save the functional data sanity-check
% plots
if FuncFilt_sanityPlot
    FuncFilt_sanityPlot_save_dir = [ProcData_save_dir, 'FuncFilt_sanityPlots/'];
    if ~exist(FuncFilt_sanityPlot_save_dir, 'dir')
        mkdir(FuncFilt_sanityPlot_save_dir)
    end
end
line_width = 1.5;
label_size = 12;
%% filtering the functional data
if ~isempty(FuncFilt_type)
    % construct the filter
    if strcmp(FuncFilt_type, 'low')
        Wn = FuncFilt_stopband(2)/(1/TR/2);
    elseif strcmp(FuncFilt_type, 'bandpass')
        Wn = FuncFilt_stopband/(1/TR/2);
    end
    [b,a] = butter(FuncFilt_order, Wn, FuncFilt_type);

    % plot the magnitude of the designed filter
    h = fvtool(b,a);
    h.Fs = 1/TR;
    saveas(gcf, [ProcData_save_dir, 'FuncFilt_MagResp_', FuncFilt_type, ...
        num2str(FuncFilt_stopband(1)*1000),...
        'to', num2str(FuncFilt_stopband(2)*1000), 'mHz'], 'png')
    close(gcf)
    % start filtering
    for isub  = subj_idx_include
        for iday = 1:2

            if iday == 1
                treat_or_cont = 'treat';
            else
                treat_or_cont = 'cont';
            end

            subj_str = ['sub-', treat_or_cont, num2str(isub, '%02.f')];
            for iscan = 1:2
                ses_str = ['ses-', num2str(iscan, '%02.f')];
                % ses-01: pre session
                % ses-02: post session
                if iscan == 1
                    pre_or_post = 'pre';
                else
                    pre_or_post = 'post';
                end

                % the data info str used for figure title and figure names
                subj_info_str = ['subject: ', num2str(isub), ' ', treat_or_cont,...
                    ' ', pre_or_post, '(', ses_str, ')'];
                subj_info_str_for_save= [subj_str, '_', ses_str];


                % the folder where we save processed the anatomical data
                ProcAnat_save_dir = [ProcData_save_root_dir, subj_str,...
                    '/', ses_str, '/anat/'];
                % the folder where we save processed the functional data
                ProcFunc_save_dir = [ProcData_save_root_dir, subj_str,...
                    '/', ses_str, '/func/'];

                % go to the direcotry where we want the processed functional
                % data to be saved
                cd(ProcData_save_dir);

                % the input functional data name
                funcName_input = [ProcFunc_afterGLM_save_dir, subj_str, '_', ses_str, ...
                    '_proc_PercChng+tlrc'];

                % the output functional data name
                funcName_output = [subj_str, '_', ses_str, ...
                    '_proc_PercChng_FuncFilt+tlrc'];

                % load the input functional data
                funcData_input = BrikLoad(funcName_input);
                sz = size(funcData_input);
                nvol = sz(4);
                funcData_input_rs = reshape(funcData_input, sz(1)*sz(2)*sz(3), sz(4));

                % applying filtering to the functional data
                funcData_output_rs = filtfilt(b, a, funcData_input_rs')';

                % save the filtered functional data
                funcData_output = reshape(funcData_output_rs, sz(1), sz(2), sz(3), sz(4));
                Mat2AFNI(double(funcData_output), funcName_output,  ...
                    funcName_input, 3);
                
                % add the TR info back
                cmd = ['3drefit -TR ', num2str(TR), ' ' funcName_output];
                system(cmd)
                if FuncFilt_sanityPlot
                    % sanity check: compare the spectra of a random voxel
                    the_rand_vox = randi(sum(brainMask), 1);
                    funcData_tmp = funcData_input_rs(brainMask, :);
                    funcData_tmp = funcData_tmp(the_rand_vox, :);
                    funcData_filt_tmp = funcData_output_rs(brainMask, :);
                    funcData_filt_tmp = funcData_filt_tmp(the_rand_vox, :);
                    % spetra of the random voxel
                    nfft = sz(4);
                    [dF,freq]   = my_yfft(funcData_tmp,nfft,1/TR);
                    power_tmp  = abs(dF).^2./nfft;
                    phase_tmp = angle(dF);
                    % spetra of the random voxel after filtering
                    nfft = sz(4);
                    [dF,freq]   = my_yfft(funcData_filt_tmp,nfft,1/TR);
                    power_tmp_filtered  = abs(dF).^2./nfft;
                    phase_tmp_filtered = angle(dF);

                    %
                    figure('Renderer', 'painters', 'Position', [10 10 1200 800]);
                    % before filtering
                    subplot(3, 2, 1)
                    plot(1:sz(4), funcData_tmp,'b', 'LineWidth', line_width)
                    xlabel('volume','fontweight','bold','fontsize',label_size)
                    ylabel('the BOLD signal (% Change)','fontweight','bold','fontsize',label_size)
                    grid on
                    xlim([1, nvol])
                    title('(a) The signal of the random selected voxel',...
                        'fontweight','bold','fontsize',label_size)

                    subplot(3, 2, 3)
                    plot(freq, power_tmp, 'r', 'LineWidth', line_width)
                    xlabel('frequency (Hz)','fontweight','bold','fontsize',label_size)
                    ylabel('power','fontweight','bold','fontsize',label_size)
                    grid on
                    xlim([0, 1/TR/2])
                    title('(b) The power spectrum of the random selected voxel',...
                        'fontweight','bold','fontsize',label_size)

                    subplot(3, 2, 5)
                    plot(freq, phase_tmp, 'r', 'LineWidth', line_width)
                    xlabel('frequency (Hz)','fontweight','bold','fontsize',label_size)
                    ylabel('phase (radians)','fontweight','bold','fontsize',label_size)
                    grid on
                    xlim([0, 1/TR/2])
                    title('(c) The phase of the random selected voxel',...
                        'fontweight','bold','fontsize',label_size)

                    % after filtering
                    subplot(3, 2, 2)
                    plot(1:nvol, funcData_filt_tmp,'b', 'LineWidth', line_width)
                    xlabel('volume','fontweight','bold','fontsize',label_size)
                    ylabel('the BOLD signal (% Change)','fontweight','bold','fontsize',label_size)
                    grid on
                    xlim([1, nvol])
                    title('(d) The signal of the random selected voxel (filtered)',...
                        'fontweight','bold','fontsize',label_size)

                    subplot(3, 2, 4)
                    plot(freq, power_tmp_filtered, 'r', 'LineWidth', line_width)
                    xlabel('frequency (Hz)','fontweight','bold','fontsize',label_size)
                    ylabel('power','fontweight','bold','fontsize',label_size)
                    grid on
                    xlim([0, 1/TR/2])
                    title('(e) The power spectrum of the random selected voxel (filtered)',...
                        'fontweight','bold','fontsize',label_size)

                    subplot(3, 2, 6)
                    plot(freq(freq>=FuncFilt_stopband(1)&freq<=FuncFilt_stopband(2)),...
                        phase_tmp_filtered(freq>=FuncFilt_stopband(1)&freq<=FuncFilt_stopband(2), 1),...
                        'r', 'LineWidth', line_width)
                    xlabel('frequency (Hz)','fontweight','bold','fontsize',label_size)
                    ylabel('phase (radians)','fontweight','bold','fontsize',label_size)
                    grid on
                    xlim([0, 1/TR/2])
                    title('(f) The phase of the random selected voxel (filtered)',...
                        'fontweight','bold','fontsize',label_size)

                    sgtitle({['The timeseries and power spectra of the random selected voxel before and after filtering'];
                        ['filter paras: ', FuncFilt_type, ...
                        ', passband edges: [', num2str(FuncFilt_stopband(1)),...
                        ', ', num2str(FuncFilt_stopband(2)), ']'];
                        subj_info_str;
                        ['random selected voxel index: ', num2str(the_rand_vox)]},...
                        'fontweight','bold','fontsize',label_size)
                    saveas(gcf, [FuncFilt_sanityPlot_save_dir, subj_info_str_for_save], 'png')
                    close(gcf)
                end
            end

        end

    end
end

%% motion censoring
if strcmp(MoCen_criteria, 'FD')
    if ~isempty(MoCen_FDFilt_type)
        % construct the filter
        if strcmp(MoCen_FDFilt_type, 'low')
            Wn = MoCen_FDFilt_stopband(2)/(1/TR/2);
        elseif strcmp(MoCen_FDFilt_type, 'bandpass')
            Wn = MoCen_FDFilt_stopband/(1/TR/2);
        end
        [b,a] = butter(MoCen_FDFilt_order, Wn, MoCen_FDFilt_type);

        % plot the magnitude of the designed filter
        h = fvtool(b,a);
        h.Fs = 1/TR;
        saveas(gcf, [ProcData_save_dir, 'FDFilt_MagResp_', MoCen_FDFilt_type, ...
            num2str(MoCen_FDFilt_stopband(1)*1000),...
            'to', num2str(MoCen_FDFilt_stopband(2)*1000), 'mHz'], 'png')
        close(gcf)
    end
    for isub  = subj_idx_include
        for iday = 1:2

            if iday == 1
                treat_or_cont = 'treat';
            else
                treat_or_cont = 'cont';
            end

            subj_str = ['sub-', treat_or_cont, num2str(isub, '%02.f')];
            for iscan = 1:2
                ses_str = ['ses-', num2str(iscan, '%02.f')];
                % ses-01: pre session
                % ses-02: post session
                if iscan == 1
                    pre_or_post = 'pre';
                else
                    pre_or_post = 'post';
                end

                % the data info str used for figure title and figure names
                subj_info_str = ['subject: ', num2str(isub), ' ', treat_or_cont,...
                    ' ', pre_or_post, '(', ses_str, ')'];
                subj_info_str_for_save= [subj_str, '_', ses_str];


                % the folder where we save processed the anatomical data
                ProcAnat_save_dir = [ProcData_save_root_dir, subj_str,...
                    '/', ses_str, '/anat/'];
                % the folder where we save processed the functional data
                ProcFunc_save_dir = [ProcData_save_root_dir, subj_str,...
                    '/', ses_str, '/func/'];
                % the folder where we save the raw physiological data
                rawPhyiso_save_dir = [ProcData_save_root_dir, subj_str,...
                    '/', ses_str, '/physio/'];

                % the motion file name
                if isempty(MoCen_FDFilt_type) % no motion paramters filtering 
                    motionName = [ProcFunc_save_dir, subj_str, '_', ses_str, ...
                        '_task-rest_bold_reg_topup_motion.1D'];
                else % filter the motion parameters
                    motionName = [ProcFunc_save_dir, subj_str, '_', ses_str, ...
                        '_task-rest_bold_reg_topup_motion_filt_', ...
                        MoCen_FDFilt_type, ...
                        num2str(MoCen_FDFilt_stopband(1)*1000), 'to', ...
                        num2str(MoCen_FDFilt_stopband(2)*1000), 'mHz.txt'];
                    if ~isfile(motionName) % no existing filtered motion parameters
                        % filter the motion parameters
                        motionName_input = [ProcFunc_save_dir, subj_str, '_', ses_str, ...
                            '_task-rest_bold_reg_topup_motion.1D'];
                        motionName_output = [ProcFunc_save_dir, subj_str, '_', ses_str, ...
                            '_task-rest_bold_reg_topup_motion_filt_', ...
                            MoCen_FDFilt_type, ...
                            num2str(MoCen_FDFilt_stopband(1)*1000), 'to', ...
                            num2str(MoCen_FDFilt_stopband(2)*1000), 'mHz.txt'];
                        % load the motion parameters before filtering
                        motionData_input = load(motionName_input);
                        % filter the motion parameters
                        for i = 2:7
                            tmp=filtfilt(b,a,motionData_input(:, i));
                            motionData_output(:, i) = tmp;
                        end
                        % write the filtered motion parameters
                        writematrix(motionData_output ,[motionName_output],'Delimiter','tab')
                    end
                end

                % load the motion parameters
                motionData = load(motionName);
                
                % calculate the FD
                radius=50;
                moreg = [motionData(:,5:7), motionData(:,2:4)];
                delta_moreg = [ zeros(1,6); moreg(2:end,:)-moreg(1:end-1,:) ];
                fd =sum( abs([delta_moreg(:,1:3) delta_moreg(:,4:6)*2*pi*radius/360]) ,2);
                goodPoints = ones(length(fd), 1);
                goodPoints(fd>MoCen_FDthres) = 0;
                goodPoints = logical(goodPoints);
                
                % censor out motion timepoints               
                % go to the direcotry where we want the processed functional
                % data to be saved
                cd(ProcData_save_dir);
                
                % the input functional data name
                if isempty(FuncFilt_type)
                    funcName_input = [ProcFunc_afterGLM_save_dir, subj_str, '_', ses_str, ...
                        '_proc_PercChng+tlrc'];
                else
                    funcName_input = [subj_str, '_', ses_str, ...
                        '_proc_PercChng_FuncFilt+tlrc'];
                end
                
                % the output functional data name
                if isempty(FuncFilt_type)
                    funcName_output = [subj_str, '_', ses_str, ...
                        '_proc_PercChng_MoCen+tlrc'];
                else
                    funcName_output = [subj_str, '_', ses_str, ...
                        '_proc_PercChng_FuncFilt_MoCen+tlrc'];    
                end
                
                % load the input functional data
                funcData_input = BrikLoad(funcName_input);
                sz = size(funcData_input);
                
                % remove the first a few TRs from fd and goodpoints
                fd = fd(end-sz(4)+1:end);
                goodPoints = goodPoints(end-sz(4)+1:end);
                
                % applying the motion censoring
                funcData_output = funcData_input(:, :, :, goodPoints);

                % save the motion censored functional data
                Mat2AFNI(double(funcData_output), funcName_output,  ...
                    funcName_input, 3);
                % add the TR info back
                cmd = ['3drefit -TR ', num2str(TR), ' ' funcName_output];
                system(cmd)
                
                fd_all(isub, iday, iscan, :) = fd;
                goodPoints_all(isub, iday, iscan, :) = goodPoints;
                
            end

        end

    end
    save([ProcData_save_dir, 'fd_and_goodPoints'], 'fd_all', 'goodPoints_all');    
end