%% The minimal preprocessing pipeline.
% Description
% For the functional data, this pipeline does:
% 1. susceptibility distortion correction (topup correction) + motion
% correction;
% 2. time shift correction (slice timing correction);
% 3. spatial normliaztion to standard space.
% For the anatomical data, this pipeline does:
% 1. skull striping;
% 2. aligning to the functional data;
% 3. warping to standard space.

% Dependencies: 
% 1. FSL (version 6.0)
% 2. AFNI (Version AFNI_21.2.04 'Nerva')

% Authors:  Yixiang Mao (y1mao@ucsd.ued)

close all; clearvars; clc;
%% Define paths
% add the paths to other functions in the repo
repo_dir = ['/home/ymao/git_repo/rsfmri_tavns/'];
addpath(genpath(repo_dir))

% raw data root directory
raw_data_root_dir = '/data/students_group/data/vorso_BIDS_skull_stripped/';

% processed data save root directory
ProcData_save_root_dir = ['/data/students_group/data/vorso_reProc_test/'];
if ~exist(ProcData_save_root_dir, 'dir')
    mkdir(ProcData_save_root_dir)
end

%% the indices of subjects to be processed
% note, subject 18 and 19 didn't complete the whole experiment, hence are
% not processed
subj_idx_include = [1:17, 20:27];

%% scan parameters
TR = 0.96; % Repetition time: 0.96 seconds
nvol = 363; % number of volumes before tcat
%% Motion correction + topup correction
% The script named run_topup_hcpfmri is used to do susceptibility
% distortion correction (topup correction) + motion correction.
% In that script, AFNI command 3dvolreg is used for volume
% registration and FSL commands topup and applytopup are used
% for topup correction.  
run_topup_script_name = [repo_dir, '/dependencies/run_topup_hcpfmri'];

% set the suffix of the input and output data
inputFunc_suffix = []; % suffix of the input functional data of this step;
                       % []: the raw data
outputFunc_suffix = [inputFunc_suffix, '_reg_topup'];
                    % suffix of the output functional data of this step;
for isub  = subj_idx_include
    for iday = 1:2
        
        if iday == 1
            treat_or_cont = 'treat';
        else
            treat_or_cont = 'cont';
        end
        
        subj_str = ['sub-', treat_or_cont, num2str(isub, '%02.f')];
        for iscan = 1:2
            ses_str = ['ses-', num2str(iscan, '%02.f')];
            % ses-01: pre session
            % ses-02: post session
            
            % the folder where the raw data of this session locate
            rawData_ses_folder = [raw_data_root_dir, subj_str, '/', ses_str, '/'];
            
            % the raw functional data name
            funcName_raw = [subj_str, '_', ses_str, ...
                '_task-rest_bold.nii.gz'];
            
            % the raw fieldmap (fmap) data name
            % A-P
            fmapName_AP_raw = [subj_str, '_', ses_str, ...
                '_dir-AP_epi.nii.gz'];
            % P-A
            fmapName_PA_raw = [subj_str, '_', ses_str, ...
                '_dir-PA_epi.nii.gz'];
            
            % create the folder to save processed the functional data
            ProcFunc_save_dir = [ProcData_save_root_dir, subj_str,...
                '/', ses_str, '/func/'];
            if ~exist(ProcFunc_save_dir, 'dir')
                mkdir(ProcFunc_save_dir)
            end
            
            % go to the above dir
            cd(ProcFunc_save_dir);
            
            % copy the raw func data
            cmd = ['cp ', rawData_ses_folder, 'func/', funcName_raw, ' ./'];
            system(cmd)
            cmd = ['cp ', rawData_ses_folder, 'fmap/', fmapName_AP_raw, ' ./'];
            system(cmd)
            cmd = ['cp ', rawData_ses_folder, 'fmap/', fmapName_PA_raw, ' ./'];
            system(cmd)
            
            cmd = [run_topup_script_name,...
                ' -d1 ', fmapName_AP_raw(1:end-7), ...
                ' -d2 ', fmapName_PA_raw(1:end-7), ...
                ' -i ', funcName_raw(1:end-7), inputFunc_suffix, ...
                ' -o ', funcName_raw(1:end-7), outputFunc_suffix, ...
                ' -nocleanup'];
            system(cmd)
            
            % 3dcopy the functional data from .nii.gz to BRIK file
            % the raw data
            cmd = ['3dcopy ', funcName_raw, ' ', funcName_raw(1:end-7), '+orig'];
            system(cmd)
            % the data after correction
            cmd = ['3dcopy ', funcName_raw(1:end-7), outputFunc_suffix, '.nii.gz ',...
                funcName_raw(1:end-7), outputFunc_suffix, '+orig'];
            system(cmd)

        end
    end
end
%% Time shift correction
% the AFNI command 3dTshift is used for time shift correction.

% settings for the time shift correction.
SliceTimings = ['0.00 0.48 0.08 0.56 0.16 0.64 0.24 0.72 0.32 0.80 0.40 0.88', ...
    ' 0.00 0.48 0.08 0.56 0.16 0.64 0.24 0.72 0.32 0.80 0.40 0.88', ...
    ' 0.00 0.48 0.08 0.56 0.16 0.64 0.24 0.72 0.32 0.80 0.40 0.88', ...
    ' 0.00 0.48 0.08 0.56 0.16 0.64 0.24 0.72 0.32 0.80 0.40 0.88', ...
    ' 0.00 0.48 0.08 0.56 0.16 0.64 0.24 0.72 0.32 0.80 0.40 0.88', ...
    ' 0.00 0.48 0.08 0.56 0.16 0.64 0.24 0.72 0.32 0.80 0.40 0.88'];
% The slice timing value for each slice. 3drefit is used to insert the
% slice timings.

inputFunc_suffix = ['_reg_topup']; % suffix of the input functional data of this step;
                       % []: the raw data
outputFunc_suffix = [inputFunc_suffix, '_ts'];
                    % suffix of the output functional data of this step;
inputAnat_suffix = []; % suffix of the input anatomical data of this step;
                       % []: the raw data
outputAnat_suffix = [inputAnat_suffix, ''];
                    % suffix of the output anatomical data of this step;                    
% in this step, we don't need the anatomical data.                     
for isub  = subj_idx_include
    for iday = 1:2
        
        if iday == 1
            treat_or_cont = 'treat';
        else
            treat_or_cont = 'cont';
        end
        
        subj_str = ['sub-', treat_or_cont, num2str(isub, '%02.f')];
        for iscan = 1:2
            ses_str = ['ses-', num2str(iscan, '%02.f')];
            % ses-01: pre session
            % ses-02: post session
            
            % the folder where we save the processed functional data
            ProcFunc_save_dir = [ProcData_save_root_dir, subj_str,...
                '/', ses_str, '/func/'];
            
            % go to the above dir
            cd(ProcFunc_save_dir);
            
            % the input functional data name
            funcName_input = [subj_str, '_', ses_str, ...
                '_task-rest_bold', inputFunc_suffix, '+orig'];
            
            
            % the output functional data name
            funcName_output = [subj_str, '_', ses_str, ...
                '_task-rest_bold', outputFunc_suffix, '+orig'];
            
            % insert the slice timings to the input funcitional data
            cmd = ['3drefit -Tslices ' SliceTimings, ...
                ' ', funcName_input];
            system(cmd)
            
            % do the time shift correction
            cmd = ['3dTshift -prefix ' funcName_output, ...
                ' ', funcName_input];
            system(cmd)     
        end
    end
end
%% Skull strip the anatomical image
% Three commands are used to do the skull striping
% 1. fsl_anat
% 2. bet
% 3. 3dUnifize + 3dSkullStrip
% For subject 26, the command bet is used.
% For subject 14, treatment session, 3dUnifize + 3dSkullStrip are used.
% For other runs, fsl_anat is used.
% It is becuase fsl_anat didn't work well for subject 26 and subject 14,
% treatment session.

% inputAnat_suffix = []; % suffix of the input anatomical data of this step;
%                        % []: the raw data
% outputAnat_suffix = [inputAnat_suffix, '_ns'];
%                     % suffix of the output anatomical data of this step;     
% for isub  = subj_idx_include
%     for iday = 1:2
%         
%         if iday == 1
%             treat_or_cont = 'treat';
%         else
%             treat_or_cont = 'cont';
%         end
%         
%         subj_str = ['sub-', treat_or_cont, num2str(isub, '%02.f')];
%         for iscan = 1:2
%             ses_str = ['ses-', num2str(iscan, '%02.f')];
%             % ses-01: pre session
%             % ses-02: post session
%             
%             % the folder where the data of this session locate
%             rawData_ses_folder = [raw_data_root_dir, subj_str, '/', ses_str, '/'];
%             
%             % the name of the input anatomical data
%             anatName_input = [subj_str, '_', ses_str, ...
%                 '_T1w.nii.gz'];
%             % the name of the output anatomical data
%             anatName_output = [subj_str, '_', ses_str, ...
%                 '_T1w', outputAnat_suffix, '+orig'];
%             
%             % create the folder to save processed the anatomical data
%             ProcAnat_save_dir = [ProcData_save_root_dir, subj_str,...
%                 '/', ses_str, '/anat/'];
%             if ~exist(ProcAnat_save_dir, 'dir')
%                 mkdir(ProcAnat_save_dir)
%             end
%             
%             % go to the above dir
%             cd(ProcAnat_save_dir);
%             
%             % copy the raw anat data
%             cmd = ['cp ', rawData_ses_folder, 'anat/', anatName_input, ' ./'];
%             system(cmd)
%             
%             % start skull striping
%             if isub==14 && iday==1 % subject 14, treatment session
%                 % 3dUnifize 
%                 cmd = ['3dUnifize -input ', anatName_input, ' -prefix tmp_unif+orig'];
%                 system(cmd)
%                 % 3dSkullStrip
%                 cmd = ['3dSkullStrip -orig_vol -prefix ', anatName_output, ...
%                     ' -input tmp_unif+orig'];
%                 system(cmd)
%                 % rm the unifized anatomical data
%                 cmd = ['rm tmp_unif+orig*'];
%                 system(cmd)
%             elseif isub==26 % subject 26
%                 % bet
%                 cmd = ['bet ', anatName_input, ' output_tmp -f 0.4 -g 0.2 -R'];
%                 system(cmd)
%                 % 3dcopy 
%                 cmd = ['3dcopy output_tmp.nii.gz ', anatName_output];
%                 system(cmd)
%                 % rm output_tmp.nii.gz
%                 cmd = ['rm output_tmp.nii.gz'];
%                 system(cmd)
%             else % all other runs
%                 cmd = ['fsl_anat -i ', anatName_input];
%                 system(cmd)
%                 % 3dcopy out the skull stripped anatomical data
%                 cmd = ['3dcopy ', ...
%                     anatName_input(1:end-7), '.anat/T1_biascorr_brain.nii.gz', ...
%                     ' ', anatName_output];
%                 system(cmd)
%                 % rm intermediate files
%                 cmd = ['rm -rf ', ...
%                     anatName_input(1:end-7), '.anat'];
%                 system(cmd)
%             end
%             
%         end 
%     end
% end
%% Copy the skull-stripped anatomical data
% Since the skull-stripped anatomical data were uploaded, we skip the
% skull-stripping and just copy the the skull-stripped anatomical data.

inputAnat_suffix = []; % suffix of the input anatomical data of this step;
                       % []: the raw data
outputAnat_suffix = [inputAnat_suffix, '_ns'];
                    % suffix of the output anatomical data of this step;     
for isub  = subj_idx_include
    for iday = 1:2
        
        if iday == 1
            treat_or_cont = 'treat';
        else
            treat_or_cont = 'cont';
        end
        
        subj_str = ['sub-', treat_or_cont, num2str(isub, '%02.f')];
        for iscan = 1:2
            ses_str = ['ses-', num2str(iscan, '%02.f')];
            % ses-01: pre session
            % ses-02: post session
            
            % the folder where the data of this session locate
            rawData_ses_folder = [raw_data_root_dir, subj_str, '/', ses_str, '/'];
            
            % the name of the input anatomical data
            anatName_input = [subj_str, '_', ses_str, ...
                '_T1w.nii.gz'];
            % the name of the output anatomical data
            anatName_output = [subj_str, '_', ses_str, ...
                '_T1w', outputAnat_suffix, '+orig'];
            
            % create the folder to save processed the anatomical data
            ProcAnat_save_dir = [ProcData_save_root_dir, subj_str,...
                '/', ses_str, '/anat/'];
            if ~exist(ProcAnat_save_dir, 'dir')
                mkdir(ProcAnat_save_dir)
            end
            
            % go to the above dir
            cd(ProcAnat_save_dir);
            
            % copy the skull-stripped anat data
            cmd = ['3dcopy ', rawData_ses_folder, 'anat/', anatName_input, ...
                ' ./', anatName_output];
            system(cmd)
            
            
        end 
    end
end
%% Align the anatomical data to the functional data
% Aligning the anat image to the EPI data is performed by the AFNI command
% align_epi_anat.py.

% parameters
epiBaseVolume = '0';
alignDirection = '-anat2epi'; 
suffix = '_al';
warpType =  'shift_rotate'; 
                            % shift_only         *OR* sho =  3 parameters
                            % shift_rotate       *OR* shr =  6 parameters
                            % shift_rotate_scale *OR* srs =  9 parameters
                            % affine_general     *OR* aff = 12 parameters
costFunc = 'lpc+ZZ';	% Defines the 'cost' function that defines the matching
                        % between the source and the base; 'ccc' is one of
                        % ls   *OR*  leastsq         = Least Squares [Pearson Correlation]
                        % mi   *OR*  mutualinfo      = Mutual Information [H(b)+H(s)-H(b,s)]
                        % crM  *OR*  corratio_mul    = Correlation Ratio (Symmetrized*)
                        % nmi  *OR*  norm_mutualinfo = Normalized MI [H(b,s)/(H(b)+H(s))]
                        % hel  *OR*  hellinger       = Hellinger metric
                        % crA  *OR*  corratio_add    = Correlation Ratio (Symmetrized+)
                        % crU  *OR*  corratio_uns    = Correlation Ratio (Unsym)
                        % lpc  *OR*  localPcorSigned = Local Pearson Correlation Signed
                        % lpa  *OR*  localPcorAbs    = Local Pearson Correlation Abs
                        % lpc+ *OR*  localPcor+Others= Local Pearson Signed + Others
                        % lpa+ *OR*  localPcorAbs+Others= Local Pearson Abs + Others
                        % According to the documentation of 3dAllineate
                        % * For many purposes, lpc+ZZ and lpa+ZZ are the most robust
                        % cost functionals, but usually the slowest to evaluate.

inputFunc_suffix = ['_reg_topup_ts']; 
                    % suffix of the input functional data of this step;
                    % []: the raw data
outputFunc_suffix = [inputFunc_suffix, ''];
                    % suffix of the output functional data of this step;
inputAnat_suffix = ['_ns']; 
                    % suffix of the input anatomical data of this step;
                    % []: the raw data
outputAnat_suffix = [inputAnat_suffix, suffix];
                    % suffix of the output anatomical data of this step;

                   
for isub  = subj_idx_include
    for iday = 1:2
        
        if iday == 1
            treat_or_cont = 'treat';
        else
            treat_or_cont = 'cont';
        end
        
        subj_str = ['sub-', treat_or_cont, num2str(isub, '%02.f')];
        for iscan = 1:2
            ses_str = ['ses-', num2str(iscan, '%02.f')];
            % ses-01: pre session
            % ses-02: post session
            
            % the folder where we save processed the anatomical data
            ProcAnat_save_dir = [ProcData_save_root_dir, subj_str,...
                '/', ses_str, '/anat/'];

            % the folder where we save processed the functional data
            ProcFunc_save_dir = [ProcData_save_root_dir, subj_str,...
                '/', ses_str, '/func/'];

            % go to the anat save dir
            cd(ProcAnat_save_dir);
                        
            % the input functional data name
            funcName_input = [subj_str, '_', ses_str, ...
                '_task-rest_bold', inputFunc_suffix, '+orig'];
            % the input anatomical data name
            anatName_input = [subj_str, '_', ses_str, ...
                '_T1w', inputAnat_suffix, '+orig'];
            
            cmd = ['align_epi_anat.py -anat ', anatName_input,...
                ' -epi ', ProcFunc_save_dir, funcName_input, ...
                ' -epi_base ', epiBaseVolume, ' ', alignDirection, ...
                ' -suffix ', suffix, ' -anat_has_skull no', ...
                ' -epi_strip 3dAutomask', ...
                ' -volreg off -tshift off', ...
                ' -Allineate_opts -warp ', warpType, ' -cost ', costFunc];
            system(cmd)
        end
    end
end
                        
%% Warp the functional data to the standard space
% Spatial normalization (warpping) of the functional data to standard space
% is performed by the AFNI command auto_warp.py and 3dNwarpApply.
% Specifically, the anatomical data is first warpped to standard space and
% warp files are generated. Then, using those warp files, the functional
% data is warpped to standard sp

% parameters
template_name = [repo_dir, 'masks_and_templates/mni_icbm152_t1_tal_nlin_asym_09a_brain+tlrc'];
template_resolution = '2'; % the resolution of the standard template in mm


inputFunc_suffix = ['_reg_topup_ts']; 
                    % suffix of the input functional data of this step;
                    % []: the raw data
outputFunc_suffix = [inputFunc_suffix, '_aw'];
                    % suffix of the output functional data of this step;
inputAnat_suffix = ['_ns_al']; 
                    % suffix of the input anatomical data of this step;
                    % []: the raw data
outputAnat_suffix = [inputAnat_suffix, '_aw'];
                    % suffix of the output anatomical data of this step;
                    
for isub  = subj_idx_include
    for iday = 1:2
        
        if iday == 1
            treat_or_cont = 'treat';
        else
            treat_or_cont = 'cont';
        end
        
        subj_str = ['sub-', treat_or_cont, num2str(isub, '%02.f')];
        for iscan = 1:2
            ses_str = ['ses-', num2str(iscan, '%02.f')];
            % ses-01: pre session
            % ses-02: post session
            
            % the folder where we save processed the anatomical data
            ProcAnat_save_dir = [ProcData_save_root_dir, subj_str,...
                '/', ses_str, '/anat/'];

            % the folder where we save processed the functional data
            ProcFunc_save_dir = [ProcData_save_root_dir, subj_str,...
                '/', ses_str, '/func/'];

                        
            % the input functional data name
            funcName_input = [subj_str, '_', ses_str, ...
                '_task-rest_bold', inputFunc_suffix, '+orig'];
            % the input anatomical data name
            anatName_input = [subj_str, '_', ses_str, ...
                '_T1w', inputAnat_suffix, '+orig'];
            
            % the output functional data name
            funcName_output = [subj_str, '_', ses_str, ...
                '_task-rest_bold', outputFunc_suffix, '+tlrc'];
            % the output anatomical data name
            anatName_output = [subj_str, '_', ses_str, ...
                '_T1w', outputAnat_suffix, '+tlrc'];
            
            % go to the anat save dir
            cd(ProcAnat_save_dir);
            if ~isfile([anatName_output, '.BRIK']) % in case that we didn't transform the anatomical image before
                cmd = ['auto_warp.py -skull_strip_input no -base ', template_name,' -input ', anatName_input];
                system(cmd)

                % 3dcopy the warped anatomical image
                cmd = ['3dcopy awpy/',anatName_input(1:end-5),'.aw.nii ', anatName_output];
                system(cmd)

                % move the warp files
                cmd = ['mv awpy/anat.un.aff.qw_WARP.nii ./'];
                system(cmd)
                cmd = ['mv awpy/anat.un.aff.Xat.1D ./'];
                system(cmd)
            end
            % go to the functional save dir
            cd(ProcFunc_save_dir);
            if ~isfile([funcName_output, '.BRIK']) 
                cmd = ['3dNwarpApply -master ', ProcAnat_save_dir, anatName_output, ...
                    ' -source ', funcName_input, ...
                    ' -nwarp ', ProcAnat_save_dir, 'anat.un.aff.qw_WARP.nii ', ...
                    ProcAnat_save_dir, 'anat.un.aff.Xat.1D', ...
                    ' -dxyz ', template_resolution, ...
                    ' -prefix ', funcName_output];
                system(cmd)
            end
        end
    end
end
