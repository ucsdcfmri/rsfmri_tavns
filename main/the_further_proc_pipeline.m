%% the further preprocessing pipeline
% Description
% This pipeline does:
% 1. Spatial smoothing.
% 2. Removing the first a few TRs.
% 3. Regressing out nuisance regressors using GLM.
% 4. Converting the functional data to percent change.

% Dependencies: 
% 1. FSL (version 6.0)

% Authors:  Yixiang Mao (y1mao@ucsd.ued)

close all; clearvars; clc;
%% Define paths
% add the paths to other functions in the repo
repo_dir = ['/home/ymao/git_repo/rsfmri_tavns/'];
addpath(genpath(repo_dir))

% raw data root directory
raw_data_root_dir = '/data/students_group/data/vorso_BIDS_skull_stripped/';

% processed data save root directory
ProcData_save_root_dir = ['/data/students_group/data/vorso_reProc_test/'];
if ~exist(ProcData_save_root_dir, 'dir')
    mkdir(ProcData_save_root_dir)
end

%% the indices of subjects to be processed
% note, subject 18 and 19 didn't complete the whole experiment, hence are
% not processed
subj_idx_include = [1:17, 20:27];

%% scan parameters
TR = 0.96; % Repetition time: 0.96 seconds
nvol = 363; % number of volumes before tcat

%% Read the brain masks
brain_mask_dir = [repo_dir, 'masks_and_templates/', ...
    'mni_icbm152_t1_tal_nlin_asym_09a_brain_rs_automask2+tlrc'];
brainMask3d = BrikLoad(brain_mask_dir);
brainMask3d = logical(brainMask3d);
sz = size(brainMask3d);
brainMask = reshape(brainMask3d, sz(1)*sz(2)*sz(3),1 );
brainMask = logical(brainMask);

%% options
smooth_fwhm = '4'; % the FWHM (mm) use in functional data smoothing;
                   % 0: don't smooth
smooth_toFWHM = 0; % 0: use 3dBlurInMask -FWHM [smooth_fwhm].
                   % 1: use 3dBlurToFWHM -FWHM [smooth_fwhm].
tcat = 0; % remove the first [tcat] TRs from the functional data to reach steady state
           % <=0: don't remove any TRs
           
% GLM options
GLM.doGLM = 1; % Do you want to do the GLM?
               % 0: Don't. This is useful when we only want to take a
               %            look at the regressors.
               % 1: Do.
               

% settings of the polynomial (Poly) regressors:
GLM.Poly = 1; % Include polynomials in the GLM or not.
              % 0: Don't.
              % 1: Do.
GLM.Poly_MaxOrder = 2; % The maximum order of the polynomials in the GLM. 
                       % E.g. when GLM.Poly_MaxOrder = 2, there will be the DC
                       % (0-th order), the linear trend (1-st order) and
                       % the quadratic trend (2-nd order).
% Note, in this pipeline, we use the function processPreg_RS.m (in repo:
% rstoolbox) to regress out the regressors using the GLM. That function
% works in the following way:
% 1. Remove the mean from the functional data.
% 2. Regress out the regressors using the GLM.
% 3. Add back the mean to the functional data.
% As a result, having DC in the GLM will not remove the mean from the
% functional data. The purpose of having DC in the GLM is to remove the
% mean from the other regressors when we do the regression (verified in
% GLM_check.m, argument 1). 
GLM.Poly_outsideGLM = 1; % Regress out the polynomial regressors outside 
                         % the GLM or not.
                         % 0: Don't
                         % 1: Do.
                         % When it sets to 1, the ploynomial regressors
                         % will be first regressed out from the functional
                         % data (using processPreg_RS.m, the mean of
                         % functional data will not be removed) and other
                         % regressors (not using processPreg_RS.m, so the
                         % mean of the other regressors will be removed).
                         % Note, this option will not change the output
                         % functional data from the GLM （verified in
                         % GLM_check.m, argument2）. When this option 
                         % is 1, the functional data before GLM in the QA
                         % would have the polynomial regressors regressed
                         % out, so we can better evaluate the effect of
                         % regressing out the motion, WM/CSF and RVHRCOR
                         % regressors. 
                         

% settings of the motion regressors:
GLM.motion = 1; % Include the motion parameters in the GLM or not.
                % 0: Don't.
                % 1: Do.
GLM.motion_type = 1; % Options for motion resgressors:
                     % 1: 6-parameters + 1st derivative (a total of 12 regressors)
                     % 2: 6-parameters only (a total of 6 regressors)
                     % 3: 6-parameters + 1st derivative + squared terms (a total of 24 regressors)
                     % (default = 1)
GLM.motion_filter = 0; % Apply filtering to the motion parameters or not. 
                       % 0: Don't.
                       % 1: Do.
                       % (default = 0)
                       % To better make a decision whether filter the
                       % motion parameters or not, see reference:  
                       % Fair DA, et al., Correction of respiratory artifacts in MRI head motion estimates. Neuroimage. 2020 Mar 1;208:116400.
                       % Note, if you decide to filter the motion
                       % parameters, please take care of the following
                       % GLM options.
GLM.motion_filter_method = 'lp'; % The filtering method. 
                                 % 1. 'lp': the lowpass filter, implemented by
                                 %  the butter function with two more
                                 %  parameters: i) the order of the filter
                                 %  N and ii) the stopband Wn.
                                 % 2. 'notch': the notch filter,
                                 %  implemented by the iirnotch function
                                 %  with two more parameters: i) the central
                                 %  cutoff frequency Wo and ii) the
                                 %  bandwidth BW.
GLM.motion_filter_paras = [10, 0.1]; % The parameters used in the filtering method.
                                     % 1. When GLM.motion_filter_method = 'lp',
                                     %   GLM.motion_filter_paras = [N, Wn] 
                                     % 2. When GLM.motion_filter_method = 'notch',
                                     %   GLM.motion_filter_paras = [Wo, BW]
% An example of the GLM options when we want to apply a 10-th order lowpass
% filter with stopband of 0.1 Hz to the motion parameters:
% GLM.motion_filter_method = 'lp';
% GLM.motion_filter_paras = [10, 0.1];
% Another example of the GLM options when we want to apply a notch filter
% with the central cutoff frequency of 0.3 Hz and the bandwidth of 0.4 Hz
% to the motion parameters: 
% GLM.motion_filter_method = 'notch';
% GLM.motion_filter_paras = [0.3, 0.4];



% settings of the WMCSF regressors:
GLM.WMCSF = 1; % Include WM and CSF regressors in the GLM or not.
               % 0: Don't.
               % 1: Do.
GLM.WMCSF_type = 1; % How to compute the WMCSF regressors
                    % 0: mean
                    % 1: aCompCor
                    % (default = 0)
GLM.WMCSF_numPC = 5; % How many PCs to be used as regressors in aCompCor 
                      % method.
                      % (default = 5)
GLM.WMCSF_combine_WM_CSF = 1; % Do you want to combine the WM and CSF masks
                              % by using the 'and' operation? 
                              % 0: Don't.
                              % 1: Do.
                              % (default = 0)
                              % With GLM.WMCSF_type = 0, set this option to
                              % 0 if you want one mean WM signal from the
                              % WM mask and one mean CSF signal from the
                              % CSF mask, and set this option to 1 if you
                              % want only one mean signal from the combined
                              % WM and CSF mask.
                              % With GLM.WMCSF_type = 1, set this option to
                              % 0 if you want [GLM.WMCSF_numPC] PCs from
                              % the WM mask and [GLM.WMCSF_numPC] PCs from
                              % the CSF mask, and set this option to 1 if
                              % you want [GLM.WMCSF_numPC] PCs from the
                              % combined WM and CSF mask. 
GLM.WMCSF_derivatives = 0; % Include the 1-st derivatives or not.
                           % 0: Don't.
                           % 1: Do.
                           % (default = 1)
                           % When GLM.WMCSF_type = 0, the 1-st derivatives
                           % of the mean signal(s) will be added as
                           % regressors. 
                           % When GLM.WMCSF_type = 1, the 1-st derivatives
                           % of the aCompCor regressors will be added
                           % as regressors.                         
GLM.WMCSF_pv_thres = [0.99, 0.99]; % The partial volume map thresholds to 
                                   % determine the WM and CSF voxels
                                   % repectively. 
                                   % (default = [0.99, 0.99])
                                   % E.g., if we set
                                   % GLM.WMCSF_pv_thres = [0.99, 0.99],
                                   % then CSF voxels were determined by
                                   % first thresholding the CSF partial
                                   % volume maps at 0.99. Similarly, the WM
                                   % voxels were determined by first
                                   % thresholding the WM partial volume
                                   % maps at 0.99.  
GLM.WMCSF_erosionNum = [2, 0];	% Specify the number of voxels for erosion, in 
                                % WM mask and CSF mask, respectively.
                                % Set this option to 0 if you don't want to
                                % do the erosion.
                                % (default = [2, 0])
                                % E.g., set this option to [2, 0]. The
                                % thresholded WM mask will be
                                % morphologicall eroded by 2 voxels. And
                                % the thresholded CSF mask will not be
                                % eroded.
GLM.WMCSF_ClustMode = [2, 2]; % The partial volume map after thresholding 
                              % is then clusterized to minimize multiple
                              % tissue partial voluming (Behzadi Y, et al.,
                              % NIMG, 2007). This option is to control the
                              % cluster mode when clustering the WM and CSF
                              % masks respectively: 
                              % 1: touching faces (NN1) 
                              % 2: touching faces/edges (NN2)
                              % 3: touching faces/edges/corners (NN3)
                              % (default = [2, 2])
                              % E.g., set this option to [2, 3], the two
                              % voxels in the WM mask touching faces or
                              % edges are regarded as being in one cluster,
                              % and the two voxels in the CSF mask touching
                              % faces, edges or corners are regarded as
                              % being in one cluster.
GLM.WMCSF_ClustSize = [3, 3]; % The partial volume map after thresholding 
                              % is then clusterized to minimize multiple
                              % tissue partial voluming (Behzadi Y, et al.,
                              % NIMG, 2007). This option is to control the
                              % cluster size when clustering the WM and CSF
                              % masks respectively.
                              % Set this option to 1 if you don't want to
                              % do the clustering.
                              % (default = [3, 3])
                              % E.g., set this option to [10, 1]. The
                              % clusters with sizes smaller than 10 voxels
                              % in the thresholded and eroded WM mask will
                              % be excluded. And the CSF mask will not be
                              % clustered.
GLM.WMCSF_space = 'native'; % The WM and CSF regressors are calculated by 
                            % the data in which space.
                            % 1. 'native': the functional and anatomical
                            %   data in the native space. Specifically, the 
                            %   anatomical data is aligned to the functional data.  
                            % For now (05/24/2021), the WMCSF regressors
                            % can only be calculated in the native space.
GLM.WMCSF_FuncDetrend = 2; % The maximum order of the polynomials 
                           % (not include DC) you want to regress out from
                           % the functional data before normalization and 
                           % calculating the WM and CSF regressors.
                           % 0:   Don't detrend.
                           % >=1: Detrend. 1 to [FuncDetrend] order of
                           %        polynomials will be regressed out from
                           %        the functional data.
                           % (defualt = 2)
GLM.WMCSF_FuncNorm = 2;  % How to normalize the functional data after 
                         % detrending and before calculating the WM and CSF
                         % regressors 
                         % 0:  don't normalize
                         % 1:  convert to percent change
                         % 2:  z-normalization (zero mean and unit
                         %   variance). Note, this option is used
                         %   in the aCompCor paper (Behzadi Y, et
                         %   al., NIMG, 2007).                                 
                         % 3:  demean
                         % (defualt = 2)
% Note, the functional data are first detrended then normalized.
                         
% settings of the RVHRCOR regressors:
GLM.RVHRCOR = 1; % Include RVHRCOR regressors in the GLM or not. 
                 % 0: Don't.
                 % 1: Do.
GLM.RVHRCOR_RV_WinLen = 6; % The window length used to calculate RV (in sec).
                           % (default = 6)
                           % In Catie Chang's cardiac response function paper
                           % (Chang, et al., NIMG, 2009), a 6-sec long window 
                           % was used to calculate the RV.
GLM.RVHRCOR_HR_WinLen = 6; % The window length used to calculate HR (in sec).
                           % (default = 6)
                           % In Catie Chang's cardiac response function paper
                           % (Chang, et al., NIMG, 2009), a 6-sec long window 
                           % was used to calculate the HR.
GLM.RVHRCOR_CardSrate = 100; % The sampling rate of the raw caridac signal (Hz).
                             % (default = 100)
                             % For the GE system, the sampling rate of the
                             % raw caridac signal is 100 Hz. For some old
                             % datasets, e.g. the caffeine dataset acquired
                             % by the cfmri, the sampling rate is 40 Hz.
GLM.RVHRCOR_RespSrate = 25; % The sampling rate of the respiration signal (Hz).
                            % (default = 25)
                            % For the GE system, the sampling rate of the
                            % raw respiration signal is 25 Hz. For some old
                            % datasets, e.g. the caffeine dataset acquired
                            % by the cfmri, the sampling rate is 40 Hz.
% * It is alway good to double check on the sampling rate of the raw
% physiological signals! Here is a way to do the sanity check:
% num_tps_in_raw_physio ~= (T+TR*num_vol_in_func)*sample_rate
% where
% num_tps_in_raw_physio - The total number of timepoints in the raw
%                           physiologcial signal
% T                     - The time duration (sec) of the physiological
%                           signal recorded before the start of the
%                           functional data acquisition. For the GE system,
%                           typically, T is around 30 sec. For the Nesos
%                           dataset, T varies from 30.01 sec to 30.1 sec.
% TR                    - The repetition time of the functional data.
% num_vol_in_func       - The total number of volumes in the functional
%                           data (including the 2*multiband_factor TRs in
%                           case of the multiband data acquisition).  
% sample_rate           - The sampling rate of the raw physiological
%                           signal.


% settings of the GS regressor:
GLM.GS = 0; % Include the global signal regressor in the GLM or not.
            % 0： Don't.
            % 1 = Do.
GLM.GS_FuncNorm = 1;  % how to normalize the functional data before 
                      % calculating the GS regressor
                      % 0: don't normalize 
                      % 1: convert to percent change
                      % 2: z-normalization (zero mean and unit variance).                     
                      % (default = 1)

% Form a string that contains key settings about the GLM
GLM_str = [];
if GLM.Poly 
    GLM_str = [GLM_str, '_', 'Poly', num2str(GLM.Poly_MaxOrder)];
else
    GLM_str = [GLM_str, '_Poly0'];
end
if GLM.motion
    GLM_str = [GLM_str, '_Mo1'];
        if GLM.motion_filter
            GLM_str = [GLM_str, GLM.motion_filter_method];
        end
else
    GLM_str = [GLM_str, '_Mo0'];
end
if GLM.WMCSF
    if GLM.WMCSF_type % aCompCor
        GLM_str = [GLM_str, '_WMCSF', num2str(GLM.WMCSF_numPC), 'aCompCor']; 
    else % mean
        GLM_str = [GLM_str, '_WMCSF1mean'];
    end
else 
    GLM_str = [GLM_str, '_WMCSF0'];
end
if GLM.RVHRCOR    
    GLM_str = [GLM_str, '_RVHRCOR1'];
else 
    GLM_str = [GLM_str, '_RVHRCOR0'];
end
if GLM.GS   
    GLM_str = [GLM_str, '_GS1'];
else 
    GLM_str = [GLM_str, '_GS0'];
end
GLM_str

% plot settings
if_plot_regs = 0; % Do you want plots showing the regressors? 1 = do, 0 = don't.
if_plot_QA = 0; % Do you want a sanity plot showing the FD, the WM and CSF 
                % regressors, the physiological signals and corresponding
                % regressors, the GS of the functional data before and
                % after the GLM and two heat maps showing the voxel-wise
                % functional data before and after GLM?
                % 1 = do, 0 = don't.
if if_plot_QA % if you want that sanity plot
    QAplot_Opts.FD_thresh = 0.2; % the FD values larger than QAplot_Opts.FD_thresh 
                                 % will be regarded as bad timepoints in
                                 % the QA plot.
                                 
end
line_width = 1.5;
label_size = 15;


%% smooth the functional data
% One of the AFNI commands 3dBlurToFWHM and 3dBlurInMask is used for
% spatial smoothing. 

if ~strcmp(smooth_fwhm, '0') % in case we want to apply extra smooth to the functional data
    
    inputFunc_suffix = ['_reg_topup_ts_aw'];
    % suffix of the input functional data of this step;
    % []: the raw data
    if smooth_toFWHM == 1 % 3dBlurToFWHM
        outputFunc_suffix = [inputFunc_suffix, '_to', smooth_fwhm, 'mm'];
        % suffix of the output functional data of this step;
    elseif smooth_toFWHM == 0 % 3dBlurInMask
        outputFunc_suffix = [inputFunc_suffix, '_', smooth_fwhm, 'mm'];
        % suffix of the output functional data of this step;
    end
    
    for isub  = subj_idx_include
        for iday = 1:2
            
            if iday == 1
                treat_or_cont = 'treat';
            else
                treat_or_cont = 'cont';
            end
            
            subj_str = ['sub-', treat_or_cont, num2str(isub, '%02.f')];
            for iscan = 1:2
                ses_str = ['ses-', num2str(iscan, '%02.f')];
                % ses-01: pre session
                % ses-02: post session
                
                % the folder where we save processed the functional data
                ProcFunc_save_dir = [ProcData_save_root_dir, subj_str,...
                    '/', ses_str, '/func/'];
                
                % go to the anat save dir
                cd(ProcFunc_save_dir);
                
                % the input functional data name
                funcName_input = [subj_str, '_', ses_str, ...
                    '_task-rest_bold', inputFunc_suffix, '+tlrc'];
                
                % the output functional data name
                funcName_output = [subj_str, '_', ses_str, ...
                    '_task-rest_bold', outputFunc_suffix, '+tlrc'];
                
                % start smoothing               
                if smooth_toFWHM == 1 % 3dBlurToFWHM
                    cmd = ['3dBlurToFWHM -prefix ', funcName_output, ...
                        ' -input ',funcName_input,' -automask',...
                        ' -FWHM ', smooth_fwhm];
                    system(cmd)
                elseif smooth_toFWHM == 0 % 3dBlurInMask
                    cmd = ['3dBlurInMask -prefix ', funcName_output, ...
                        ' -input ',funcName_input,' -automask',...
                        ' -FWHM ', smooth_fwhm];
                    system(cmd);
                end
               
            end
        end
    end
end

%% remove the first [tcat] TRs from the functional data to reach steady state
% The AFNI command 3dTcat is used to remove the first [tcat] TRs from the
% functional data.
if tcat>0 % in case we want to remove the first a few TRs from the functional data
if ~strcmp(smooth_fwhm, '0') % in case we want to apply extra smooth to the functional data
    if smooth_toFWHM == 1 % 3dBlurToFWHM
        inputFunc_suffix = ['_reg_topup_ts_aw_to', smooth_fwhm, 'mm'];
        outputFunc_suffix = ['_reg_topup_ts_aw_to', smooth_fwhm, 'mm_', num2str(tcat), 'tcat'];
    elseif smooth_toFWHM == 0 % 3dBlurInMask
        inputFunc_suffix = ['_reg_topup_ts_aw_', smooth_fwhm, 'mm'];
        outputFunc_suffix = ['_reg_topup_ts_aw_', smooth_fwhm, 'mm_', num2str(tcat), 'tcat'];
    end
else % in case we don't apply extra smooth to the functional data
    inputFunc_suffix = ['_reg_topup_ts_aw'];
    outputFunc_suffix = ['_reg_topup_ts_aw_', num2str(tcat), 'tcat'];
end

for isub  = subj_idx_include
    for iday = 1:2
        
        if iday == 1
            treat_or_cont = 'treat';
        else
            treat_or_cont = 'cont';
        end
        
        subj_str = ['sub-', treat_or_cont, num2str(isub, '%02.f')];
        for iscan = 1:2
            ses_str = ['ses-', num2str(iscan, '%02.f')];
            % ses-01: pre session
            % ses-02: post session
            
            % the folder where we save processed the functional data
            ProcFunc_save_dir = [ProcData_save_root_dir, subj_str,...
                '/', ses_str, '/func/'];
            
            % go to the anat save dir
            cd(ProcFunc_save_dir);
            
            % the input functional data name
            funcName_input = [subj_str, '_', ses_str, ...
                '_task-rest_bold', inputFunc_suffix, '+tlrc'];
            
            % the output functional data name
            funcName_output = [subj_str, '_', ses_str, ...
                '_task-rest_bold', outputFunc_suffix, '+tlrc'];
            
            % tcat
            cmd = ['3dTcat -prefix ', funcName_output, ' ', ...
                funcName_input, '''[', num2str(tcat), '..$]''']; % 0, 1, ..., [tcat]-1 TRs are removed
            system(cmd)

            
        end
    end
end
end
%% regress out nuisance regressors from the functional data using GLM
% in-house implemented functions are used to calculate the nuisance
% regressors and perform the GLM.
if ~strcmp(smooth_fwhm, '0') && tcat>0
    % applied extra smooth and removed the first a few TRs
    if smooth_toFWHM == 1 % 3dBlurToFWHM
        inputFunc_suffix = ['_reg_topup_ts_aw_to', smooth_fwhm, 'mm_', num2str(tcat), 'tcat'];
    elseif smooth_toFWHM == 0 % 3dBlurInMask
        inputFunc_suffix = ['_reg_topup_ts_aw_', smooth_fwhm, 'mm_', num2str(tcat), 'tcat'];
    end
elseif ~strcmp(smooth_fwhm, '0') && ~(tcat>0)
    % applied extra smooth but didn't remove the first a few TRs
    if smooth_toFWHM == 1 % 3dBlurToFWHM
        inputFunc_suffix = ['_reg_topup_ts_aw_to', smooth_fwhm, 'mm'];
    elseif smooth_toFWHM == 0 % 3dBlurInMask
        inputFunc_suffix = ['_reg_topup_ts_aw_', smooth_fwhm, 'mm'];
    end
elseif strcmp(smooth_fwhm, '0') && tcat>0
    % removed the first a few TRs but didn't apply extra smooth
    inputFunc_suffix = ['_reg_topup_ts_aw_', num2str(tcat), 'tcat'];
elseif strcmp(smooth_fwhm, '0') && ~(tcat>0)
    % didn't apply extra smooth and didn't remove the first a few TRs
    inputFunc_suffix = ['_reg_topup_ts_aw'];
end

% The functional data of all the runs will be saved in the following path.
% When form the post GLM data save path, I add the date info as part of the
% suffix. It is because the same GLM_str doesn't mean the same GLM options,
% since we have many GLM options, but we only have some key options in
% the GLM_str.
% The date info is in this format: YYYYMMDDHHMM
t = datetime;
date_str = [num2str(t.Year), num2str(t.Month, '%02.f'),...
    num2str(t.Day, '%02.f'), num2str(t.Hour, '%02.f'), num2str(t.Minute, '%02.f')];

postGLM_save_dir = [ProcData_save_root_dir, inputFunc_suffix,...
    '_GLM', GLM_str, '_tlrc_', date_str, '/'];
if ~exist(postGLM_save_dir, 'dir')
    mkdir(postGLM_save_dir)
end

% export the GLM options to a txt file
fid = fopen([postGLM_save_dir, 'GLM_options.txt'], 'w');
export_GLMoptions(GLM, fid)
fclose(fid)

% form the path for the QA plots
if if_plot_QA
    plot_QA_save_dir = [postGLM_save_dir, 'QA_plots/'];
    if ~exist(plot_QA_save_dir, 'dir')
        mkdir(plot_QA_save_dir)
    end
end
% form the path for the regressor QA plots
if if_plot_regs
    % the path we save the polynomial regressors
    plot_regs_save_root_dir = [postGLM_save_dir, 'Regs_plots/'];
    if ~exist(plot_regs_save_root_dir, 'dir')
        mkdir(plot_regs_save_root_dir)
    end
    % where we save the plots for the motion parameters
    if GLM.motion
        plot_regs_Motion_save_dir = [plot_regs_save_root_dir, 'motion/'];
        if ~exist(plot_regs_Motion_save_dir, 'dir')
            mkdir(plot_regs_Motion_save_dir)
        end
    end
    % where we save the plots for WM and CSF regressors
    if GLM.WMCSF
        plot_regs_WMCSF_save_dir = [plot_regs_save_root_dir, 'WMCSF/'];
        if ~exist(plot_regs_WMCSF_save_dir, 'dir')
            mkdir(plot_regs_WMCSF_save_dir)
        end
    end
    % where we save the plots for RVHRCOR regressors
    if GLM.RVHRCOR
        plot_regs_RVHRCOR_save_dir = [plot_regs_save_root_dir, 'RVHRCOR/'];
        if ~exist(plot_regs_RVHRCOR_save_dir, 'dir')
            mkdir(plot_regs_RVHRCOR_save_dir)
        end
    end
end

% initialization
WMmask_all = {}; CSFmask_all = {};
num_vox_in_WM_mask = []; num_vox_in_CSF_mask = [];
card_duration = []; num_TRs_in_card_data = [];
resp_duration = []; num_TRs_in_resp_data = [];
GLM_regressors_all = {};

for isub  = subj_idx_include
    for iday = 1:2
        
        if iday == 1
            treat_or_cont = 'treat';
        else
            treat_or_cont = 'cont';
        end
        
        subj_str = ['sub-', treat_or_cont, num2str(isub, '%02.f')];
        for iscan = 1:2
            ses_str = ['ses-', num2str(iscan, '%02.f')];
            % ses-01: pre session
            % ses-02: post session
            if iscan == 1
                pre_or_post = 'pre';
            else
                pre_or_post = 'post';
            end
            
            % the data info str used for figure title and figure names
            subj_info_str = ['subject: ', num2str(isub), ' ', treat_or_cont,...
                ' ', pre_or_post, '(', ses_str, ')'];
            subj_info_str_for_save= [subj_str, '_', ses_str];
            
            
            % the folder where we save processed the anatomical data
            ProcAnat_save_dir = [ProcData_save_root_dir, subj_str,...
                '/', ses_str, '/anat/'];
            % the folder where we save processed the functional data
            ProcFunc_save_dir = [ProcData_save_root_dir, subj_str,...
                '/', ses_str, '/func/'];
            % the folder where we save the raw physiological data
            rawPhyiso_save_dir = [repo_dir, 'physio/', subj_str,...
                '/', ses_str, '/physio/'];
            
            % go to the direcotry where we want the processed functional
            % data to be saved
            cd(postGLM_save_dir);
            
            % the input functional data name
            funcName_input = [ProcFunc_save_dir, subj_str, '_', ses_str, ...
                '_task-rest_bold', inputFunc_suffix, '+tlrc'];
            
            % the output functional data name
            funcName_output = [subj_str, '_', ses_str, ...
                '_proc+tlrc'];
            
            %% form the legendre polynomial regressors
            reg_polynomials = [];
            if GLM.Poly
                reg_polynomials = legendremat(GLM.Poly_MaxOrder,nvol-tcat);
            end
            
            %% form the motion regressors
            reg_motion = [];
            if GLM.motion
                motion_file_path = [ProcFunc_save_dir, subj_str, '_', ses_str, ...
                    '_task-rest_bold_reg_topup_motion.1D'];
                [motion_paras,reg_motion] = calcMotionReg(motion_file_path,...
                    GLM.motion_type, GLM.motion_filter,...
                    GLM.motion_filter_method, GLM.motion_filter_paras, TR);
                reg_motion = reg_motion(end-(nvol-tcat)+1:end, :); % remove the first tcat volumes if there is any
            end
            
            %% form the WM and CSF regressor
            reg_WMCSF = [];
            if GLM.WMCSF
                if strcmp(GLM.WMCSF_space, 'native') % functional data in the original space are used
                    % load the functional data after the volume registration,
                    % topup correction and the time-shift correction in the
                    % native space.
                    funcName_orig_proc = [ProcFunc_save_dir, subj_str, '_', ses_str, ...
                        '_task-rest_bold_reg_topup+orig'];
                    [~,funcData_orig,funcROI] =...
                        loadFunc(funcName_orig_proc);
                    % form a whole brain mask in native space
                    brain_mask_orig = smoothmask(funcROI>0);
                    for ii=1:size(brain_mask_orig,3)
                        brain_mask_orig(:,:,ii)=bwmorph(brain_mask_orig(:,:,ii),'close',Inf);
                        brain_mask_orig(:,:,ii)=bwmorph(brain_mask_orig(:,:,ii),'erode',2);
                    end;
                    % load the partial volume (pv) maps outputed by the FAST algorithm
                    % note, the pv maps were aligned to the functional data
                    anat_for_pve = [ProcAnat_save_dir, subj_str, '_', ses_str, ...
                        '_T1w_ns_al+orig'];
                    fslVersion = 6;
                    [pv_mask]=fsl_loadpv(anat_for_pve,funcName_orig_proc,fslVersion);
                    % calculate the WM and CSF regressors
                    [reg_WMCSF, AnatMask_white, AnatMask_csf] = ...
                        calcWMCSFreg(funcData_orig,pv_mask,brain_mask_orig,...
                        GLM.WMCSF_type,GLM.WMCSF_numPC,...
                        GLM.WMCSF_derivatives, GLM.WMCSF_combine_WM_CSF, ...
                        GLM.WMCSF_erosionNum, GLM.WMCSF_pv_thres, ...
                        GLM.WMCSF_ClustSize, GLM.WMCSF_ClustMode, ...
                        GLM.WMCSF_FuncNorm, GLM.WMCSF_FuncDetrend);
                    
                    % remove the first tcat volumes if there is any
                    reg_WMCSF = reg_WMCSF(end-(nvol-tcat)+1:end, :);
                    % to be saved
                    WMmask_all{isub, iscan} = AnatMask_white;
                    CSFmask_all{isub, iscan} = AnatMask_csf;
                    num_vox_in_WM_mask(isub, iscan) = nnz(AnatMask_white>0);
                    num_vox_in_CSF_mask(isub, iscan) = nnz(AnatMask_csf>0);
                end
            end
            
            %% form the RVHRCOR regressors
            reg_RVHRCOR = [];
            if GLM.RVHRCOR
                % load the raw physiological signals
                raw_card = load([rawPhyiso_save_dir, subj_str, '_', ses_str, '_raw_cardiac']);
                raw_resp = load([rawPhyiso_save_dir, subj_str, '_', ses_str, '_raw_respiration']);
                
                % calculate RVRRF and HRCRF
                [HR, HRCRF, CRF, c_peak] = calcHRCRF(raw_card,...
                    GLM.RVHRCOR_CardSrate, TR, GLM.RVHRCOR_HR_WinLen);
                [RV, RVRRF, RRF] = calcRVRRF(raw_resp, GLM.RVHRCOR_RespSrate,...
                    TR, GLM.RVHRCOR_RV_WinLen);
                
                % remove the first tcat volumes if there is any
                reg_RVHRCOR=[HRCRF, RVRRF];
                reg_RVHRCOR = reg_RVHRCOR(end-(nvol-tcat)+1:end, :);
                
                % calculate some things about the raw physiological signal
                % it is not related to the calculatio of RVHRCOR regressors
                card_sample_per_TR = GLM.RVHRCOR_CardSrate*TR; % number of raw cardiac timepoints in one TR
                resp_sample_per_TR = GLM.RVHRCOR_RespSrate*TR;
                
                card_duration(isub, iscan) = length(raw_card)/GLM.RVHRCOR_CardSrate;
                num_TRs_in_card_data(isub, iscan) = floor(length(raw_card)/card_sample_per_TR);
                
                resp_duration(isub, iscan) = length(raw_resp)/GLM.RVHRCOR_RespSrate;
                num_TRs_in_resp_data(isub, iscan) = floor(length(raw_resp)/resp_sample_per_TR);
                
            end
            
            %% form the global signal regressor
            reg_GS = [];
            if GLM.GS
                % the functional data before the GLM are used to calculate the
                % global signal (GS)
                [~, funcData_preGLM, ~, ~] = BrikLoad(funcName_input);
                [reg_GS]=calcGS(funcData_preGLM, brainMask, GLM.GS_FuncNorm);
            end
            %% concatenate all regressors
            regressors = [reg_polynomials, reg_motion, reg_WMCSF, reg_RVHRCOR, reg_GS];
            % save the regressors
            save([postGLM_save_dir, subj_info_str_for_save, '_GLMregs'], 'regressors')
            %% regress out nuisance regressors from the functional data
            if GLM.doGLM
                % load the functional data before GLM
                [~, funcData_preGLM, ~, ~] = BrikLoad(funcName_input);
                if GLM.Poly_outsideGLM % Regress out the polynomial
                    % regressors outside the GLM
                    GLM_regressors = [reg_motion, reg_WMCSF, reg_RVHRCOR, reg_GS];
                    % Regress out the polynomial regressors from other
                    % regressors.
                    GLM_regressors = GLM_regressors - reg_polynomials*...
                        inv(reg_polynomials'*reg_polynomials)*reg_polynomials'...
                        *GLM_regressors;
                    % Regress out the polynomial regressors from the functional
                    % data. Note, the mean of the functional data will not be
                    % removed.
                    funcData_preGLM = processPreg_RS(funcData_preGLM,reg_polynomials,brainMask, 0);
                else
                    GLM_regressors = [reg_polynomials, reg_motion, reg_WMCSF, reg_RVHRCOR, reg_GS];
                end
                % do the GLM
                [funcData_postGLM, doff]= processPreg_RS(funcData_preGLM,GLM_regressors,brainMask, 0);
                % save the functional data after GLM
                Mat2AFNI(double(funcData_postGLM), funcName_output,  ...
                    funcName_input, 3);
                % add the TR info back
                cmd = ['3drefit -TR ', num2str(TR), ' ' funcName_output];
                system(cmd)
                % save the regressors
                save([postGLM_save_dir, subj_info_str_for_save, '_GLMregs'],...
                    'regressors', 'GLM_regressors')
                if if_plot_QA
                    QAplot_Opts.subjInfo = ['subject: ', num2str(isub), ' ', treat_or_cont, ' ', pre_or_post];
                    QAplot_Opts.GLM_Poly_outsideGLM = GLM.Poly_outsideGLM; % pass this option
                    if GLM.WMCSF_type % PCA
                        if GLM.WMCSF_combine_WM_CSF
                            WM_reg = reg_WMCSF(:, 1);
                            CSF_reg = reg_WMCSF(:, 2);
                        else
                            WM_reg = reg_WMCSF(:, 1);
                            CSF_reg = reg_WMCSF(:, GLM.WMCSF_numPC+1);
                        end
                    else
                        WM_reg = reg_WMCSF(:, 1);
                        CSF_reg = reg_WMCSF(:, 2);
                    end
                    the_further_proc_QA(funcData_preGLM, funcData_postGLM, ...
                        brainMask, nvol-tcat, TR, ...
                        motion_paras, WM_reg, CSF_reg, raw_card, GLM.RVHRCOR_CardSrate,...
                        c_peak, HR, HRCRF, raw_resp, GLM.RVHRCOR_RespSrate, RV, RVRRF, QAplot_Opts)
                    % save the QAplot
                    QAplot_save_name = [subj_info_str_for_save, '_theQAplot'];
                    % saveas(gcf, file_name, 'epsc')
                    saveas(gcf, [plot_QA_save_dir, QAplot_save_name], 'png')
                    close(gcf)
                end
            end
            %% save the regressors
            GLM_regressors_all{isub, iscan} = GLM_regressors;
            %% plot the regressors if if_plot_regs is not 0
            if if_plot_regs % plot the regressors
                if GLM.Poly
                    % plot the legendre polynomial regressors
                    plot_regs_Poly(reg_polynomials,subj_info_str, line_width, label_size)
                    saveas(gcf, [plot_regs_save_root_dir,...
                        'reg_polynomials_maxOrder',...
                        num2str(GLM.Poly_MaxOrder)], 'png')
                    close(gcf)
                end
                
                if GLM.motion
                    % generate the plot showing the motion parameters
                    plot_regs_Motion(reg_motion, subj_info_str, line_width, label_size)
                    saveas(gcf, [plot_regs_Motion_save_dir,...
                        subj_info_str_for_save], 'png')
                    close(gcf)
                end
                
                if GLM.WMCSF
                    % generate the plots about the WM and CSF regressors
                    % load the anatomical image that was aligned to the functional data
                    % first resample the anatomical images
                    % this resampled anat image is the underlay of the pv
                    % maps
                    anat_for_pve_rs = [ProcAnat_save_dir, subj_str, '_', ses_str, ...
                        '_T1w_ns_al_rs+orig'];
                    if ~isfile([anat_for_pve_rs, '.BRIK'])
                        cmd = ['3dresample -master ', funcName_orig_proc,...
                            ' -prefix ', anat_for_pve_rs,...
                            ' -input ', anat_for_pve];
                        system(cmd)
                    end
                    
                    % plot settings
                    slices_plot = [1:72];
                    nrow = 8;
                    ncol = 9;
                    
                    % plot 1: results of brain tissue segmentation
                    % overlay is the partial volume maps for gray matter, white matter
                    % and CSF, i.e. the result of brain tissue segmentation
                    gray_pv_thres = 0.9;
                    plot_regs_TissueSeg(anat_for_pve_rs, slices_plot, nrow, ncol,...
                        brain_mask_orig, pv_mask, gray_pv_thres, GLM, subj_info_str,...
                        line_width, label_size)
                    saveas(gcf, [plot_regs_WMCSF_save_dir, 'brain_tissue_seg_',...
                        subj_info_str_for_save], 'png')
                    close(gcf)
                    
                    % plot2: WM and CSF masks used to generate the WM and CSF regressors
                    % overlay is the WM and CSF maps used to calculate the WM and CSF
                    % regressors
                    plot_regs_WMCSFmask(anat_for_pve_rs, squeeze(funcData_orig(:, :, :, 1)),...
                        AnatMask_white, AnatMask_csf, brain_mask_orig, slices_plot, ...
                        nrow, ncol, GLM, subj_info_str, line_width, label_size)
                    saveas(gcf, [plot_regs_WMCSF_save_dir, 'WMCSF_masks_',...
                        subj_info_str_for_save], 'png')
                    close(gcf)
                    
                    % plot3: WM and CSF regressors
                    plot_regs_WMCSF(reg_WMCSF, GLM, subj_info_str,...
                        line_width, label_size)
                    if GLM.WMCSF_type % PCA
                        if GLM.WMCSF_combine_WM_CSF % combined WM and CSF masks
                            saveas(gcf, [plot_regs_WMCSF_save_dir,...
                                'reg_aCompCor', num2str(GLM.WMCSF_numPC),...
                                'WMCSF_', subj_info_str_for_save], 'png')
                            close(gcf)
                        else
                            saveas(gcf, [plot_regs_WMCSF_save_dir,...
                                'reg_aCompCor', num2str(GLM.WMCSF_numPC),...
                                'WM', num2str(GLM.WMCSF_numPC), 'CSF_',...
                                subj_info_str_for_save], 'png')
                            close(gcf)
                        end
                    else % mean
                        saveas(gcf, [plot_regs_WMCSF_save_dir,...
                            'reg_meanWMCSF',...
                            subj_info_str_for_save], 'png')
                        close(gcf)
                    end
                end
                
                if GLM.RVHRCOR
                    % plot raw physio signals, HR, RV, HRCRF and RVCRF
                    plot_regs_RVHRCOR(reg_RVHRCOR, raw_card, c_peak,...
                        GLM.RVHRCOR_CardSrate, HR,...
                        raw_resp, GLM.RVHRCOR_RespSrate, RV,...
                        nvol-tcat, TR, subj_info_str, line_width, label_size)
                    saveas(gcf, [plot_regs_RVHRCOR_save_dir,...
                        subj_info_str_for_save], 'png')
                    close(gcf)
                end
            end
        end
    end
    
end

% save the summary data
save([postGLM_save_dir, 'GLM_summary_data'], ...
    'WMmask_all', 'CSFmask_all', ...
    'num_vox_in_WM_mask', 'num_vox_in_CSF_mask', ....
    'card_duration', 'num_TRs_in_card_data', ...
    'resp_duration', 'num_TRs_in_resp_data', ...
    'GLM_regressors_all', 'GLM')

%% convert the functional data to percent change
% The AFNI commands 3dTstat and 3dcalc aare used to convert the functional
% data to percent change.
for isub  = subj_idx_include
    for iday = 1:2
        
        if iday == 1
            treat_or_cont = 'treat';
        else
            treat_or_cont = 'cont';
        end
        
        subj_str = ['sub-', treat_or_cont, num2str(isub, '%02.f')];
        for iscan = 1:2
            ses_str = ['ses-', num2str(iscan, '%02.f')];
            % ses-01: pre session
            % ses-02: post session
            if iscan == 1
                pre_or_post = 'pre';
            else
                pre_or_post = 'post';
            end
            
            % the data info str used for figure title and figure names
            subj_info_str = ['subject: ', num2str(isub), ' ', treat_or_cont,...
                ' ', pre_or_post, '(', ses_str, ')'];
            subj_info_str_for_save= [subj_str, '_', ses_str];
            
            
            % the folder where we save processed the anatomical data
            ProcAnat_save_dir = [ProcData_save_root_dir, subj_str,...
                '/', ses_str, '/anat/'];
            % the folder where we save processed the functional data
            ProcFunc_save_dir = [ProcData_save_root_dir, subj_str,...
                '/', ses_str, '/func/'];
            
            % go to the direcotry where we want the processed functional
            % data to be saved
            cd(postGLM_save_dir);
            
            % the input functional data name
            funcName_input = [subj_str, '_', ses_str, ...
                '_proc+tlrc'];
            
            % the output functional data name
            funcName_output = [subj_str, '_', ses_str, ...
                '_proc_PercChng+tlrc'];
            
            % calculate the mean of the functional data
            cmd = ['3dTstat -mean -prefix ', funcName_input(1:end-5), '_mean+tlrc', ' ', funcName_input];
            system(cmd)
            
            % convert the functional data to percent change
            cmd = ['3dcalc -a ',funcName_input,' -b ', funcName_input(1:end-5), '_mean+tlrc',...
                ' -expr ''100*((a-b)/b)'' -prefix ', funcName_output];
            system(cmd)
            
            % calculate the stdev of the percent change functional data
            cmd = ['3dTstat -stdev -prefix ', funcName_output(1:end-5), '_stdev+tlrc', ' ', funcName_output];
            system(cmd)
            
        end
        
    end
    
end


