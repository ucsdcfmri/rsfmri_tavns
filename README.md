# README #

This repo contains matlab scripts used to generate the results in 'Effects of Sub-threshold Transcutaneous Auricular Vagus Nerve Stimulation on Cingulate Cortex and Insula Resting-state Functional Connectivity' (https://doi.org/10.3389/fnhum.2022.862443).

Authors: Yixiang Mao (y1mao@ucsd.edu), Conan Chen, Maryam Falahpour, Kelly H. MacNiven, Gary Heit, Vivek Sharma, Konstantinos Alataris and Thomas T. Liu (ttliu@ucsd.edu)

### Contents ###

* `./main/`

	`the_minimal_proc_pipeline.m` 			- the script that does minimal fmri data processing

	`the_further_proc_pipeline.m`			- the script that does further fmri data processing

	`the_further_proc_pipeline_Filt_MoCen.m` 	- the script that does filtering and motion censoring

	`groupAnalysis.m` 						- the script that does the group level analyses

	`groupAnalysis_supp.m` 					- the script that does supplementary analysis


* `./dependencies/`
	contains functions that will be used in main matlab scripts.


* `./masks_and_templates/`
	contains the standard space templates and the ROI masks.


* `./physio./`
	contains the physiological signals (the raw cardiac and respiration signals).

### How to reproduce the results in the paper? ###
Starting with the summary data:**

* Download the summary data here: https://osf.io/c2s9z/. 
* Put the downloaded data under the directory of this repo.
* Run groupAnalysis.m and groupAnalysis_supp.m to get the results in the paper. 

Note, please revise the directories properly in the matlab scripts before run them.

### Questions? ###

If you have any questions or find any mistakes in the scripts, please feel free to contact: Yixiang Mao (y1mao@ucsd.edu) and Thomas T. Liu (ttliu@ucsd.edu)